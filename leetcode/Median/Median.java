class Main
{
    public static void main(String[] args)
    {
        Solution s = new Solution();
        int [] a1 = new int[] {1};
        int [] a2 = new int[] {2, 3, 4, 5, 6, 7, 8};

        System.out.println(s.findMedianSortedArrays(a1, a2));
    }
}

class Solution
{
    public double findMedianSortedArrays(int[] nums1, int[] nums2)
    {
        int [] a1;
        int [] a2;
        // Use the larger of the 2 arrays as n2, and the smaller as n1. Reference copy is fine.
        if(nums1.length >= nums2.length)
        {
            a1 = nums1;
            a2 = nums2;
        }
        else
        {
            a1 = nums2;
            a2 = nums1;
        }

        /*
         * Things to Keep in mind for the algorithm
         * We are looking for the correct cut which results in finding the median
         * Pretend each array has length * 2 + 1 places to cut at. If cut in a postition between 2 numbers, take average.
         * The cut should have the same amount of numbers on the left and the right
         * L and R represent values to the immediate left and right of a cut respectively.
         * The value chosen of where to make Cut2 determines the spot where Cut1 is made, 
         * For the 2 cuts to be correct, L1 < R2 and L2 < R1. If this is true, then the cut is the one we want.
         * If not, then we move where we cut. The process of moving the cut uses binary search.
         * If a cut is made on an edge of the array, -infinity or positive infinity should be used for L or R respectively
         */
        int n1 = a1.length;
        int n2 = a2.length;

        // Set up binary search over nums2
        int lo = 0;
        int hi = n2 * 2;

        while(lo <= hi)
        {
            int mid2 = (lo + hi) / 2;

            // There are N1 + N2 + 2 places to cut, 2 of the spaces are adjacent to the cut, therefore
            int mid1 = n1 + n2 - mid2;


            double l1 = 0.0;
            double l2 = 0.0;
            double r1 = 0.0;
            double r2 = 0.0;

            if(mid1 == 0)
            {
                l1 = Double.MIN_VALUE;
            }
            else
            {
                l1 = a1[(mid1 - 1)/ 2];
            }

            if(mid1 == n1 * 2)
            {
                r1 = Double.MAX_VALUE; 
            }
            else
            {
                r1 = a1[mid1/2];
            }

            if(mid2 == 0)
            {
                l2 = Double.MIN_VALUE;
            }
            else
            {
                l2 = a2[(mid2 - 1) / 2];
            }

            if(mid2 == n2 * 2)
            {
                r2 = Double.MAX_VALUE;
            }
            else
            {
                r2 = a2[(mid2 / 2)];
            }

            // Move mids based on whether the cut is too far to the left or right.
            if(l1 > r2)
            {
                // In this case left segment 1 has too large of a value, so cut 1 needs to move further left. This also means cut2 moves right.
                lo = mid2 + 1;
            }
            else if(l2 > r1)
            {
                hi = mid2 - 1;
            }
            else
            {
                // This is the correct cut.
                // L1, L2, R1, and R2 are adjacent to the 2 individual cuts. Only 2 of them are adjacent to the actual cumulative cut.
                // The larger of the L's and the smaller of the R's is where the actual cut is.
                return (Math.max(l1, l2) + Math.min(r1, r2)) / 2;
            }
        }

        // Error case
        return -1;
    }
}
