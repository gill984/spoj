/*
 * Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
 * Note: The solution set must not contain duplicate triplets.
 *
 * For example, given array S = [-1, 0, 1, 2, -1, -4],

 * A solution set is:
 * [
 *   [-1, 0, 1],
 *   [-1, -1, 2]
 * ]
 */
import java.util.HashMap;
import java.util.Arrays;

class Solution 
{
	public List<List<Integer>> threeSum(int[] nums) 
	{
		// This map has values which correspond to all possible pairings of values in the input array
		// The keys for each value are the integers needed for the key to sum to 0.
		HashMap<ArrayList<Integer>, Integer> pairToVal = new HashMap<ArrayList<Integer>, Integer>();

		// Sort input array to avoid adding any duplicates
		Arrays.sort(nums);

		// For each pair, add the pair to the hashmap
		for(int i = 0; i < nums.length - 1; i++)
		{
			for(int j = i + 1; j < nums.length; j++)
			{
				// Create key for the hashmap
				ArrayList<Integer> pair = new ArrayList<Integer>();
				pair.add(nums[i]);
				pair.add(nums[j]);
				int val = -1 * (nums[i] + nums[j]);

				// Add key and value to the hashmap
				pairToVal.put(val, pair);
			}
		}

		
	}
}
