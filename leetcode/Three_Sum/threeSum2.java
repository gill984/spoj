import java.util.*;

class Solution {
    public List<List<Integer>> threeSum(int[] num) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();

      // Sort the array, this is part of avoiding duplicates
      Arrays.sort(num);

      // For each number in the array, look for pairs which add to 0 using a two
      // pointer method
      // The 2 pointer method is similar to binary search
      // There is a lo and a hi, iteration continues until lo >= hi
      // lo starts at i + 1, hi starts at nums.length - 1
      for (int i = 0; i < num.length - 2; i++)
      {
         // lo starts at i + 1, hi starts at the end of the array
         int lo = i + 1;
         int hi = num.length - 1;

         // Check if the current index matches the previous, if it does, skip
         // it. This helps ignore duplicate triples
         if (i == 0 || num[i - 1] != num[i])
         {
            // Execute the 2 pointer algorithm
            while (lo < hi)
            {
               // Check to see if the triplet (i, lo, hi) is a valid triplet
               // which adds to 0
               if (num[lo] + num[hi] + num[i] == 0)
               {
                  result.add(Arrays.asList(num[i], num[lo], num[hi]));

                  // Move the pointers past any duplicate values to avoid adding
                  // duplicate triplets
                  while (lo < hi && num[lo] == num[lo + 1])
                  {
                     lo++;
                  }
                  while (lo < hi && num[hi] == num[hi - 1])
                  {
                     hi--;
                  }
                  lo++;
                  hi--;
               }
               else if (num[lo] + num[hi] < -num[i])
               {
                  lo++;
               }
               else
               {
                  hi--;
               }
            }
         }
      }

      return result;
    }
}
