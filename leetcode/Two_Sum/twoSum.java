/*
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 */

class Solution
{
	public int[] twoSum(int[] nums, int target)
	{
		// For each number in nums, loop through the rest of nums.
		// If the sum of both numbers equals target, return [num1, num2]		
		for(int i = 0; i < nums.length - 1; i++)
		{
			for(int j = i + 1; j < nums.length; j++)
			{
				if(nums[i] + nums[j] == target)
				{
					return new int[] {nums[i], nums[j]};
				}
			}
		}
	}
}
