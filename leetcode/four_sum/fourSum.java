import java.util.HashMap;
import java.util.ArrayList;
import java.util.HashSet;

class Solution
{
    public List<List<Integer>> fourSum(int[] nums, int target)
    {
        if(nums == null || nums.length < 4)
            return new ArrayList<List<Integer>>();
        
        // Create n^2 sized array of all couples of numbers
        // Create a map from a sum value to the 
        int n = nums.length;
        Tuple[] pairs = new Tuple[n * (n - 1) / 2];
        
        int count = 0;
        for(int i = 0; i < nums.length; i++)
        {
            for(int j = i + 1; j < nums.length; j++)
            {
                pairs[count++] = new Tuple(nums[i], nums[j], i, j);
            }
        }
        
        // HashMap, sum --> List<pair>
        HashMap<Integer, ArrayList<Tuple>> map = new HashMap<Integer, ArrayList<Tuple>>();
        HashSet<Quad> solutions = new HashSet<Quad>();
        
        // Create hashmap while solving problem
        // For each pair, add this pair to the list mapped to by the sum
        // Then check the target - sum value to see if a solution set has been found, if it has, combine to quads and
        // Add these quads to the hashset of solutions
        for(int i = 0; i < pairs.length; i++)
        {
            Tuple t = pairs[i];
            int sol = target - t.sum;
            
            // Check to see if solution exists. Should do this first to avoid case where doubling is correct.
            if(map.containsKey(sol))
            {
                // Create quads and add them to the set of solutions
                ArrayList<Tuple> vals = map.get(sol);
                for(Tuple c : vals)
                {
                    if(t.adx != c.adx && t.adx != c.bdx && t.bdx != c.adx && t.bdx != c.bdx)
                    {
                        Quad q = new Quad(t.a, t.b, c.a, c.b);
                        solutions.add(q);
                    }
                }
            }
            
            // If absent, add new list
            if(!map.containsKey(t.sum))
            {
                ArrayList<Tuple> l = new ArrayList<Tuple>();
                map.put(t.sum, l);
            }
            
            // Add this tuple to the map
            map.get(t.sum).add(t);
        }
        
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        
        for(Quad q : solutions)
        {
            ArrayList l = new ArrayList<Integer>();
            l.add(q.vals[0]);
            l.add(q.vals[1]);
            l.add(q.vals[2]);
            l.add(q.vals[3]);
            result.add(l);
        }
        
        return result;
    }
}

class Tuple implements Comparable
{
    public int a;
    public int b;
    
    public int adx;
    public int bdx;
    
    public int sum;
    
    public Tuple(int a, int b, int adx, int bdx)
    {
        this.a = a;
        this.b = b;
        this.adx = adx;
        this.bdx = bdx;        
        this.sum = a + b;
    }
    
    public int compareTo(Object o)
    {
        Tuple t = (Tuple) o;
        return this.sum - t.sum;
    }
}

class Quad
{
    public int [] vals;
    
    public Quad(int a, int b, int c, int d)
    {
        vals = new int[4];
        vals[0] = a;
        vals[1] = b;
        vals[2] = c;
        vals[3] = d;
        Arrays.sort(vals);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + vals[0];
        result = prime * result + vals[1];
        result = prime * result + vals[2];
        result = prime * result + vals[3];
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Quad other = (Quad) obj;
        if (this.vals[0] != other.vals[0])
            return false;
        if (this.vals[1] != other.vals[1])
            return false;
        if (this.vals[2] != other.vals[2])
            return false;
        if (this.vals[3] != other.vals[3])
            return false;
        
        return true;
    }
}
