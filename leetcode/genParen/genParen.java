class Solution
{
    public List<String> generateParenthesis(int n)
    {
        // Keep track of the sum of left parentheses which we have placed and build
        if(n == 0)
            return new ArrayList<String>();
        
        ArrayList<String> list = new ArrayList<String>();
        genParens("(", 1, n, list);
        
        return list;
    }
    
    public void genParens(String s, int left, int n, ArrayList<String> soln)
    {
        if(s.length() == n * 2)
        {
            if(left == 0)
                soln.add(s);
            return;
        }
        
        if(left > 0)
            genParens(s + ")", left - 1, n, soln);                
        
        genParens(s + "(", left + 1, n, soln);        
    }
}
