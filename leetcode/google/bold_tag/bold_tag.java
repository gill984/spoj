class Solution
{
    public String addBoldTag(String s, String[] dict)
    {
        boolean [] bold = new boolean[s.length()];  // If true, bold; if false, not bold
        for(String sub : dict)
        {
            int idx = 0;
            while(true)
            {
                idx = s.indexOf(sub, idx);
                if(idx == -1)
                {
                    // No substrings remaining, move on to next string
                    break;
                }
                else
                {
                    // Found substring, mark up boolean array
                    markRange(bold, idx, sub.length());
                    idx += 1;
                }
            }
        }

        // Boolean array is now marked, apply bold tag
        StringBuilder res = new StringBuilder();
        boolean bolding = false;
        for(int i = 0; i < s.length(); i++)
        {
            if(bold[i] && !bolding) 
            {
                res.append("<b>");
                bolding = true;
            }
            else if(!bold[i] && bolding)
            {
                res.append("</b>");
                bolding = false;
            }
            res.append(s.charAt(i));
        }

        if(bolding)
            res.append("</b>");

        return res.toString();
    }

    public void markRange(boolean [] arr, int start, int length)
    {
        for(int i = 0; i < length; i++)
            arr[i + start] = true;
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution s = new Solution();
        String str = "abcxyz123";
        String [] dict1 = {"abc", "123"};
        System.out.println(s.addBoldTag(str, dict1));

        str = "aaabbcc";
        String [] dict2 = {"aaa","aab","bc"};
        System.out.println(s.addBoldTag(str, dict2));
    }
}
