import java.util.ArrayList;
import java.util.ArrayDeque;

class Solution
{
    private final static int NUM_IDX = 0;
    private final static int DEN_IDX = 1;
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries)
    {
        // Create a graph of equations
        // Read in equations and values and put them into an array of nodes
        Node [] nodes = new Node[equations.length];
        for(int i = 0; i < nodes.length; i++)
        {
            nodes[i] = new Node(equations[i][NUM_IDX], equations[i][DEN_IDX], values[i]);
        }

        ArrayList<ArrayList<Integer>> neighborList = new ArrayList<ArrayList<Integer>>();

        // Populate the neighbor list
        for(int i = 0; i < nodes.length; i++)
        {
            neighborList.add(new ArrayList<Integer>());
            for(int j = 0; j < nodes.length; j++)
            {
                // Don't link to self
                if(i == j) 
                    continue;
                else if(nodes[i].linkExists(nodes[j]))
                {
                    neighborList.get(i).add(j);
                }
            }
        }

        for(ArrayList<Integer> l : neighborList)
            System.out.println(l);

        for(String[] query : queries)
        {
            // Create a new node to make use of the linkExists function
            Node queryNode = new Node(query[NUM_IDX], query[DEN_IDX], 0.0)

            // Find a starting point in the node array
            for(int i = 0; i < nodes.length; i++)
            {
                if(nodes[i].linkExists(queueNode))
                {
                    // Link exists, find link to other string
                    ArrayDeque<Node> queue = new ArrayDeque<Node>();
                    queue.offer(nodes[i]);

                    // Pull out query values
                    String qNumer = query[NUM_IDX];
                    String qDenom = query[DEN_IDX];

                    // Set up boolean match values, mark true if we are starting from a point where we know the numer value
                    boolean numerFound = qNumer.equals(nodes[i].numer) || qNumer.equals(nodes[i].denom);
                    boolean denomFound = qDenom.equals(nodes[i].denom) || qDenom.equals(nodes[i].numer);

                    while(!queue.isEmpty() && (!numerFound || !denomFound))
                    {
                        
                    }
                }
            }

        }

        return null;
    }
}

class Node
{
    public String numer;
    public String denom;
    public double val;

    public Node(String n, String d, double v)
    {
        numer = n;
        denom = d;
        val = v;
    }

    public boolean linkExists(Node n)
    {
        if(this.numer.equals(n.denom) || this.numer.equals(n.numer) || this.denom.equals(n.denom) || this.denom.equals(n.numer))
            return true;
        else
            return false;
    }
}

class Main
{
    public static void main(String[] args)
    {
        String[][] equations = { {"a", "b"}, {"b", "c"}};
        double [] values = {2.0, 3.0};
        String[][] queries = { {"a", "c"}, {"b", "a"}, {"a", "e"}, {"a", "a"}, {"x", "x"}}; 

        Solution sol = new Solution();
        sol.calcEquation(equations, values, queries);
    }
}
