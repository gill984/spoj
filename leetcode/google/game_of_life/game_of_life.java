import java.util.Arrays;

class Solution
{
    private final static int ROW = 0;
    private final static int COL = 1;
    private final static int [][] nbrPos = 
    {
        {-1, -1},   // Top left
        {-1,  0},   // Straight Above
        {-1,  1},   // Top Right
        { 0,  1},   // Straight Right
        { 1,  1},   // Bottom Right
        { 1,  0},   // Straight Below
        { 1, -1},   // Bottom Left
        { 0, -1}    // Straight Left      
    };
    public void gameOfLife(int[][] board)
    {
        for(int i = 0; i < board.length; i++)
        {
            for(int j = 0; j < board[0].length; j++)
            {
                 board[i][j] += numNeighbors(board, i, j) * 10;
            }
        }


        for(int i = 0; i < board.length; i++)
        {
            for(int j = 0; j < board[0].length; j++)
            {
                 board[i][j] = aliveOrDead(board, i, j);
            }
        }
        
        return;
    }

    public int numNeighbors(int[][] board, int row, int col)
    {
        int numNeighbors = 0;
        for(int i = 0; i < nbrPos.length; i++)
        {
            int nbrRow = row + nbrPos[i][ROW];
            int nbrCol = col + nbrPos[i][COL];

            // Make sure this neighbor value is in bounds
            if(nbrRow >= 0 && nbrCol >= 0 && nbrRow < board.length && nbrCol < board[0].length)
            {
                if(board[nbrRow][nbrCol] % 10 == 1)
                {
                    numNeighbors += 1;
                }
            }
        }

        return numNeighbors;
    }

    private int aliveOrDead(int [][] board, int row, int col)
    {
        int neighbors = board[row][col] / 10;
        if(board[row][col] % 10 == 1)
        {
            // Alive case
            // If fewer than 2 living neighbors, dies
            // If two or 3 neighbors, lives
            // If more than 3 neighbors, dies
            if(neighbors < 2 || neighbors > 3)
                return 0;
            else
                return 1;
        }
        else
        {
            // Dead Case
            // Stay dead unless exactly 3 neighbors
            if(neighbors == 3)
                return 1;
            else
                return 0;
        }
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution S = new Solution();
        int [][] arr = 
        {
            {0,1,0},
            {0,0,1},
            {1,1,1},
            {0,0,0},
        };


        S.gameOfLife(arr);
        System.out.println(Arrays.deepToString(arr));
    }
}
