class Solution 
{
    public Node insert(Node head, int insertVal)
    {
        // If the list is empty, create a new cyclic list
        // So create a new node which points to itself.
        if(head == null)
        {
            Node res = new Node(insertVal, null);
            res.next = res;
            return res;
        }

        // Now handle case where we have at least 1 node
        // Iterate through list until spot is found where either there are 2 nodes in order n1 -> n2 and n1.val <= insertVal < n2.val
        // Or n1.val < insertVal, n1.val > n2.val
        Node lo = head;
        Node hi = head.next;

        while(true)
        {
            boolean normalInsertFound = (lo.val <= insertVal) && (insertVal <= hi.val);
            boolean endOfListInsertFound = (hi.val < lo.val && (insertVal >= lo.val || insertVal <= hi.val))
            boolean allEqualFound = ((hi.next == head) && (hi.val == head.val));

            // If any of these cases is true, insert into list between lo and hi
            if(normalInsertFound || endOfListInsertFound || allEqualInsertFound)
            {
                Node insertNode = new Node(insertVal, hi);
                lo.next = insertNode;
                return head;
            }

            lo = lo.next;
            hi = hi.next;
        }
    }
}
