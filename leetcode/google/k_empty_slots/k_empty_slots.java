import java.util.Arrays;

class Solution
{
    public static int kEmptySlots(int[] flowers, int k)
    {
        // Put the flowers in the correct positions
        int positions [] = new int [flowers.length];
        for(int i = 0; i < flowers.length; i++)
        {
            positions[flowers[i] - 1] = i + 1;
        }

        int left = 0;
        int right = k + 1;
        int res = Integer.MAX_VALUE;
        for(int i = 0;  right < positions.length; i++)
        {
            else if(i == right)
            {
                res = Math.min(res, Math.max(positions[left], positions[right]));
                left = i;
                right = left + k + 1;
            }
            if(positions[i] < positions[left] || positions[i] < positions[right])
            {
                left = i;
                right = left + k + 1;
            }
        }

        if(res == Integer.MAX_VALUE)
        {
            return -1;
        }
        else
        {
            return res;
        }
    }

    public static void main(String[] args)
    {
        int [] test1 = new int [] {1, 3, 2};
        int k = 1;
        System.out.println(kEmptySlots(test1, k));

        int [] test2 = new int [] {6, 5, 8, 9, 7, 1, 10, 2, 3, 4};
        k = 2;
        System.out.println(kEmptySlots(test2, k));
    }
}
