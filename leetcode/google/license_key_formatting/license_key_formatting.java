import java.lang.StringBuilder;

class Solution
{
    public String licenseKeyFormatting(String S, int K)
    {
        // Algorithm: Iterate backwards through the String.
        // group letters together in groups of K
        // Use a string builder to make this easier
        StringBuilder license = new StringBuilder();

        int counter = 0;
        for(int i = S.length() - 1; i >= 0; i--)
        {
            char c = S.charAt(i);
            if(c == '-')
            {
                continue;
            }
            else
            {
                if(counter == K)
                {
                    license.insert(0, "-");
                    counter = 0;
                }
                
                // Remember to change the character to an uppercase value
                if(c >= 'a' && c <= 'z')
                    c -= ('a' - 'A');
                
                license.insert(0, c);
                counter++;
            }
        }

        return license.toString();
    }
}
