import java.util.HashSet;

class Solution
{
    public int lengthOfLongestSubstringKDistinct(String s, int k)
    {
        // Null check
        if(s == null || s.equals(""))
            return 0;

        HashSet<Character> used = new HashSet<Character>(); // Keep track of which characters were used
        int [] occ = new int [256];                         // Keep track of occurences in this substring for each character
        char[] chars = s.toCharArray();                     // Char array representation of this string
        int max = 0;                                        // Max length of substring, result of this method

        // Start of substring and end of substring
        int start = 0;
        int end = 0;
        while(end < chars.length)
        {
            char c = chars[end];

            // Add to hashset, does nothing if already in hashset
            // Increment occurence count for this character by 1
            used.add(c);
            occ[(int) c] += 1;

            while(used.size() > k)
            {
                // We need to decrease the size of the hashset.
                // Increment start to the point where used is small enough
                occ[(int) chars[start]] -= 1;
                if(occ[(int) chars[start]] == 0)
                {
                    used.remove(chars[start]);
                }
                start += 1;
            }

            // We've added a character and need to see if we have a new max size
            max = Math.max(max, end - start + 1);
            end += 1;
        }

        return max;
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution s = new Solution();
        String str = "eceba";
        int k = 2;
        System.out.println(s.lengthOfLongestSubstringKDistinct(str, k));

        str = "aa";
        k = 1;
        System.out.println(s.lengthOfLongestSubstringKDistinct(str, k));
    }
}
