import java.util.*;

class Solution
{
    public int longestUnivaluePath(TreeNode root)
    {
        return find(root, -1, 0);
    }

    public int find(TreeNode cur, int pathVal, int L)
    {
        if(cur == null)
            return L;

        if(pathVal == cur.val)
            L += 1;
        else
            L = 0;

        // Check for an elbow case. An elbow case is where the longest path extends
        // from the left subtree into the right subtree through this node.
        int elbow = 0;
        if(cur.left != null && cur.right != null && cur.val == cur.left.val && cur.val == cur.left.val)
        {
            elbow = dfs(cur.left, 0, cur.val) + dfs(cur.right, 0, cur.val);
        }

        return max3(elbow, find(cur.left, cur.val, L), find(cur.right, cur.val, L));
    }

    // This function returns the maximum path value with the value passed in
    public int dfs(TreeNode cur, int L, int val)
    {
        if(cur == null || val != cur.val)
            return L;
        else
            return Math.max(dfs(cur.left, L + 1, val), dfs(cur.right, L + 1, val));
    }

    // Return the maximum of 3 values
    public int max3(int a, int b, int c)
    {
        return Math.max(a, Math.max(b, c));
    }
}
