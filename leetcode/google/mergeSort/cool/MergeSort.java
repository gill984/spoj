package cool;

import java.util.Arrays;

class MergeSort
{
    public void sortInPlace(int[] arr)
    {
        partition(0, arr.length - 1, arr);
    }
    
    public void partition(int lo, int hi, int[] arr)
    {
        if(lo >= hi)
            return;
        
        // Form left partition
        int mid = lo + (hi - lo) / 2;
        partition(lo, mid, arr);
        
        // Form right partition
        partition(mid + 1, hi, arr);
        
        // Merge left and right
        merge(lo, mid, hi, arr);
    }
    
    public void merge(int lo, int mid, int hi, int[] arr)
    {
        for(int i = 0; i <= hi; i++)
        {
            System.out.print(arr[i] + ", ");
        }
        System.out.println();
        System.out.println("Hi: " + hi + ", Lo: " + lo + ", Mid: " + mid);
        
        if(lo >= hi) return;
        
        // smallest values start at lo and mid + 1
        // [n_lo, .., n_mid] [n_mid+1, .., n_hi]
        int leftPtr = lo;
        int rightPtr = mid + 1;
        int [] temp = new int[hi - lo + 1];
        
        for(int i = 0; i < temp.length; i++)
        {
            if(rightPtr < arr.length && arr[leftPtr] >= arr[rightPtr])
            {
                temp[i] = arr[leftPtr++];
            }
            else
            {
                temp[i] = arr[rightPtr++];
            }
        }
        
        for(int i = 0; i < temp.length; i++)
        {
            arr[i + lo] = temp[i];
        }
    }
}

class Main
{
    public static void main(String [] args)
    {
        MergeSort m = new MergeSort();
        int [] arr  = {7, 11, 5, 2, 12, 1, 0};
        
        m.sortInPlace(arr);
        System.out.println(Arrays.toString(arr));
    }
}
