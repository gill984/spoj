package google;

class ListNode
{
    int val;
    ListNode next;

    ListNode(int x)
    {
        val = x;
    }

    public String toString()
    {
        return "" + this.val;
    }
}

/**
 * Definition for singly-linked list. public class ListNode { int val; ListNode
 * next; ListNode(int x) { val = x; } }
 */
class Solution
{
    public ListNode mergeKLists(ListNode[] lists)
    {
        return sortByGroup(lists, 0, lists.length - 1);
    }
    
    private ListNode sortByGroup(ListNode[] lists, int start, int end)
    {
        System.out.println("start: " + start);
        System.out.println("end: " + end);
        if(start == end) return lists[start];
        
        int middle = (start + end) / 2;
        
        ListNode left = sortByGroup(lists, start, middle);
        ListNode right = sortByGroup(lists, middle + 1, end);
        
        return merge(left, right);
    }
    
    private ListNode merge(ListNode left, ListNode right)
    {
        if(left == null) return right;
        if(right == null) return left;
        
        if(left.val < right.val)
        {
            // Make left point to the next highest node
            left.next = merge(left.next, right);
            return left;
        }
        else
        {
            right.next = merge(left, right.next);
            return right;
        }        
    }

}

class Main
{
    public static void main(String[] args)
    {
        ListNode[] lists = new ListNode[3];

        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(1);
        ListNode n3 = new ListNode(2);

        lists[0] = n1;
        lists[1] = n2;
        lists[2] = n3;

        n1.next = new ListNode(4);
        n1 = n1.next;
        n1.next = new ListNode(5);

        n2.next = new ListNode(3);
        n2 = n2.next;
        n2.next = new ListNode(4);

        n3.next = new ListNode(6);

        Solution sol = new Solution();
        ListNode res = sol.mergeKLists(lists);
        while (res != null)
        {
            System.out.println(res);
            res = res.next;
        }
    }
}