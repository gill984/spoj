import java.util.Arrays;

class Solution
{
    public static String nextClosestTime(String time)
    {
        // Given a time represented in the format "HH:MM", ofrm the next closest time by reusing the current digits.
        // There is no limit on how many times a digit can be reused.
        //
        // Assume the given input string is always valid.
        // For example, "01:34", "12:09" are all valid. "1:34", "12:9" are all invalid.
        //
        // The returned time must be after the passed in time. I.e., do not return a time which occurred previously.
        //
        // I think brute forcing will take 4^4 time, 256 different combinations to check at constant time.
        int[] vals = new int[4];
        String min_time = "";
        int min = 25 * 60; 

        vals[0] = Integer.parseInt(time.substring(0,1));
        vals[1] = Integer.parseInt(time.substring(1,2));
        vals[2] = Integer.parseInt(time.substring(3,4));
        vals[3] = Integer.parseInt(time.substring(4,5));

        for(int i = 0; i < vals.length; i++)
        {
            for(int j = 0; j < vals.length; j++)
            {
                for(int k = 0; k < vals.length; k++)
                {
                    for(int l = 0; l < vals.length; l++)
                    {
                        String next = "" + vals[i] + vals[j] + ":" + vals[k] + vals[l];
                        if(is_valid_time(next))
                        {
                            int minutes = time_difference(time, next);
                            if(minutes < min)
                            {
                                min = minutes;
                                min_time = next;
                            }
                        }

                    }
                }
            }
        }

        return min_time;
    }

    // Check to see if the passed in time is a valid military time for this problem
    public static boolean is_valid_time(String time)
    {
        int hours = Integer.parseInt(time.substring(0,2));
        int minutes = Integer.parseInt(time.substring(3));

        // In military time, hours cannot exceed 24 and minutes cannot exceed 60
        if(hours >= 24 || minutes >= 60)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // Check the amount of time between 2 times
    // Assumes both times are valid
    public static int time_difference(String prev, String next)
    { 
        int prev_hours = Integer.parseInt(prev.substring(0,2));
        int prev_minutes = Integer.parseInt(prev.substring(3));

        int next_hours = Integer.parseInt(next.substring(0,2));
        int next_minutes = Integer.parseInt(next.substring(3));

        int diff_hours = next_hours - prev_hours;
        int diff_minutes = next_minutes - prev_minutes;

        // Check to see if we have transitioned to the next day
        // Alternatively, we could add 24 to the next hours time instead.
        /*
         * System.out.println("next hours: " + next_hours);
         * System.out.println("next minutes: " + next_minutes);
         * System.out.println("Diff hours: " + diff_hours);
         * System.out.println("Diff minutes: " + diff_minutes);
         */
        if(diff_hours < 0 || (diff_hours == 0 && diff_minutes <= 0))
        {
            diff_hours += 24;
        }

        // Calculate the difference in time, after the previous statement, we are guaranteed to be in the same day
        return diff_hours * 60 + diff_minutes;
    }

    public static void main(String[] args)
    {
        String test_1 = "19:34";
        String answer_1 = "19:39";
        System.out.println(nextClosestTime(test_1));
        assert nextClosestTime(test_1).equals(answer_1);

        String test_2 = "23:59";
        String answer_2 = "22:22";
        System.out.println(nextClosestTime(test_2));
        assert nextClosestTime(test_2).equals(answer_2);
    }
}
