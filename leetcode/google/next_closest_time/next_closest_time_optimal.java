import java.util.Arrays;

class Solution
{
    public static String nextClosestTime(String time)
    {
        // Given a time represented in the format "HH:MM", ofrm the next closest time by reusing the current digits.
        // There is no limit on how many times a digit can be reused.
        //
        // Assume the given input string is always valid.
        // For example, "01:34", "12:09" are all valid. "1:34", "12:9" are all invalid.
        //
        // The returned time must be after the passed in time. I.e., do not return a time which occurred previously.
        //
        // Optimized solution by following the following algorithm
        // From right to left attempt to put a bigger number which still keeps the time valid.
        // Once a bigger number fits, put the smallest number possible into all values to the right.
        // If the end is reached and no bigger values can be slotted in, use the smallest number for each value.
        int [] vals = new int[4];
        int [] min_time_arr = new int[4];
        String min_time = "";
        
        vals[0] = Integer.parseInt(time.substring(0,1));
        vals[1] = Integer.parseInt(time.substring(1,2));
        vals[2] = Integer.parseInt(time.substring(3,4));
        vals[3] = Integer.parseInt(time.substring(4,5));

        int [] time_arr = new int[4];
        for(int i = 0; i < time_arr.length; i++)
        {
            time_arr[i] = vals[i];
        }

        // Sort the values so that we can always use the smallest number which is big enough to cause time to advance
        Arrays.sort(vals);

        // From right to left perform the algorithm described above
        for(int i = time_arr.length - 1; i >= 0; i--)
        {
            for(int j = 0; j < vals.length; j++)
            {
                if(vals[j] > time_arr[i])
                {
                    
                    // Attempt to create a valid time which is as small as possible
                    for(int k = 0; k < min_time_arr.length; k++)
                    {
                        if(k < i)
                        {
                            min_time_arr[k] = time_arr[k];
                        }
                        else if(k == i)
                        {
                            min_time_arr[k] = vals[j];
                        }
                        else
                        {
                            min_time_arr[k] = vals[0];
                        }
                    }


                    min_time = "" + min_time_arr[0] + "" + min_time_arr[1] + ":" + min_time_arr[2] + min_time_arr[3];
                    if(is_valid_time(min_time))
                    {
                        return min_time;
                    }
                }
            }
        }

        min_time = "" + vals[0] + "" + vals[0] + ":" + vals[0] + vals[0];
        return min_time;
    }

    // Check to see if the passed in time is a valid military time for this problem
    public static boolean is_valid_time(String time)
    {
        int hours = Integer.parseInt(time.substring(0,2));
        int minutes = Integer.parseInt(time.substring(3));

        // In military time, hours cannot exceed 24 and minutes cannot exceed 60
        if(hours >= 24 || minutes >= 60)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static void main(String[] args)
    {
        String test_1 = "19:34";
        String answer_1 = "19:39";
        System.out.println(nextClosestTime(test_1));
        assert nextClosestTime(test_1).equals(answer_1);

        String test_2 = "23:59";
        String answer_2 = "22:22";
        System.out.println(nextClosestTime(test_2));
        assert nextClosestTime(test_2).equals(answer_2);
    }
}
