class Solution
{
    public boolean isOneEditDistance(String s, String t)
    {
        // Algorithm
        // Check the lengths of the 2 strings
        // If more than one apart, return false, need more than one edit to match strings
        // If they are 1 apart, iterate through the smaller and biger string looking for the first mismatch
        //  Once the mismatch is found, add big[i] to small at index i, then check if the 2 strings are now equal
        //  if we get through the entire smaller string length, return true, we would end up adding the last character of big
        // If same length, iterate through and count differences, if > 1, return false
        if(s == null || t == null)
            return false;
        if(s.equals(t))
            return false;

        StringBuilder big;
        StringBuilder small;

        // Check lengths and determine which is big and which is small
        if(s.length() > t.length())
        {
            small = new StringBuilder(t);
            big = new StringBuilder(s);
        }
        else
        {
            small = new StringBuilder(s);
            big = new StringBuilder(t);
        }

        if(big.length() - small.length() > 1)
        {
            return false;
        }
        else
        {
            if(small.toString().equals(""))
            {
                return true;
            }

            // Index into small and index into big
            int idx = 0;
            while(idx < big.length())
            {
                // If index ever gets past small's length, then they are 1 edit distance apart
                if(idx == small.length()) return true;

                char bc = big.charAt(idx);
                char sc = small.charAt(idx);

                if(bc != sc)
                {
                    big.deleteCharAt(idx);
                    if(big.length() != small.length())
                    {
                        small.deleteCharAt(idx);
                    }
                    break;
                }
                idx += 1;
            }
            return big.toString().equals(small.toString());
        }
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution sol = new Solution();
        String s = "ab";
        String t = "acb";
        System.out.println(sol.isOneEditDistance(s, t));

        s = "cab";
        t = "ad";
        System.out.println(sol.isOneEditDistance(s, t));

        s = "1203";
        t = "1213";
        System.out.println(sol.isOneEditDistance(s, t));

        s = "ab";
        t = "a";
        System.out.println(sol.isOneEditDistance(s, t));

        s = "";
        t = "a";
        System.out.println(sol.isOneEditDistance(s, t));
    }
}
