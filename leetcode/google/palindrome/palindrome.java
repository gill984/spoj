class Solution
{
    public boolean isPalindrome(String s)
    {
        // Algorithm
        // Use 2 pointers, one at start and one at end
        // if a pointer is pointing to a character which is a space, skip it.
        // Check if the characters pointed to by the pointers are equal, ignoring case
        // If not return false
        // If yes continue until start >= end
        if(s == null || s.equals(""))
            return true;

        char [] arr = s.toCharArray();
        int lo = 0;
        int hi = arr.length - 1;

        // Move hi and lo to the next spot with no spaces
        while(lo < arr.length && !isAlphaNumeric(arr[lo])) lo++;
        while(hi >= 0 && !isAlphaNumeric(arr[hi])) hi--;


        while(lo < hi)
        {
            char loC = arr[lo];
            char hiC = arr[hi];

            // Make everything lowercase for comparison
            if(loC >= 'A' && loC <= 'Z')
                loC += 'a' - 'A';
            if(hiC >= 'A' && hiC <= 'Z')
                hiC += 'a' - 'A';

            // Perform comparison
            if(hiC != loC)
            {
                return false;
            }

            // Update pointers
            lo++;
            hi--;

            // Move hi and lo to the next spot with no spaces
            while(lo < arr.length && !isAlphaNumeric(arr[lo])) lo++;
            while(hi >= 0 && !isAlphaNumeric(arr[hi])) hi--;
        }

        return true;
    }

    private boolean isAlphaNumeric(char c)
    {
        if(c >= '0' && c <= '9') return true;
        else if(c >= 'A' && c <= 'Z') return true;
        else if(c >= 'a' && c <= 'z') return true;
        else return false;
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution sol = new Solution();
        String s = "A man, a plan, a canal: Panama";
        System.out.println(sol.isPalindrome(s));

        s = "race a car";
        System.out.println(sol.isPalindrome(s));
    }
}
