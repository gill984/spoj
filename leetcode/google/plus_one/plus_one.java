class Solution {
    public int[] plusOne(int[] digits) {
        if(digits == null)
            return digits;
        
        boolean done = false;
        for(int i = digits.length -1; i >= 0 && !done; i--)
        {
            digits[i] += 1;
            if(digits[i] > 9)
                digits[i] -= 10;
            else
                done = true;
        }
        
        if(done)
        {
            return digits;
        }
        else
        {
            int [] newDigits = new int[digits.length + 1];
            newDigits[0] = 1;
            return newDigits;
        }
    }
}
