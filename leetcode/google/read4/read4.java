/* The read4 API is defined in the parent class Reader4.
   int read4(char[] buf); */
import java.util.Arrays;
public class Solution extends Reader4
{
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    public int read(char[] buf, int n)
    {
        int sum = 0;                    // Total number of bytes read so far
        char [] in = new char[4];       // Buffer to place reads from read4 into
        boolean endOfFile = false;      // If we hit the end of the file, stop. This occurs if read4 returns less than 4

        // i == number of reads performed at the start of the loop, first iteration is 0th read
        for(int i = 0; !endOfFile && sum < n; i++)
        {
            // Get the next subarray to be read
            int bytesRead = read4(in);

            // Set endOfFile flag if reached
            // if we're at the end, stop reading after this iteration
            if(bytesRead < 4) endOfFile = true;

            // How many of the bytes read in do we want to copy onto the buffer?
            // The answer is Math.min(n - sum, bytesRead)
            int copySize = Math.min(n - sum, bytesRead);
            for(int j = 0; j < copySize; j++)
                buf[i * 4 + j] = in[j];

            sum += copySize;
        }

        return sum;
    }
}
