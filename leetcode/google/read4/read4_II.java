import java.util.ArrayDeque;

public class Solution extends Reader4
{
    private Queue<Character> queue = new ArrayDeque<Character>();
    public int read(char[] buf, int n)
    {
        int sum = 0;                    // Total number of bytes read so far
        char [] in = new char[4];       // Buffer to place reads from read4 into
        boolean endOfFile = false;      // If we hit the end of the file, stop. This occurs if read4 returns less than 4
        int bufIdx = 0;                 // Index into the return buffer, buf

        while(sum < n && !queue.isEmpty())
        {
            buf[bufIdx++] = queue.poll();
            sum += 1;
        }


        for(int i = 0; !endOfFile && sum < n; i++)
        {
            int bytesRead = read4(in);
            
            if(bytesRead < 4) 
                endOfFile = true;

            int copySize = Math.min(n - sum, bytesRead);
            for(int j = 0; j < bytesRead; j++)
            {
                if(j < copySize) buf[bufIdx++] = in[j];
                else queue.offer(in[j]);
            }
            sum += copySize;
        }
        return sum;
    }
}
