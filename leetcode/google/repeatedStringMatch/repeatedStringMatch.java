class Solution
{
	public static int repeatedStringMatch(String A, String B)
	{
		// B has to start at a character in A.
		// 1. Iterate through A and find the starting character in B.
		// 2. From there see how many times A needs to be duplicated to match.
		// If no match, continue iterating through A in step 1

		// Handle error cases
		if(A == null || B == null || A.equals("") || B.equals(""))
		{
			return -1;
		}


		// 1. Iterate through A and find the starting character in B.
		for(int i = 0; i < A.length(); i++)
		{
			// If this is a possible solution,
			// 2. From there see how many times A needs to be duplicated to match.
			int repetitions = 1;
			if(A.charAt(i) == B.charAt(0))
			{
				int aIndex = i;
				for(int bIndex = 0; bIndex < B.length(); bIndex++)
				{
					if(A.charAt(aIndex) == B.charAt(bIndex))
					{
						if(bIndex == B.length() - 1)
						{
							return repetitions;
						}
						else
						{
							aIndex += 1;
							if(aIndex >= A.length())
							{
								aIndex = aIndex % A.length();
								repetitions += 1;
							}
						}
					}
					else
					{
						break;
					}
				}
			}
		}

		// If there is no solution, return -1 per instructions.
		return -1;
	}

	public static void main(String[] args)
	{
		String A = "abcd";
		String B = "cdabcdab";
		assert 3 == repeatedStringMatch(A, B);
		System.out.println(repeatedStringMatch(A, B));
	}
}
