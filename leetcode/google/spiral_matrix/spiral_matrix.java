import java.util.List;
import java.util.ArrayList;

class Main
{
    public static void main(String [] args)
    {
        Solution s = new Solution();
        int [][] matrix = {
            { 1, 2, 3 },
            { 4, 5, 6 },
            { 7, 8, 9 },
        };

        System.out.println(s.spiralOrder(matrix));

        int [][] matrix2 = {
            { 1, 2, 3, 4 },
            { 5, 6, 7, 8 },
            { 9, 10, 11, 12 },
        };

        System.out.println(s.spiralOrder(matrix2));
    }
}

class Solution
{
    private final static int RIGHT = 0;
    private final static int DOWN = 1;
    private final static int LEFT = 2;
    private final static int UP = 3;
    private final static int ROW = 0;
    private final static int COL = 1;

    // Array of increments, order is right, down, left, up
    private final static int [][] inc = {
        {0, 1}, // RIGHT
        {1, 0}, // DOWN
        {0, -1},// LEFT
        {-1, 0},// UP
    };


    public List<Integer> spiralOrder(int[][] matrix)
    {
        // Order of spiral is right -> down -> left -> up
        ArrayList<Integer> res = new ArrayList<Integer>();

        if(matrix == null || matrix.length == 0 || matrix[0] == null || matrix[0].length == 0)
            return res;

        // Bounds for the spiral print, update as we go
        int maxRow = matrix.length - 1;
        int maxCol = matrix[0].length - 1;
        int minRow = 0;
        int minCol = 0;

        // Initial direction is always to the right
        // Starting point is always (0,0)
        int dir = RIGHT;
        int row = 0;
        int col = 0;

        // Keep adding elements until we've added them all
        while(matrix.length * matrix[0].length > res.size())
        {
            while(row <= maxRow && row >= minRow && col >= minCol && col <= maxCol)
            {
                // Add this element to the list
                res.add(matrix[row][col]);

                // Increment pointers in the correct direction
                row += inc[dir][ROW];
                col += inc[dir][COL];
            }

            // Go back 1 in the current direction to stay in bounds
            row -= inc[dir][ROW];
            col -= inc[dir][COL];

            // Restrict the bounds of the array based on the direction
            if(dir == RIGHT) minRow += 1;
            else if(dir == DOWN) maxCol -= 1;
            else if(dir == LEFT) maxRow -= 1;
            else if(dir == UP) minCol += 1;

            // Go in the next direction
            dir = (dir + 1) % 4;

            // Need to perform one increment before we start, so we don't re-use the last element
            // Luckily dir was just updated, so just increment again.
            row += inc[dir][ROW];
            col += inc[dir][COL];
        }

        return res;
    }
}
