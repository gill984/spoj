package array_intersection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

class Solution
{
    public int[] intersection(int[] nums1, int[] nums2)
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        HashSet<Integer> set = new HashSet<Integer>();

        for (int i : nums1)
        {
            set.add(i);
        }

        for (int i : nums2)
        {
            if (set.contains(i))
            {
                list.add(i);
                set.remove(i);
            }
        }

        int [] res = new int[list.size()];
        for(int i = 0; i < list.size(); i++)
            res[i] = list.get(i);
        
        return res;

    }
}

class Solution2
{
    public int[] intersection(int[] nums1, int[] nums2)
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        
        int idx1 = 0;
        int idx2 = 0;
        while(idx1 < nums1.length && idx2 < nums2.length)
        {
            if(nums1[idx1] == nums2[idx2])
            {
                int val = nums1[idx1];
                list.add(val);
                while(idx1 < nums1.length && nums1[idx1] == val) idx1 += 1;
                while(idx2 < nums2.length && nums2[idx2] == val) idx2 += 1;
            }
            else if(nums1[idx1] < nums2[idx2])
            {
                idx1 += 1;
            }
            else
            {
                idx2 += 1;
            }
        }

        int [] res = new int[list.size()];
        for(int i = 0; i < list.size(); i++)
            res[i] = list.get(i);
        
        return res;

    }
}

class Main
{
    public static void main(String[] args)
    {
        int[] arr1 = {1, 2, 2, 1};
        int[] arr2 = {2, 2};
        Solution2 sol = new Solution2();
        System.out.println(Arrays.toString(sol.intersection(arr1, arr2)));

        int[] arr3 = {4, 9, 5};
        int[] arr4 = {9, 4, 9, 8, 4};
        System.out.println(Arrays.toString(sol.intersection(arr3, arr4)));
    }
}
