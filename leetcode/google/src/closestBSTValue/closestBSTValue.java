package closestBSTValue;

class Solution
{
    public int closestValue(TreeNode root, double target)
    {
        return closestValue(root, target, root.val);
    }

    public int closestValue(TreeNode root, double target, int closest)
    {
        if (root == null)
            return closest;
        
        int newClosest = closest;
        // Search right subtree if root.val > closest
        if((double)root.val < target)
        {
            newClosest = closestValue(root.right, target, closest);
        }
        else if((double)root.val > target)
        {
            newClosest = closestValue(root.left, target, closest);
        }
        
        if(Math.abs((double)newClosest - target) < Math.abs((double)closest - target))
        {
            closest = newClosest;
        }
        
        return closest;
    }
}

class TreeNode
{
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x)
    {
        val = x;
    }
}
