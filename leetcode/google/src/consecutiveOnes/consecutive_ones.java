package consecutiveOnes;

class Solution
{
    public int findMaxConsecutiveOnes(int[] nums)
    {
        if(nums == null)
            return 0;
        
        int cur = 0;        // Current running consecutive ones
        int max = 0;        // Current maximum
        int prevZero = 0;  // Index of last 0
        
        for(int i = 0; i < nums.length; i++)
        {
            if(nums[i] == 1)
            {
                cur += 1;
            }
            else
            {
                // Set cur to new length which is:
                // [prevZero, i] whose length is (i - prevZero + 1)
                cur = (i - prevZero + 1);
                prevZero = i + 1;
//                System.out.println("Cur: " + cur);
//                System.out.println("PrevZero: " + prevZero);
            }
            max = Math.max(cur, max);
        }
        
        return max;
    }
}

class Main
{
    public static void main(String[] args)
    {
        int [] arr1 = {1, 1, 0, 1, 1, 1};
        int [] arr2 = {1, 0, 1};
        int [] arr3 = {1, 1, 1, 1, 0, 1};
        int [] arr4 = {};
        int [] arr5 = {1};
        int [] arr6 = {0};
        int [] arr7 = {1, 0, 1, 1, 0, 1};
        Solution sol = new Solution();
        System.out.println(sol.findMaxConsecutiveOnes(arr1));
        System.out.println(sol.findMaxConsecutiveOnes(arr2));
        System.out.println(sol.findMaxConsecutiveOnes(arr3));
        System.out.println(sol.findMaxConsecutiveOnes(arr4));
        System.out.println(sol.findMaxConsecutiveOnes(arr5));
        System.out.println(sol.findMaxConsecutiveOnes(arr6));
        System.out.println(sol.findMaxConsecutiveOnes(arr7));
    }
}
