package eval_div;

import java.util.Arrays;
import java.util.HashMap;

class Solution
{
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries)
    {
        HashMap<String, Integer> stringToIndex = new HashMap<String, Integer>();
        HashMap<Integer, String> indexToString = new HashMap<Integer, String>();

        int numNodes = 0;
        for (String[] vars : equations)
        {
            for (String var : vars)
            {
                if (!stringToIndex.containsKey(var))
                {
                    stringToIndex.put(var, numNodes);
                    indexToString.put(numNodes, var);
                    numNodes += 1;
                }
            }
        }

        // Build adjacency matrix
        double[][][] D = new double[numNodes + 1][numNodes][numNodes];

        // Put 0 in each self edge and infinity in the rest
        for (int k = 0; k < numNodes + 1; k++)
        {
            for (int i = 0; i < numNodes; i++)
            {
                for (int j = 0; j < numNodes; j++)
                {
                    // if (i == j)
                    // D[k][i][j] = 0;
                    // else
                    D[k][i][j] = Double.MAX_VALUE;
                }
            }
        }

        for (int i = 0; i < equations.length; i++)
        {
            String num = equations[i][0];
            String den = equations[i][1];
            double val = values[i];

            D[0][stringToIndex.get(num)][stringToIndex.get(den)] = val;
            D[0][stringToIndex.get(den)][stringToIndex.get(num)] = 1.0 / val;
        }

        // Matrix is now in beginning state of Floyd Warshall
        for (int k = 0; k < numNodes; k++)
            for (int i = 0; i < numNodes; i++)
                for (int j = 0; j < numNodes; j++)
                    if (i == j)
                        continue;
                    else
                        D[k + 1][i][j] = Math.min(D[k][i][j], D[k][i][k] * D[k][k][j]);

        double[] result = new double[queries.length];

        for (int i = 0; i < queries.length; i++)
        {
            if (!stringToIndex.containsKey(queries[i][0]) || !stringToIndex.containsKey(queries[i][1]))
            {
                result[i] = -1;
            } else
            {

                int from = stringToIndex.get(queries[i][0]);
                int to = stringToIndex.get(queries[i][1]);
                if (from == to)
                {
                    result[i] = 1.0;
                } else if (D[numNodes][from][to] > 1E100)
                {
                    result[i] = -1;
                } else
                {
                    result[i] = D[numNodes][from][to];
                }
            }
        }

        return result;
    }
}

class Main
{
    public static void main(String[] args)
    {
        String[][] equations = { { "a", "b" }, { "b", "c" } };
        double[] values = { 2.0, 3.0 };
        String[][] queries = { { "a", "b" }, { "a", "c" } };

        Solution sol = new Solution();
        double[] result = sol.calcEquation(equations, values, queries);
        System.out.println(Arrays.toString(result));

        String[][] equations2 = { { "x1", "x2" }, { "x2", "x3" }, { "x1", "x4" }, { "x2", "x5" } };
        double[] values2 = { 3.0, 0.5, 3.4, 5.6 };
        String[][] queries2 = { { "x2", "x4" }, { "x1", "x5" }, { "x1", "x3" }, { "x5", "x5" }, { "x5", "x1" },
                { "x3", "x4" }, { "x4", "x3" }, { "x6", "x6" }, { "x0", "x0" } };
        double[] result2 = sol.calcEquation(equations2, values2, queries2);
        System.out.println(Arrays.toString(result2));
    }
}
