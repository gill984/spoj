package first_missing_positive;

import java.util.Arrays;

class Solution
{
    public int firstMissingPositive(int[] nums)
    {
        if (nums == null || nums.length == 0)
            return 1;
        
        // Goal of this array is to place elements at the index = value - 1
        // Since the maximum value we care about is the total length of the array and
        // the minimum
        // Value we care about is 1, there are just enough indexes to do this
        for (int i = 0; i < nums.length; i++)
        {
            int num = nums[i];
            if (num >= 1 && num <= nums.length)
            {
                // i is current index, want to put at newIdx
                // Correct slot is: newIdx = value - 1
                int newIdx = num - 1;
                if (i > newIdx)
                {
                    // Don't need to swap in this case, just put the value at the new index
                    // The reason we don't need to check is because the previous index was already
                    // set, so the value was already evaluated
                    nums[newIdx] = num;
                }
                else if (i < newIdx)
                {
                    // In this case, need to swap and beware of infinite loop when the values are
                    // equal
                    int temp = nums[newIdx];

                    // If the values are equal, just set the current index to something which will
                    // be ignored
                    if (temp == num)
                        nums[i] = 0;
                    else
                    {
                        nums[newIdx] = num;
                        nums[i] = temp;
                        
                        // rerun since this is a new value which could be valid
                        i--;
                    }
                }
            }
        }

        // Array has been set up so that we can loop through one more time and if we
        // find an element for which arr[i] != i+1, then return i+1
        for (int i = 0; i < nums.length; i++)
            if (nums[i] != i + 1)
                return i + 1;

        return nums.length + 1;
    }
}

class Main
{
    public static void main(String[] args)
    {
        Solution sol = new Solution();
        int[] arr1 = {1, 2, 0};
        int[] arr2 = {3, 4, -1, 1};
        int[] arr3 = {7, 8, 9, 11, 12};
        int[] arr4 = {1, 2, 3, 4};
        int[] arr5 = {4, 3, 2, 1};
        
        // System.out.println(sol.shortestPalindrome("abcd"));
        System.out.println(sol.firstMissingPositive(arr1));
        System.out.println(sol.firstMissingPositive(arr2));
        System.out.println(sol.firstMissingPositive(arr3));
        System.out.println(sol.firstMissingPositive(arr4));
        System.out.println(sol.firstMissingPositive(arr5));
    }
}
