package hashTable;

public class HashTable
{
    private int size;
    private int capacity;
    Integer [] table;
    
    public HashTable(int s)
    {
        this.size = s;
        this.capacity = 0;
        Integer[] table = new Integer[this.size];
    }
    
    
    public boolean insert(int i)
    {
        if(capacity == size)
        {
            System.out.println("Hash table is full, cannot insert element: " + i);
            return false;
        }
        
        int key = i % size;
        while(table[key] != null)
        {
            key = (key + 1) % size;
        }
        table[key] = i;
        capacity += 1;
        System.out.println("Put element " + i + " at index " + key);
        
        return true;
    }
    
    public boolean remove(int i)
    {
        if(capacity == 0)
        {
            System.out.println("Cannot remove element from empty hash table");
            return false;
        }
        
        int key = i % size;
        for(int count = 0; table[key] != null && count < size; count++)
        {
            if(table[key] == i)
            {
                table[key] = null;
                return true;
            }
            else
            {
                key = (key + 1) % size;
            }
        }
        
        // If we reach this point, we hit a null or have gone through every element
        return false;
    }
}

class Main
{
    public static void main(String[] args)
    {
        HashTable h = new HashTable(11);
        
    }
}
