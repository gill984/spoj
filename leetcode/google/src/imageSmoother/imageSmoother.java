package imageSmoother;


import java.util.Arrays;

class Solution
{
    private final static int ROW = 0;
    private final static int COL = 1;
    private final static int[][] nbrPos = {{-1, -1}, // Top left
            {-1, 0}, // Straight Above
            {-1, 1}, // Top Right
            {0, 1}, // Straight Right
            {1, 1}, // Bottom Right
            {1, 0}, // Straight Below
            {1, -1}, // Bottom Left
            {0, -1} // Straight Left
    };

    public int[][] imageSmoother(int[][] M)
    {
        if(M == null)
            return null;
        
        for(int i = 0; i < M.length; i++)
        {
            for(int j = 0; j < M[0].length; j++)
            {
                M[i][j] += averageNeighbors(i, j, M) << 8;
            }
        }
        
        for(int i = 0; i < M.length; i++)
        {
            for(int j = 0; j < M[0].length; j++)
            {
                M[i][j] = M[i][j] >>> 8;
            }
        }
        
        return M;
    }
    
    // Return average value for this cell and all neighbors
    private int averageNeighbors(int r, int c, int[][] M)
    {
        int numerator = M[r][c];    // Running sum of this cell plus all neighbor vals
        int denominator = 1;        // Running sum of the number of cells to average
        
        for(int[] arr : nbrPos)
        {
            int nbrRow = r + arr[ROW];
            int nbrCol = c + arr[COL];
            
            // Bounds check
            if(nbrRow >= 0 && nbrRow < M.length && nbrCol >= 0 && nbrCol < M[0].length)
            {
                numerator += M[nbrRow][nbrCol];
                denominator += 1;
            }
        }
        
        return numerator / denominator;
    }
}

class Main
{
    public static void main(String[] args)
    {
        Solution sol = new Solution();
        int[][] arr = {{1, 1, 1}, {1, 0, 1}, {1, 1, 1}};
        System.out.println(Arrays.deepToString(sol.imageSmoother(arr)));
    }
}
