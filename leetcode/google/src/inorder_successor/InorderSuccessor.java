package inorder_successor;

import java.util.ArrayDeque;
import java.util.Stack;

class Solution
{  

    
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p)
    {
        return inorderIter(root, p.val);
    }
    
    public TreeNode inorderIter(TreeNode root, int p)
    {
        boolean returnNext = false;
        ArrayDeque<TreeNode> stack = new ArrayDeque<TreeNode>();
        stack.push(root);
        while(!stack.isEmpty())
        {
            TreeNode n = stack.peek();
            
            if(n == null)
            {
                stack.pop();
                continue;
            }
            System.out.println(n.val);
            

            // Check Left
            if(n.val > p && n.left != null)
            {
                stack.push(n.left);
                continue;
            }
            
            // Check this node
            if(n.val == p)
            {
                System.out.println(returnNext);
                returnNext = true; 
            }
            else if(returnNext)
            {
                return n;
            }
            stack.pop();
            
            // Check right            
            if(n.val <= p && n.right != null)
            {
                stack.push(n.right);
            }
        }
        
        return null;
    }
}

class TreeNode
{
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x)
    {
        val = x;
    }
    
    public String toString()
    {
        return "" + val;
    }
}