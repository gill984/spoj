package is_BST;

class Solution
{
    public boolean isValidBST(TreeNode root)
    {
        return isValidBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    
    private boolean isValidBST(TreeNode root, int min, int max)
    {
        if(root == null)
            return true;
        
        if(root.val <= max && root.val >= min)
        {
            return isValidBST(root.left, min, root.val) && isValidBST(root.right, root.val, max); 
        }
        else
        {
            return false;
        }
    }
}

class Main
{
    public static void main(String[] args)
    {
        
    }
}

class TreeNode
{
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x)
    {
        val = x;
    }
}