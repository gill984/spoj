package robot;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

class Solution
{
    public static final int UP = 0;
    public static final int LEFT = 1;
    public static final int DOWN = 2;
    public static final int RIGHT = 3;
    
    private static final int ROW = 0;
    private static final int COL = 1;

    private static final int[][] moveArr = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};

    private int direction = UP;
    private Coordinate position = new Coordinate(0, 0);

    private Map<Integer, HashSet<Integer>> visited = new HashMap<Integer, HashSet<Integer>>();

    public void cleanRoom(Robot robot)
    {
        ArrayDeque<Coordinate> stack = new ArrayDeque<Coordinate>();
        Coordinate start = new Coordinate(position.row, position.col);
        stack.push(start);
        addCoordinateToMap(start);
        

        while (!stack.isEmpty())
        {
            // Peek at the first element on the queue
            // Move to this coordinate if not already on this coordinate, should be at most
            // 1 space away. Update position.
            // look to see if this element has had all directions traveled to
            // If yes then pop this element and clean this coordinate
            Coordinate moveTo = stack.peek();
            
//            System.out.println("_____Peeked coordinate on stack_____");
//            System.out.println(moveTo);
            
            boolean moved = moveDirection(moveTo, robot);
            
            // If the move did not work, we have hit a wall.
            if(!moved)
            {
                stack.pop();
                continue;
            }
            else
            {
                boolean visitedAll = true;
                for(int i = 0; i < 4; i++)
                {
                    Coordinate next = new Coordinate(moveTo.row + moveArr[i][ROW], moveTo.col + moveArr[i][COL]);
                    if(!coordinateVisited(next))
                    {
//                        System.out.println("__Pushing Coordinate onto stack__");
//                        System.out.println(next);
                        stack.push(next);
                        addCoordinateToMap(next);
                        visitedAll = false;
//                        System.out.println(visited);
                        break;
                    }
                }
                
                if(visitedAll)
                {
//                    System.out.println("__Cleaning coordinate__");
//                    System.out.println(moveTo);
                    robot.clean();
                    stack.pop();
                }
            }
        }
    }
    
    private void addCoordinateToMap(Coordinate c)
    {
        if(!visited.containsKey(c.row))
        {
            visited.put(c.row, new HashSet<Integer>());
        }
        visited.get(c.row).add(c.col);
    }
    
    private boolean coordinateVisited(Coordinate c)
    {
        if(!visited.containsKey(c.row))
        {
            return false;
        }
        return visited.get(c.row).contains(c.col);
    }

    private boolean moveDirection(Coordinate moveTo, Robot robot)
    {
        // If we are already where we want to go
        if(moveTo.equals(position))
            return true;
        
        // Move to the coordinate pCoord
        int rowDelta = moveTo.row - position.row;
        int colDelta = moveTo.col - position.col;

        if (rowDelta == 1)
        {
            faceDirection(DOWN, robot);
        }
        else if (rowDelta == -1)
        {
            faceDirection(UP, robot);
        }
        else if (colDelta == 1)
        {
            faceDirection(RIGHT, robot);
        }
        else if (colDelta == -1)
        {
            faceDirection(LEFT, robot);
        }

        boolean moved = robot.move();
        if(moved)
        {
            position.row += rowDelta;
            position.col += colDelta;
//            System.out.println("__Moved to coordinate__");
        }
        else
        {
//            System.out.println("__Did not move to coordinate__");
        }
//        System.out.println(moveTo);
        return moved;
    }

    private void faceDirection(int newDirection, Robot robot)
    {
        int turns = newDirection - direction;
        if (turns > 0)
        {
            for (int i = 0; i < turns; i++)
                robot.turnLeft();
        }
        else if (turns < 0)
        {
            for (int i = 0; i > turns; i--)
                robot.turnRight();
        }
        
        direction = newDirection;
    }
}

class Coordinate
{
    public int row;
    public int col;

    public Coordinate(int r, int c)
    {
        this.row = r;
        this.col = c;
    }

    public boolean equals(Coordinate c)
    {
        return this.row == c.row && this.col == c.col;
    }
    
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        s.append("Row: " + row + "\n");
        s.append("Column: " + col + "\n");
        return s.toString();
    }
}

interface Robot
{
    // Returns true if the cell in front is open and robot moves into the cell.
    // Returns false if the cell in front is blocked and robot stays in the current
    // cell.
    public boolean move();

    // Robot will stay in the same cell after calling turnLeft/turnRight.
    // Each turn will be 90 degrees.
    public void turnLeft();

    public void turnRight();

    // Clean the current cell.
    public void clean();
}
