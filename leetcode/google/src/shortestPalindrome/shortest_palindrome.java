package shortestPalindrome;

class Solution
{
    public String shortestPalindrome(String s) 
    {       
        // Algorithm
        // ptr = s.length - 1
        // while string is not a palindrome, string = char[ptr] + string, ptr--
        // return string
        StringBuilder pal = new StringBuilder(s);
        StringBuilder rev = new StringBuilder(s);
        rev.reverse();
        
        for(int i = 0; i < s.length(); i++)
        {
            if(rev.substring(i).equals(pal.substring(0, s.length() - i)))
                return rev.substring(0, i) + pal.substring(0, s.length());
        }
        
        return "";
    }
}

class Main
{
    public static void main(String[] args)
    {
        Solution sol = new Solution();
        // System.out.println(sol.shortestPalindrome("abcd"));
        System.out.println(sol.shortestPalindrome("aacecaaa"));
    }
}
