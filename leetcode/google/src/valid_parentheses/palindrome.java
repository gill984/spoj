package valid_parentheses;

import java.util.ArrayDeque;
import java.util.Deque;

class Solution
{
    
    
    public boolean isValid(String s)
    {
        Deque<Character> stack = new ArrayDeque<Character>();
        if(s == null || s.equals(""))
        {
            return true;
        }
        
        char [] arr = s.toCharArray();
        for(int i = 0; i < arr.length; i++)
        {
            
            char c1 = arr[i];
            if(c1 == '(' || c1 == '[' || c1 == '{')
            {
                // Left brace, always add to stack
                stack.push(c1);
            }
            else if(c1 == ')' || c1 == ']' || c1 == '}')
            {
                // If the stack is empty here, we're in a bad case
                if(stack.isEmpty()) return false;
                
                // Pop stack and check if correct
                char c2 = stack.pop();
                if(c2 == '(' && c1 == ')' || c2 == '[' && c1 == ']' || c2 == '{' && c1 == '}')
                {
                    // This is a good case, continue to the next iteration
                }
                else
                {
                    // Bad case, right brace type did not match left brace type
                    return false;
                }
            }
            else
            {
                // The character wasn't valid
                return false;
            }
            
            System.out.println(stack.toString());
        }
        
        return stack.isEmpty();
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution sol = new Solution();
        System.out.println(sol.isValid("()"));
        System.out.println(sol.isValid("()[]{}"));
        System.out.println(sol.isValid("(]"));
        System.out.println(sol.isValid("([)]"));
        System.out.println(sol.isValid("{[]}"));
    }
}
