package wordSearchII;

import java.util.ArrayList;
import java.util.List;

class Solution
{
    char[][] b;
    int[] UP = { -1, 0 };
    int[] DOWN = { 1, 0 };
    int[] LEFT = { 0, -1 };
    int[] RIGHT = { 0, 1 };
    int[][] DIRECTIONS = { UP, DOWN, LEFT, RIGHT };
    int ROW = 0;
    int COL = 1;

    public List<String> findWords(char[][] board, String[] words)
    {
        b = board;
        // Build tree of character nodes
        Node root = new Node('0');
        for (int i = 0; i < words.length; i++)
        {
            Node current = root;
            for (int j = 0; j < words[i].length(); j++)
            {
                if(current.children[words[i].charAt(j) - 'a'] == null)
                {
                    current.children[words[i].charAt(j) - 'a'] = new Node(words[i].charAt(j));
                }
                current = current.children[words[i].charAt(j) - 'a'];
            }
            current.completed = words[i];
        }

        

        List<String> list = new ArrayList<String>();
        boolean[][] visited = new boolean[b.length][b[0] != null ? b[0].length : 0];
        for (int i = 0; i < board.length; i++)
        {
            for (int j = 0; j < board[i].length; j++)
            {
                dfs(i, j, root, list, visited);
            }
        }

        return list;
    }

    public void dfs(int row, int col, Node current, List<String> list, boolean[][] visited)
    {
        visited[row][col] = true;
        char c = b[row][col];
        Node next = current.children[c - 'a'];
        if (next == null)
        {
            visited[row][col] = false;
            return;
        }

        if (next.completed != null)
        {
            list.add(next.completed); // Add to list of strings found
            next.completed = null; // Avoid double counting same string
        }

        for (int i = 0; i < DIRECTIONS.length; i++)
        {
            int nextRow = row + DIRECTIONS[i][ROW];
            int nextCol = col + DIRECTIONS[i][COL];
            if (nextRow >= b.length || nextRow < 0 || nextCol >= b[row].length || nextCol < 0
                    || visited[nextRow][nextCol])
            {
                continue;
            }
            dfs(nextRow, nextCol, next, list, visited);
        }
        visited[row][col] = false;
    }
}

class Node
{
    public char id;
    public String completed;
    public Node[] children;

    public Node(char id)
    {
        this.id = id;
        children = new Node[26];
        completed = null;
    }

    public String toString()
    {
        StringBuilder out = new StringBuilder();
        out.append("ID: " + id + "\n");
        out.append("Completed: " + completed + "\n");
        return out.toString();
    }
}

class Main
{
    public static void main(String[] args)
    {
        Solution sol = new Solution();
        String[] words = { "oath", "pea", "eat", "rain" };
        char[][] board = { { 'o', 'a', 'a', 'n' }, { 'e', 't', 'a', 'e' }, { 'i', 'h', 'k', 'r' },
                { 'i', 'f', 'l', 'v' } };

        System.out.println(sol.findWords(board, words));

        String[] words2 = { "aaa" };
        char[][] board2 = { { 'a', 'a' } };
        System.out.println(sol.findWords(board2, words2));

        String[] words3 = { "ab", "cb", "ad", "bd", "ac", "ca", "da", "bc", "db", "adcb", "dabc", "abb", "acb" };
        char[][] board3 = { { 'a', 'b' }, { 'c', 'd' } };
        System.out.println(sol.findWords(board3, words3));
    }
}
