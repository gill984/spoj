class Solution
{
    public int trap(int[] height)
    {
        int curHeight = 0;      // Current highest for this iteration
        int curVol = 0;         // Current volume, will be real if another height is found which matches curHeight
        int maxFound = 0;       // Highest height found which is less than curHeight
        int idx = 0;            // Current index we are at in the height array
        int curIdx = 0;         // Index where curHeight was found
        int sum = 0;            // True sum of volume found so far

        while(idx <= height.length)
        {
            // If we hit the end of the array there are two possibilities
            // Either there is no volume to report
            // Or we need to go back and count from a lower height in order to collect the actual
            // Amount of volume collected.
            if(idx == height.length)
            {
                // If there is not volume found, just return
                if(curVol == 0)
                    return sum;
                else
                {
                    // Set this height back to the max found so far and redo from there
                    curHeight = maxFound;
                    height[curIdx] = maxFound;
                    idx = curIdx;
                    curVol = 0;
                    maxFound = 0;
                }
            }

            if(height[idx] >= curHeight)
            {
                // We have some actual volume, add to sum
                // Reset curHeight to new max
                // 0 out curVol to not double count
                sum += curVol;
                curVol = 0;
                curHeight = height[idx];
                maxFound = 0;
                curIdx = idx;
                idx++;
            }
            else
            {
                // Add volume for this height to curVol
                // Update maxFound if necessary
                curVol += curHeight - height[idx];
                maxFound = Math.max(maxFound, height[idx]);
                idx++;
            }
        }

        return sum;
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution s = new Solution();
        int [] height = {0,1,0,2,1,0,1,3,2,1,2,1};
        System.out.println(s.trap(height));
    }
}
