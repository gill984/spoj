class Solution
{
    public int trap(int[] height)
    {
        if(height == null)
            return 0;

        int max = Integer.MIN_VALUE;    // Max height found in heights
        int sum = 0;                    // Sum of all heights
        int water = 0;                  // Total amount of water

        for(int i = 0; i < height.length; i++)
        {
            max = Math.max(max, height[i]);
            sum += height[i];
        }

        // Calculate maximum possible water if both ends were max height
        water = (max * height.length) - sum;

        // Take out water by looking from the left
        int maxSoFar = 0;
        for(int i = 0; i < height.length; i++)
        {
            maxSoFar = Math.max(height[i], maxSoFar);
            water -= (max - maxSoFar);
        }

        // Same thing but from the right
        maxSoFar = 0;
        for(int i = height.length - 1; i >= 0; i--)
        {
            maxSoFar = Math.max(height[i], maxSoFar);
            water -= (max - maxSoFar);
        }

        return water;
    }
}

class Main
{
    public static void main(String [] args)
    {
        Solution s = new Solution();
        int [] height = {0,1,0,2,1,0,1,3,2,1,2,1};
        System.out.println(s.trap(height));
    }
}
