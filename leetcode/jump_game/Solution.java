class Solution {
    public boolean canJump(int[] nums) {
        // Iterate through array and keep track of furthest position we can jump to
        // if the furthest element we can jump to is less than the current index examined
        // by the iteration, return false.
        // Note that this greedy solution works here becauese if we can reach a value n,
        // we can reach all values [0, n-1] based on the jumping rules.
        int max = 0;
        for(int i = 0; i < nums.length; i++)
        {
            if(max < i)
                return false;
            
            max = Math.max(max, nums[i] + i);
        }
        
        return max >= nums.length - 1;
    }
}
