class Solution
{
    public String longestPalindrome(String s)
    {
        //System.out.println(s.substring(0,1));
        int n = s.length();
        String result = "";         // String to return
        
        if(n == 1)
            return s;
        
        // For each index, assume this is the middle of the palindrome and look for it.
        // This finds the max odd length palindrome
        for(int i = 0; i < n - 1; i++)
        {
            String odd = findPalindrome(s, i, i);
            String even = findPalindrome(s, i, i + 1);
            
            if(odd.length() > even.length() && odd.length() > result.length())
            {
                result = odd;
            }
            else if(even.length() > odd.length() && even.length() > result.length())
            {
                result = even;
            }
        }
        
        return result;
    }
    
    // Returns [lo, hi], the start and end indices of the found palindrome
    public String findPalindrome(String s, int lo, int hi)
    {        
        // Case where we are looking for an even palindrome and are starting from a bad spot.
        // In this case the palindrome is just size 1.
        if(s.charAt(lo) != s.charAt(hi))
        {
            return s.substring(lo, lo);
        }
        
        while(lo > 0 && hi < s.length() - 1 && s.charAt(lo - 1) == s.charAt(hi + 1))
        {
            lo--;
            hi++;
        }
        
        return s.substring(lo, hi + 1);
    }
}
