import java.util.HashMap;

class Solution
{
    public int lengthOfLongestSubstring(String s)
    {
        // First pointer, second pointer, and length of longest substring
        int lo = 0;
        int hi = 1;
        int max = 1;
        char [] arr = s.toCharArray();

        // If the string references null or has no characters
        if(s == null || s.equals(""))
        {
            return 0;
        }

        // False if character is not in current substring
        // True if character is in current substring
        HashMap<Character, Integer> charToIndex = new HashMap<Character, Integer>();

        // Base step, lo is the first substring, set index value
        charToIndex.put(arr[lo], lo);

        // Iterate over the length of the string
        while(hi < arr.length && lo < arr.length)
        {
            // If this character is already in the substring, increment lo until the character is removed
            // Update lo to the index after where this matching character was previously found
            if(charToIndex.containsKey(arr[hi]))
            {   
                int prevIndex = charToIndex.get(arr[hi]);
                for(int i = lo; i <= prevIndex; i++)
                {
                    charToIndex.remove(arr[i]);
                }

                lo = prevIndex + 1;
            }
            else
            {
                charToIndex.put(arr[hi], hi);
                max = Math.max(max, charToIndex.size());
                hi++;
            }

            System.out.println("lo: " + lo);
            System.out.println("hi: " + hi);
            System.out.println(charToIndex);
        }

        return max;        
    }
}

class Main
{
    public static void main(String[] args)
    {
        Solution s = new Solution();
        System.out.println(s.lengthOfLongestSubstring("cdmyhietzgalfkjindktzztkpcfghxblaqjjypezweldqwmgcyzbytnn"));
    }
}
