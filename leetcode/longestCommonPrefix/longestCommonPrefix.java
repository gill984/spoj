class Solution
{
	public String longestCommonPrefix(String[] strs)
	{
		
		// Ideas
		// Build a tree out of the strings
		// Node class, count and String substring
		// If count >= 2 and substring length > max, update max
		// 
		// Time complexity of building the tree: O(n)
		
		// Base case is root node with length 0, no visits
		Node root = new Node("", 0);
		String result = "";

		for(int i = 0; i < strs.length; i++)
		{
			String s = strs[i];
			Node cur = root;

			for(int j = 0; j < s.length(); j++)
			{
				char c = s.charAt(j);

				

				if(cur.children[c] == null)
				{
					// Not visited yet, put in new node
					cur.children[c] = new Node(cur.substring + c, 1);
				}
				else
				{
					// Visited already, increment count
					cur.children[c].count += 1;
				}

				cur = cur.children[c];

// Check if result needs to be replaced
				if(cur.count == strs.length && result.length() < cur.substring.length())
				{
					result = cur.substring;
				}
			}
		}

		return result;
		
	}

	class Node
	{
		public String substring;
		public int count;
		public Node[] children;

		public Node(String s, int c)
		{
			substring = s;
			count = c;
			children = new Node[256];
		}
	}
			
}

class Main
{
	public static void main(String [] args)
	{
		Solution sol = new Solution();
		String[] input1 = {"flower", "flow", "flight"};
		String[] input2 = {"dog","racecar","car"};
		System.out.println(sol.longestCommonPrefix(input1));
		System.out.println(sol.longestCommonPrefix(input2));
	}
}
