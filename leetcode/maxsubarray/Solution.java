class Solution {
    public int maxSubArray(int[] nums) {
        // Find the contiguous non-empty subarray with the largest value
        // Subarray cannot be empty
        
        // At each point, solution is either the current subarray plus this new value 
        // or this new value by itself
        int max = nums[0];
        for(int i = 1; i < nums.length; i++)
        {
            if(nums[i-1] > 0)
                nums[i] += nums[i-1];
            
            max = Math.max(max, nums[i]);
        }
        
        return max;
    }
}
