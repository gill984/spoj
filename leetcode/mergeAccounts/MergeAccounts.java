class Solution
{
    public List<List<String>> accountsMerge(List<List<String>> accounts)
    {
        DoubleMap map = new DoubleMap();
        int idx = 0;

        for(List<String> a : accounts)
        {            
            // Only add emails if the email was not already added
            // In other words, emails are unique, names are not
            for(int i = 1; i < a.size(); i++)
            {
                if(!map.contains(a.get(i)))
                {
                    map.put(idx++, a.get(i));
                }
            }
        }

        // Check if the map has been built correctly
        // System.out.println(map);

        // Disjoint set emails are 0 to num of emails, accounts are all the elements after
        DisjointSet ds = new DisjointSet(idx + accounts.size() + 1);

        // Build the disjoint set
        // Root will always be the first element in each list, the name
        // If a previous link is found, union the previously found set into the set
        // currently being built. Why? Because other emails in the current list need to be included
        // In the set and they may have also already been found in other sets.
        for(int i = 0; i < accounts.size(); i++)
        {
            List<String> l = accounts.get(i);
            // Index of name in ds is idx + i
            int rootIdx = idx + i;
            for(int j = 1; j < l.size(); j++)
            {
                String s = l.get(j);

                // Entry point into disjoint set
                int key = map.get(s);

                // This handles the case where the root is set and not yet set
                ds.union(rootIdx, ds.root(key));
            }
        }

        List<List<String>> res = new ArrayList<List<String>>();
        for(List<String> l : accounts)
        {
            ArrayList<String> n = new ArrayList<String>();
            n.add(l.get(0));
            res.add(n);
        }

        for(int i = 0; i < idx; i++)
        {
            String email = map.get(i);
            // System.out.println(email);
            int root = ds.root(i);

            // root - idx is the index into res, this maps back to the name order
            res.get(root - idx).add(email);
        }

        for(int i = 0; i < res.size();)
        {
            // System.out.println("List: " + res.get(i));
            if(res.get(i).size() == 1)
            {
                // System.out.println("Res before removal: " + res);
                // System.out.println("Removing: " + res.get(i));
                res.remove(i);
                // System.out.println("Res after removal: " + res);
            }
            else
            {
                Collections.sort(res.get(i));
                i++;
            }
        }

        return res;
    }
}

class DisjointSet
{
    int [] p;

    public DisjointSet(int x)
    {
        this.p = new int[x];

        for(int i = 0; i < x; i++)
        {
            p[i] = i;
        }
    }

    // Union b into a
    public boolean union(int a, int b)
    {
        int rootA = root(a);
        int rootB = root(b);

        // Don't union two elements already in the same set
        if(rootA == rootB)
            return false;

        p[rootB] = rootA;

        // Union complete
        return true;
    }

    public int root(int a)
    {
        while(p[a] != a)
        {
            p[a] = p[p[a]];
            a = p[a];
        }

        return a;
    }
}

class DoubleMap
{
    private HashMap<Integer, String> iToS;
    private HashMap<String, Integer> sToI;

    public DoubleMap()
    {
        iToS = new HashMap<Integer, String>();
        sToI = new HashMap<String, Integer>();
    }

    public int get(String s)
    {
        if(sToI.containsKey(s)) return sToI.get(s);
        else return -1;
    }

    public String get(int i)
    {
        if(iToS.containsKey(i)) return iToS.get(i);
        else return null;
    }

    public void put(String s, int i)
    {
        sToI.put(s, i);
        iToS.put(i, s);
    }

    public void put(int i, String s)
    {
        put(s, i);
    }

    public boolean contains(String s)
    {
        return sToI.containsKey(s);
    }

    public boolean contains(int i)
    {
        return iToS.containsKey(i);
    }

    public String toString()
    {
        return iToS + " " + sToI;
    }
}
