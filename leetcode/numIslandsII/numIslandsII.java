class numIslandsII
{
    public static void main(String[] args)
    {
        Inner in = new Inner();
        in.val = 1;

        Outer a = new Outer();
        Outer b = new Outer();
        Outer c = new Outer();
        a.in = in;
        b.in = in;
        c.in = in;

        System.out.println(a.in.val);
        System.out.println(b.in.val);
        System.out.println(c.in.val);

        Inner in2 = new Inner();
        in2.val = 5;
        in.val = in2.val;


        System.out.println(a.in.val);
        System.out.println(b.in.val);
        System.out.println(c.in.val);

        a.in.val = 3;
        System.out.println(a.in.val);
        System.out.println(b.in.val);
        System.out.println(c.in.val);
    }
}

class Outer
{
    public Inner in;
}

class Inner
{
    public int val;
}
