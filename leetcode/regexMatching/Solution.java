class Solution {
    public boolean isMatch(String s, String p)
    {
        return solveCharArrs(s.toCharArray(), p.toCharArray());    
    }
    
    public boolean solveCharArrs(char[] s, char[] p)
    {
        // Solve problem using 2D DP
        // dp [i][j] corresponds to whether string s from index [0, i]
        // is matched by pattern string p from index [0, j]
        // We build up the array using a set of rules to reach the final
        // state. At the end of the process, dp[s.length][p.length] is
        // the solution.
        boolean[][] dp = new boolean[s.length + 1][p.length + 1];
        
        // Consider the base case of this problem dp[0][0] = true and corresponds
        // to an empty string s being matched by an empty string p.
        dp[0][0] = true;
        
        // Consider row 0 of this dp array. For this first row, s is the empty
        // string. Therefore we only match if we can maintain an empty string in p.
        // The only way to maintain an empty string in p is if we continue to find
        // asterisk characters in p which allow us to match 0 characters.
        for(int j = 2; j < dp[0].length; j++)
        {
            if(dp[0][j-2] && p[j - 1] == '*')
                dp[0][j] = true;
        }
        
        // Iterative steps: for i = 1 --> s.length, j = 1 --> p.length
        // 1. s and p match at the character being checked. We have added a new character to the pattern and string. Therefore
        //    dp[i][j] = dp[i - 1][j - 1]
        // 2. s and p do not match at the character being checked. dp[i][j] = false
        // 3. The character checked in p is a '.' which is a wildcard. This means we automatically match regardless of
        //    the character in s, same result as 1.
        // 4. The character in p is a '*', which allows 0 or more of the preceding character to match.
        //    Here we have a choice, we can take either 0, 1, or many of the previous chars to match.
        //
        //    If the character in p before the * does not match the character in s,
        //    then dp[i][j] = dp[i][j - 2] because this is the cell which tells us if we matched through the s character
        //    in j without the 2 asterisk substring blocks.
        //    Else If the character in s matches with the character before the * in p or the char in p before * is .
        //    Then dp[i][j] is true if any of the following were true (OR operation)
        //       dp[i][j - 2] gives us the result of taking 0 characters for this asterisk block.
        //       dp[i][j - 1] gives us the result of taking 1 character for this asterisk block.
        //       dp[i - 1][j] gives us the result of taking multiple characters for this asterisk block.
        for(int i = 1; i < dp.length; i++)
        {
            for(int j = 1; j < dp[0].length; j++)
            {
                char sc = s[i - 1];
                char pc = p[j - 1];
                
                if(sc == pc || pc =='.')
                {
                    dp[i][j] = dp[i-1][j-1];
                }
                else if(sc != pc && pc != '*' && pc != '.')
                {
                    dp[i][j] = false;
                }
                else if(pc == '*')
                {
                    char prevPc = p[j - 2];
                    if(prevPc != sc && prevPc != '.')
                    {
                        dp[i][j] = dp[i][j - 2];
                    }
                    else
                    {
                        dp[i][j] = dp[i][j - 2] || dp[i][j - 1] || dp[i - 1][j];
                    }
                }
            }
        }
        
        
        return dp[s.length][p.length];
    }
}
