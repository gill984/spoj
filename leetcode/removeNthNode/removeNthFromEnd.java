/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        // Given n will always be valid
        // 1 indexed removal
        // Need to remove node n spaces from the end
        // n == 0 is invalid
        // Idea is to build arrayList of nodes then perform removal on this list at the end
        
        ListNode ptr = head;
        int count = 0;
        while(ptr != null)
        {
            ptr = ptr.next;
            count++;
        }
        
        int idxToRemove = count - n;
        
        if(idxToRemove == 0)
        {
            ListNode result = head.next;
            head = null;
            return result;
        }
        
        ListNode lo = head;
        ListNode hi = head.next;
        for(int i = 1; i < idxToRemove; i++)
        {
            lo = lo.next;
            hi = hi.next;
        }
        lo.next = hi.next;
        hi = null;
        return head;
    }
}
