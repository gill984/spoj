class Solution {
    public final static int OCTET_MAX = 255;
    public final static int MAX_DIG = 3;
    
    public List<String> restoreIpAddresses(String s)
    {
        ArrayList<String> res = new ArrayList<String>();
        
        if(s == null || s.length() == 0)
        {
            return res;
        }
        
        // There aren't that many possibilities
        // 3^4 = 81 so that isn't too much to check
        
        // Start at 1 digit each and relax
        // 4th size is always determined by the first 3 choices
        int [] sizes  = new int [3];
        
        for(int i = 1; i <= MAX_DIG; i++)
        {
            for(int j = 1; j <= MAX_DIG; j++)
            {
                for(int k = 1; k <= MAX_DIG; k++)
                {
                    int idx = 0;
                    
                    if(idx + i > s.length())
                    {
                        continue;
                    }
                    
                    String octet1 = s.substring(idx, i);
                    idx += i;
                    
                    if(idx + j > s.length())
                    {
                        continue;
                    }
                    
                    String octet2 = s.substring(idx, idx + j);
                    idx += j;
                    
                    if(idx + k > s.length())
                    {
                        continue;
                    }
                    
                    String octet3 = s.substring(idx, idx + k);
                    idx += k;
                    
                    String octet4 = s.substring(idx, s.length());
                    
                    if(isValid(octet1) && isValid(octet2) && isValid(octet3) && isValid(octet4))
                    {
                        res.add(octet1 + "." + octet2 + "." + octet3 + "." + octet4);
                    }
                }
            }
        }
        
        return res;
    }
    
    private boolean isValid(String s)
    {
        // Check string size, needs to be [1, 3]
        if(s.length() < 1 || s.length() > 3)
        {
            return false;
        }
        
        // Check for leading 0 on multi-digit number
        if(s.length() > 1 && s.charAt(0) == '0')
        {
            return false;
        }
        
        // Check if 255 or less
        int value = Integer.parseInt(s);
        if(value > OCTET_MAX || value < 0)
        {
            return false;
        }
        
        return true;
    }
}
