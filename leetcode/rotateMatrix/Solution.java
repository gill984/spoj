class Solution
{
    public void rotate(int[][] matrix)
    {
        // Observation: Matrices of size 2x2 and greater form groups of 4 with each rotation.
        // Can go around matrix at each location and swap elements, then move onto the next group.
        // See below for example with each group represented by the same character
        //
        // rc 0 1 2 3 4
        // 0 [1 2 3 1] 
        // 1 [3 4 4 2]
        // 2 [2 4 4 3]
        // 3 [1 3 2 1]
        //
        // rc 0 1 2 3 4 5 6
        // 0 [1 2 3 4 5 6 1]
        // 1 [6 7 8 9 A 7 2]
        // 2 [5 A B C B 8 3]
        // 3 [4 9 C D C 9 4]
        // 4 [3 8 B C B A 5]
        // 5 [2 7 A 9 8 7 6]
        // 6 [1 6 5 4 3 2 1]
        //
        // Can loop through and perform this operation for outer groups then move inward
        // Each time iteration begins 1 row and column inward
        // Order is move right, down, left, up
        // Moving right: next.row = current.col, next.col = n - current.row - 1
        // Moving down: nextRow = currentCol, nextCol = n - currentRow - 1
        // Moving left: nextRow = currentCol, nextCol = n - currentRow - 1
        // Moving up: nextRow = currentCol, nextCol = n - currentRow - 1
        // Note the movements are the same for each direction we move, so we can simplify our loop
        // Need 2 temp variables to complete swap
        // Call swap on outer shell n-1 groups, then on n-3 groups, then on n-5 groups
        int n = matrix.length;
        
        // Initial number of groups for the outer shell of the matrix  
        int startRC = 0;
        for(int numGroups = n - 1; numGroups > 0; numGroups -= 2)
        { 
            int row = startRC;
            int col = startRC;
            for(int i = 0; i < numGroups; i++)
            {
                int temp1 = matrix[row][col];
                for(int j = 0; j < 4; j++)
                {
                    // update row and col, careful because they rely on eachother
                    int nextRow = col;
                    int nextCol = n - row - 1;
                    row = nextRow;
                    col = nextCol;
                    
                    int temp2 = matrix[row][col];
                    matrix[row][col] = temp1;
                    // System.out.println("Putting: " + temp1 + " in " + row + ", " + col);
                    temp1 = temp2;
                    
                }
                
                // Go to the next group
                col += 1;
            }
            
            startRC += 1;
        }
        
        return;
    }
}
