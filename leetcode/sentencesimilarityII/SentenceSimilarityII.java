class Solution
{
    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, List<List<String>> pairs)
    {
        if(words1 == null || words2 == null || words1.length != words2.length || pairs == null)
            return false;
        
        HashMap<String, Integer> stringToIndex = new HashMap<String, Integer>();
        
        
        DisjointSet ds = new DisjointSet(pairs.size() * 2);
        
        int idx = 0;
        for(List<String> list : pairs)
        {
            String s1 = list.get(0);
            String s2 = list.get(1);
            
            if(!stringToIndex.containsKey(s1))
            {
                stringToIndex.put(s1, idx);
                idx += 1;
            }
            
            if(!stringToIndex.containsKey(s2))
            {
                stringToIndex.put(s2, idx);
                idx += 1;
            }
            
            ds.union(stringToIndex.get(s1), stringToIndex.get(s2));
        }
        
        for(int i = 0; i < words1.length; i++)
        {
            String w1 = words1[i];
            String w2 = words2[i];
            
            if(w1.equals(w2))
                continue;
            
            if(stringToIndex.containsKey(w1) && stringToIndex.containsKey(w2)
               && ds.inSameSet(stringToIndex.get(w1), stringToIndex.get(w2)))
                continue;
            else
                return false;
        }
        
        return true;
    }
}

class DisjointSet
{
    public int [] parent;
    public int [] size;
    
    public DisjointSet(int x)
    {
        this.parent = new int [x];
        this.size = new int [x];
        
        // Initalize disjoint set
        for(int i = 0; i < parent.length; i++)
        {
            parent[i] = i;
            size[i] = 1;
        }
    }
    
    public boolean union(int a, int b)
    {
        if(inSameSet(a, b))
            return false;
        
        int rootA = root(a);
        int rootB = root(b);
        
        if(size[rootA] < size[rootB])
        {
            parent[rootA] = rootB;
            size[rootB] += size[rootA];
        }
        else
        {
            parent[rootB] = rootA;
            size[rootA] += size[rootB];
        }
        
        return true;
    }
    
    public int root(int a)
    {
        while(parent[a] != a)
        {
            parent[a] = parent[parent[a]];
            a = parent[a];
        }
        
        return a;
    }
    
    public boolean inSameSet(int a, int b)
    {
        return root(a) == root(b);
    }
}
