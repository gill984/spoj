class Solution
{
    public void solveSudoku(char[][] board)
    {
        if(board == null || board.length == 0)
            return;
        solve(board);
    }
    
    public boolean solve(char[][] board)
    {
        for(int i = 0; i < board.length; i++)
        {
            for(int j = 0; j < board.length; j++)
            {
                if(board[i][j] == '.')
                {
                    for(char c = '1'; c <= '9'; c++)
                    {
                        if(isValid(board, i, j, c))
                        {
                            board[i][j] = c;
                            
                            if(solve(board))
                                return true;
                            else    // backtrack!
                                board[i][j] = '.';
                        }                        
                    }
                    
                    return false;
                }
            }
        }
        
        return true;
    }
    
    public boolean isValid(char[][] board, int row, int col, char val)
    {        
        // Check all row and col values
        for(int i = 0; i < 9; i++)
        {
            if(board[row][i] == val || board[i][col] == val)
            {
                return false;
            }
        }
        
        // Check all square values
        int rowThird = row / 3;
        int colThird = col / 3;
        for(int i = rowThird * 3; i < rowThird * 3 + 3; i++)
        {
            for(int j = colThird * 3; j < colThird * 3 + 3; j++)
            {
                if(board[i][j] == val)
                {
                    return false;
                }
            }
        }
        
        return true;
    }
}
