public class GetSum
{
    public int getSum(int a, int b)
    {
        if(b == 0)
        {
            return a;
        }
        else
        {
            System.out.println("a xor b: " + (a ^ b));
            System.out.println("(a & b) << 1: " + ((a & b) << 1));
            return getSum(a ^ b, (a & b) << 1);
        }
    }

    public int getSumIter(int a, int b)
    {
        while(b != 0)
        {
            int temp = a;
            System.out.println("a xor b: " + (a ^ b));
            System.out.println("(a & b) << 1: " + ((a & b) << 1));
            a = a ^ b;

            // Calculate bitwise carry. This will be used by the xor add above on the next
            // iteration. This allows calculating sums and recalculating extra carries.
            // Example
            // a     =  1011
            // b     =  0111
            // a & b =  0011
            // << 1  =  0110
            //
            b = ((temp & b) << 1);
        }

        return a;
    }
}

class Main
{
    public static void main(String [] args)
    {
        GetSum sum = new GetSum();
        System.out.println(sum.getSum(987, 599));
        System.out.println();
        System.out.println(sum.getSum(-1, 1));
        System.out.println();
        System.out.println(sum.getSumIter(987, 599));
        System.out.println();
        System.out.println(sum.getSumIter(-1, 1));
    }
}
