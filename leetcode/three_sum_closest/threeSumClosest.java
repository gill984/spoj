class Solution
{
    public int threeSumClosest(int[] nums, int target)
    {
        // Idea: For each of the n^2 pairs of numbers, perform a binary search to find the best possible value
        // This is O(n*lg(n) + n^2 * log(n))
        // Special case code is needed when the index selected is equal to one of the values already chosen for the search
        
        // Is there a way to pick a middle value and search for the best two lo and hi values in < n^2 time?
        // Let's say my current sum is < target, move hi up until >= target
        // Now move lo down until <= target. Store closest value.
        // Does this algorithm work? I belive so.
        // Time complexity is: Sort + n^2
        
        // First sort the array
        Arrays.sort(nums);
        
        // Initialize the best value to a possible set of values
        int best = nums[0] + nums[1] + nums[2];
        int bestDistance = Math.abs(target - best);
        
        for(int mid = 1; mid < nums.length - 1; mid++)
        {
            int lo = mid - 1;
            int hi = mid + 1;
            
            while(lo >= 0 && hi < nums.length)
            {

                int sum = nums[lo] + nums[hi] + nums[mid];
                int diff = Math.abs(target - sum);

                if(diff < bestDistance)
                {
                    best = sum;
                    bestDistance = diff;
                }

                if(sum < target)
                {
                    hi++;
                }
                else if(sum > target)
                {
                    lo--;
                }
                else
                {
                    return sum;
                }
            }
        }
        
        return best;
    }
}
