class Solution {
    public int uniquePaths(int m, int n) {
        // Catalan numbers have something to do with this problem's closed form solution but I don't
        // know the answer by heart.
        
        // It seems like we could figure out a closed form solution to this problem as well. But I'll solve it the
        // way I would in an interview instead of mathematically.
        
        // DFS can't avoid double counting, so use BFS
        return bfs(n, m);
    }
    
    public int bfs(int numRows, int numCols)
    {
        // Group each element by its row value plus column value. Iterating according to this
        // group order assures that we don't double count. Members of a group form a diagonal.
        // Pull to the current group rather than pushing to the next
        int[][] grid = new int[numRows][numCols];
        
        // Base case
        grid[0][0] = 1;
        
        for(int group = 0; group <= numRows + numCols - 2; group++)
        {
            // Start iteration at a maximum row and minimum column. As iteration continues, decrease row
            // and increase col. Visually this iteration moves from bottom left to upper right diagonally.
            for(int j = 0; j <= group; j++)
            {
                int row = group - j;
                int col = 0 + j;
                
                // Make sure this element is in the grid, do the same for values
                // to the left and up when pulling in to this element.
                if(row >= 0 && row < numRows && col >= 0 && col < numCols)
                {
                    // Pull from above
                    if(row - 1 >= 0)
                    {
                        grid[row][col] += grid[row - 1][col];
                    }
                    
                    // Pull from the left
                    if(col - 1 >= 0)
                    {
                        grid[row][col] += grid[row][col - 1];
                    }
                }
            }
        }
        
        return grid[numRows - 1][numCols - 1];        
    }
    
}
