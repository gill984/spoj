// Read in input
//	parse for operation and digits
//	first input is test cases
//      for each test case, read until +, -, or * is found
//      	if addition, do calculation and move to output, easy case. Use a carry variable.
//		
//		else if subtraction, I believe it is another simple easy case. Item by item subtraction with a carry.
//			remember, 0's become 9s and the your number gets 1 added to it. Turns out the second value can't be
//			greater than the first, so I did extra "flipping" work for nothing, oh well.
//
//		else if multiplication, more difficult case
//			multiply item by item.
//			will need a carry variable which will get updated after each multiplication
//			When starting a new multiplication of 2 characters, do the multiplication, then add the carry.
//			Could attempt using the addition function I write within the multiplication logic.
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;

class BigInt
{
	// Backing ArrayList with all of the digits
	private ArrayList<Integer> digits;

	// Default Constructor which takes no arguments. Instantiates digits.
	public BigInt()
	{
		digits = new ArrayList<Integer>();
	}

	// Create a BigInt Object from a passed in String
	// This constructor adds characters in reverse order due to my preference.
	public BigInt(String digitStr)
	{
		digits = new ArrayList<Integer>();

		for(int i = digitStr.length() - 1; i >= 0; i--)
		{
			this.addDigit(Character.getNumericValue(digitStr.charAt(i)));
		}
	}

	// Overwrite the toString method to print out the BigInt values.
	public String toString()
	{
		return Arrays.toString(this.digits.toArray());
	}

	// Size is directly derived from the backing ArrayList
	public int size()
	{
		return this.digits.size();
	}
	
	// Add on a digit to this BigInt.
	public void addDigit(int b)
	{
		this.digits.add(b);
	}

	// Return the int stored at the index passed in.
	public int getDigit(int index)
	{
		if(index < 0)
		{
			System.out.println("Out of bounds, returning 0");
			return 0;
		}
		else if(index >= this.size())
		{
			// Past the max digit here, return 0. This is a key point and
			// follows logic. The higher places of the number can be thought
			// of as having 0s.
			return 0;
		}
		else if(index < this.size())
		{
			return digits.get(index);
		}
		else
		{			
			System.out.println("Sanity Check Error, Returning 0");
			return 0;
		}
	}
	
	// Returns 1 if this BigInt is larger than the passed in BigInt "b"
	// Returns -1 if this BigInt is less than b
	// Returns 0 if the two BigInts are equal
	public int compare(BigInt b)
	{
		if(this.size() > b.size())
			return 1;
		else if(this.size() < b.size())
			return -1;
		else if(this.size() == b.size())
		{
			for(int i = this.size(); i >= 0; i--)
			{
				if(this.getDigit(i) == b.getDigit(i))
					continue;
				else if(this.getDigit(i) > b.getDigit(i))
					return 1;
				else if(this.getDigit(i) < b.getDigit(i))
					return -1;
			}
			
			// At this point, the numbers are equal.
			return 0;
		}
		else
		{
			System.out.println("ERROR! returning 0");
			return 0;
		}
	}

	// Add two numbers and return the result.
	public static BigInt add(BigInt x, BigInt y)
	{
		BigInt result = new BigInt();
		int carry = 0;

		for(int i = 0; i < x.size() || i < y.size(); i++)
		{
			int sum = x.getDigit(i) + y.getDigit(i) + carry;

			result.addDigit(sum % 10);
			carry = sum / 10;
		}

		if(carry == 1)
		{
			result.addDigit(carry);
		}

		return result;
	}

	// Subtract two numbers and return the result.
	public static BigInt subtract(BigInt x, BigInt y, boolean flipped)
	{
		BigInt result = new BigInt();
		int compare = x.compare(y);

		if(compare == 0)
		{	
			// If the two are equal, then we know the difference
			// is equal to 0.
			result.addDigit(0);
		}
		else if(compare == -1)
		{
			if(flipped)
			{
				// This shouldn't happen, if it does it would result in an infinite loop.
				System.out.println("Let's please not do this infinite loop thing.");
				return null;
			}
			
			// Flip the operands, and set the flipped flag. Then make a
			// recursive call to this function. This retains the initial
			// values of the BigInt arguments.
			return subtract(y, x, true);
		}
		else
		{
			// Actually do the subtraction
			int carry = 0;
			for(int i = 0; i < x.size() || i < y.size(); i++)
			{
				int diff = x.getDigit(i) + carry - y.getDigit(i);

				if(diff < 0)
				{
					diff += 10;
					carry = -1;
				}
				else
				{
					carry = 0;
				}
				result.addDigit(diff);
			}
		}

		if(flipped)
		{
			// This will signify a negative number
			result.addDigit(-1);
		}

		return result;
	}

	public static ArrayList<BigInt> multiply(BigInt x, BigInt y)
	{
		ArrayList<BigInt> results = new ArrayList<BigInt>();

		for(int ydx = 0; ydx < y.size(); ydx++)
		{
			BigInt result = new BigInt();
			if(y.getDigit(ydx) == 0 || (x.size() == 1 && x.getDigit(0) == 0))
			{
				// This particular result is 0
				// Make it zero, add it to the results, and continue.
				result.addDigit(0);
				results.add(result);
				continue;
			}

			// Add zeroes until we get to the correct place.
			for(int i = 0; i < ydx; i++)
			{
				result.addDigit(0);
			}

			int carry = 0;
			for(int xdx = 0; xdx < x.size(); xdx++)
			{
				int product = y.getDigit(ydx) * x.getDigit(xdx) + carry;
				result.addDigit(product % 10);
				carry = product / 10;
			}

			if(carry > 0)
			{
				result.addDigit(carry);
			}

			results.add(result);
		}
		
		// Only do this if the bottom operand has more than 1 digit to multiply
		if(y.size() > 1)
		{
			// Compute the final result by adding up everything
			BigInt finalResult = new BigInt();

			// Start off at 0
			finalResult.addDigit(0);

			// Iteratively add everything, use finalResult as the intermediate product
			for(BigInt b : results)
			{
				finalResult = BigInt.add(finalResult, b);
			}

			// Add in the final sum of results
			results.add(finalResult);
		}

		return results;
	}
}

class ARITH
{
	public static void main(String [] args) throws java.lang.Exception
	{
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		int numTestCases = Integer.parseInt(input.readLine());
		for(int T = 0; T < numTestCases; T++)
		{
			String line = input.readLine();

			// Split the string on +, -, or *
			String [] tokens = line.split("[\\+\\-\\*]");

			BigInt x = new BigInt(tokens[0]);
			BigInt y = new BigInt(tokens[1]);

			System.out.println(x);
			System.out.println(y);

			if(line.contains("+"))
			{
				System.out.println(BigInt.add(x, y));
			}
			else if(line.contains("-"))
			{
				System.out.println(BigInt.subtract(x, y, false));
			}
			else if(line.contains("*"))
			{
				ArrayList<BigInt> results = BigInt.multiply(x, y);

				for(BigInt b : results)
					System.out.println(b);
			}
		}

	}
}
