import java.util.*;
import java.lang.*;
import java.io.File;

public class ID6_SimpleArithmetics
{
	public static void main(String[] args) throws java.lang.Exception
	{
		boolean firstIteration = true;
		int numberOfExpressions = 0;
		File file = new File("files/ID6_SimpleArithmeticsTestCase");
		Scanner in = new Scanner(file);
//		Scanner in = new Scanner(System.in);
		if(in.hasNextInt())
		{
			numberOfExpressions = in.nextInt();
		}
		while(in.hasNext() && numberOfExpressions > 0)
		{
			if(!firstIteration)
			{
				System.out.println();
			}
			else
			{
				firstIteration = false;
			}
			String expression = in.next();
			int charValue;
			for(int i = 0; i < expression.length(); i++)
			{
				charValue = Character.valueOf(expression.charAt(i));
				if(charValue <  48)
				{
					if(charValue == 43)
					{
						//operation is addition
						String string1 = expression.substring(0, i);
						String string2 = expression.substring(i + 1, expression.length());
						String string2WithOperator = expression.substring(i, expression.length());
						String result = StringNumberAddition(string1, string2);
						PrintEquation(string1, string2WithOperator, result);
					}
					else if (charValue == 45)
					{
						String string1 = expression.substring(0, i);
						String string2 = expression.substring(i + 1, expression.length());
						String string2WithOperator = expression.substring(i, expression.length());
						String result = StringNumberSubtraction(string1, string2);
						PrintEquation(string1, string2WithOperator, result);
					}
					else if (charValue == 42)
					{
						String string1 = expression.substring(0, i);
						String string2 = expression.substring(i + 1, expression.length());
						StringNumberMultiplier(string1, string2);
					}
			}
			}
			numberOfExpressions--;
		}
	}
	
	public static void PrintEquation(String number1, String number2WithOperator, String result)
	{
		int n1 = number1.length();
		int n2 = number2WithOperator.length();
		int n3 = result.length();		
		int limit = Math.max(n3, Math.max(n2, n1));
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < limit; i++)
		{
			sb.append("-");
		}
		if(n1 < limit)
		{
			StringBuilder temp = new StringBuilder(number1).reverse();
			for(int i = n1; i < limit; i++)
			{
				temp.append(" ");
			}
			number1 = temp.reverse().toString();
		}
		if(n2 < limit)
		{
			StringBuilder temp = new StringBuilder(number2WithOperator).reverse();
			for(int i = n2; i < limit; i++)
			{
				temp.append(" ");
			}
			number2WithOperator = temp.reverse().toString();
		}
		if(n3 < limit)
		{
			StringBuilder temp = new StringBuilder(result).reverse();
			for(int i = n3; i < limit; i++)
			{
				temp.append(" ");
			}
			result = temp.reverse().toString();
		}
		System.out.println(number1);
		System.out.println(number2WithOperator);
		System.out.println(sb.toString());
		System.out.println(result);
	}
	
//	public static void PrintEquationStart(String number1, String number2WithOperator)
//	{
//		int n1 = number1.length();
//		int n2 = number2WithOperator.length();		
//		int limit = Math.max(n2, n1);
//		StringBuilder sb = new StringBuilder();
//		for(int i = 0; i < limit; i++)
//		{
//			sb.append("-");
//		}
//		if(n1 < limit)
//		{
//			StringBuilder temp = new StringBuilder(number1).reverse();
//			for(int i = n1; i < limit; i++)
//			{
//				temp.append(" ");
//			}
//			number1 = temp.reverse().toString();
//		}
//		if(n2 < limit)
//		{
//			StringBuilder temp = new StringBuilder(number2WithOperator).reverse();
//			for(int i = n2; i < limit; i++)
//			{
//				temp.append(" ");
//			}
//			number2WithOperator = temp.reverse().toString();
//		}
//		System.out.println(number1);
//		System.out.println(number2WithOperator);
//		System.out.println(sb.toString());
//	}
	
	public static int StringNumberCompare(String string1, String string2)
	{
		//Returns 1 if string1 is greater than string2, -1 if string1 is less than string2, and 0 if equal
		if(string1.length() > string2.length())
		{
			return 1;
		}
		else if(string1.length() < string2.length())
		{
			return -1;
		}
		else
		{
			for(int i = 0; i < string1.length(); i++)
			{
				if(string1.charAt(i) == string2.charAt(i))
				{
					continue;
				}
				else if(string1.charAt(i) > string2.charAt(i))
				{
					return 1;
				}
				else
				{
					return -1;
				}
			}
		}
		return 0;
	}

	public static String StringNumberAddition(String number1, String number2)
	{
		//set limit to the lesser of the two String lengths
		int limit = number1.length();
		String n1 = "";
		String n2 = "";
		if(limit > number2.length())
		{
			limit = number2.length();
			n1 = new StringBuilder(number1).reverse().toString();
			n2 = new StringBuilder(number2).reverse().toString();
		}
		else
		{
			n1 = new StringBuilder(number2).reverse().toString();
			n2 = new StringBuilder(number1).reverse().toString();
		}
		int carry = 0;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < limit; i++)
		{
			int temp1 = Character.getNumericValue(n1.charAt(i));
			int temp2 = Character.getNumericValue(n2.charAt(i));
			int temp3 = temp1 + temp2 + carry;
			if(temp3 > 9)
			{
				temp3 = temp3 % 10;
				carry = 1;
			}
			else
			{
				carry = 0;
			}
			sb.append(Integer.toString(temp3));
		}
		int currentPosition = limit;
		int size = n1.length();
		int result = 0;
		while(carry == 1 && currentPosition < size)
		{
			result = Character.getNumericValue(n1.charAt(currentPosition));
			result = result + carry;
			if(result > 9)
			{
				result = result % 10;
				carry = 1;
			}
			else
			{
				carry = 0;
			}
			sb.append(Integer.toString(result));
			currentPosition++;
		}
		if(carry == 1)
		{
			sb.append(carry);
		}
		else if(currentPosition < size)
		{
			sb.append(n1.substring(currentPosition, size));
		}
		return sb.reverse().toString();
	}
	
	public static String StringNumberSubtraction(String number1, String number2)
	{
		//set limit to the lesser of the two String lengths
		int compareResult = StringNumberCompare(number1, number2);
		int limit = 0;
		String n1 = "";
		String n2 = "";
		if(compareResult == 0)
		{
			return "0";
		}
		else if(compareResult == 1)
		{
			limit = number2.length();
			n1 = new StringBuilder(number1).reverse().toString();
			n2 = new StringBuilder(number2).reverse().toString();
		}
		else
		{
			limit = number1.length();
			n1 = new StringBuilder(number2).reverse().toString();
			n2 = new StringBuilder(number1).reverse().toString();
		}
		
		int borrow = 0;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < limit; i++)
		{
			int temp1 = Character.getNumericValue(n1.charAt(i));
			int temp2 = Character.getNumericValue(n2.charAt(i));
			int temp3 = temp1 - temp2 - borrow;
			if(temp3 < 0)
			{
//				temp3 = Math.abs(temp3);
				temp3 = temp1 + 10 - temp2 - borrow;
				borrow = 1;
			}
			else
			{
				borrow = 0;
			}
			sb.append(Integer.toString(temp3));
		}
		int currentPosition = limit;
		int size = n1.length();
		int temp1 = 0;
		int temp2 = 0;
		while(borrow == 1 && currentPosition < size)
		{
			temp2 = Character.getNumericValue(n1.charAt(currentPosition));
			temp1 = temp2 - borrow;
			if(temp1 < 0)
			{
//				result = Math.abs(result);
				temp1 = temp2 + 10 - borrow;
			}
			else
			{
				borrow = 0;
			}
			sb.append(Integer.toString(temp1));
			currentPosition++;
		}
//		if(carry == 1)
//		{
//			sb.append(carry);
//		}
		if(currentPosition < size)
		{
			sb.append(n1.substring(currentPosition, size));
		}
		//trim the solution of leading zeroes
		while(sb.charAt(sb.length() - 1) == '0')
		{
			sb.deleteCharAt(sb.length() - 1);
		}
		if(compareResult == -1)
		{
			sb.append("-");
		}
		return sb.reverse().toString();
	}
	
	public static void StringNumberMultiplier(String number1, String number2)
	{
		String cumulativeValue = "0";
		int n2 = number2.length();
		LinkedList<String> storedProducts = new LinkedList<String>();
		String number1Reversed = new StringBuilder(number1).reverse().toString();
		String number2Reversed = new StringBuilder(number2).reverse().toString();
		for(int i = 0; i < n2; i++)
		{
			String tempProduct = StringNumberMultiplierSingleDigit(number1Reversed, Character.getNumericValue(number2Reversed.charAt(i)));
			storedProducts.add(tempProduct);
			if(!tempProduct.equals("0"))
			{
				cumulativeValue = PseudoStringNumberAddition(tempProduct, cumulativeValue, i);
			}
		}
		String number2WithOperator = "*" + number2;
		int n1 = number1.length();
		n2 = number2WithOperator.length();
		int cv = Math.max(cumulativeValue.length(), Math.max(n1, n2));
		int problemSize = Math.max(n1, n2);
		StringBuilder tempStringBuilder = new StringBuilder();
		for(int i = 0; i < cv - n1; i++)
		{
			tempStringBuilder.append(" ");
		}
		System.out.println(tempStringBuilder.append(number1).toString());
		tempStringBuilder = new StringBuilder();
		for(int i = 0; i < cv - n2; i++)
		{
			tempStringBuilder.append(" ");
		}
		System.out.println(tempStringBuilder.append(number2WithOperator).toString());
		if(storedProducts.size() > 1)
		{
			tempStringBuilder = new StringBuilder();
			for(int i = 0; i < cv - problemSize; i++)
			{
				tempStringBuilder.append(" ");
			}
			for(int i = 0; i < problemSize; i++)
			{
				tempStringBuilder.append("-");
			}
			System.out.println(tempStringBuilder.toString());
			tempStringBuilder = new StringBuilder();
			for(int i = 0; i < cv; i++)
			{
				tempStringBuilder.append(" ");
			}
			String blankString = tempStringBuilder.toString();
			int currentPosition = 0;
			String currentProduct;
			ListIterator<String> listIterator = storedProducts.listIterator();
			while(listIterator.hasNext())
			{
				currentProduct = listIterator.next();
				tempStringBuilder = new StringBuilder();
				System.out.println(blankString.substring(0, cv - currentProduct.length() - currentPosition) + currentProduct + blankString.substring(0, currentPosition));
				currentPosition++;
			}
		}
		tempStringBuilder = new StringBuilder();
		for(int i = 0; i < cv; i++)
		{
			tempStringBuilder.append("-");
		}
		System.out.println(tempStringBuilder.toString());
		tempStringBuilder = new StringBuilder();
		for(int i = 0; i < cv - cumulativeValue.length(); i++)
		{
			tempStringBuilder.append(" ");
		}
		System.out.println(tempStringBuilder.toString() + cumulativeValue);
	}

	public static String StringNumberMultiplierSingleDigit(String number1, int digit)
	{
		String cumulativeValue = "0";
//		int leadingZeroCount = 0;
		for(int i = 0; i < number1.length(); i++)
		{
			int tempProduct = Character.getNumericValue(number1.charAt(i)) * digit;
			if(tempProduct != 0)
			{
				cumulativeValue = PseudoStringNumberAddition(Integer.toString(tempProduct), cumulativeValue, i);
//				if(leadingZeroCount > 0)
//				{
//					StringBuilder tempStringBuilder = new StringBuilder();
//					for(int j = 0; j < leadingZeroCount + 1; j++)
//					{
//						tempStringBuilder.append("0");
//					}
//					cumulativeValue = cumulativeValue + tempStringBuilder.toString();
//				}
//				leadingZeroCount = 0;
			}
//			else
//			{
//				leadingZeroCount++;
////				cumulativeValue = "0" + cumulativeValue;
//			}
		}
		return cumulativeValue;
	}

	public static String PseudoStringNumberAddition(String number1, String number2, int position)
	{
		String number2Substring;
		String result;
		if(number2.length() >= position)
		{
			String number2Reversed = new StringBuilder(number2).reverse().toString();
			String reversedSubstring = number2Reversed.substring(position, number2.length());
			number2Substring = new StringBuilder(reversedSubstring).reverse().toString();
			if(number2Substring.equals(""))
			{
				number2Substring = "0";
			}
			String additionResult = StringNumberAddition(number1, number2Substring);
			int n2 = number2.length();
			result = additionResult + number2.substring(n2 - position, n2);
		}
		else
		{
			number2Substring = "0";
			String additionResult = StringNumberAddition(number1, number2Substring);
			int n2 = number2.length();
			StringBuilder stringOfZeroes = new StringBuilder();
			for(int i = 0; i < position - n2; i++)
			{
				stringOfZeroes.append("0");
			}
			result = additionResult + stringOfZeroes + number2;
		}
//		String additionResult = StringNumberAddition(number1, number2Substring);
//		int n2 = number2.length();
//		String result = additionResult + number2.substring(n2 - position, n2);
		return result;
	}

}
