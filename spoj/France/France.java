import java.util.*;

class France {
	public static void main ( String[] args ) {
		Scanner scan = new Scanner ( System.in );
		while( scan.hasNext() ) {
			scan.useDelimiter( "\n" );
			String str = scan.next();
			if( str.equals ("*") ) {
				break;
			}
			boolean taut = true;
			str = str.trim();
			str = str.replaceAll("(\\r|\\n)", "");
			String[] words = str.split("\\s");
			//System.out.println( Arrays.toString( words ) );
			
			if( str.equals( "" ) ) {
				continue;
			}
			
			char c = (words[0]).charAt(0);
			char c1;
			if ( Character.isUpperCase( c ) ) {
				c1 = Character.toLowerCase( c );
			} else {
				c1 = Character.toUpperCase( c );
			}
		
			for(int i = 0; i < words.length; i++) {
				char f = (words[i]).charAt(0);				//first character of a word
				if( f != c && f != c1 ) {			//if c is the first character
					//System.out.println( c + "\n" + c1 );
					taut = false;
				}
			}
		
			if( taut ) {
					System.out.println( "Y" );					//taut is true
				} else {
					System.out.println( "N" );					//taut is false
			}
		}
	}
}
