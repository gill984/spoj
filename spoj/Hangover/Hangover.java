import java.util.*;
import java.io.*;
class Dice
{
	public static void main(String[] args) {
	Scanner s = new Scanner(System.in);
	int x = s.nextInt();
	for(int i = 0; i<x ; i++) {
		double sum = 0;
		double n = s.nextDouble();
		for(double j=n;j>0;j--) {
			sum += (n/j);
		}
		System.out.format( "%.2f%n",sum);
	}
	}
}