import java.util.*;
import java.math.BigInteger;

class julka {
	public static void main( String[] args ) {
		final int cases = 10;
		final BigInteger TWO = new BigInteger( "2" );
		Scanner scan = new Scanner( System.in );
		for( int i = 0; i < cases; i++ ) {
			BigInteger apples = new BigInteger( scan.next() );
			BigInteger more = new BigInteger( scan.next() );
			BigInteger half = apples.shiftRight( 1 );
			BigInteger half2;
			if( apples.remainder( TWO ).equals( BigInteger.ONE ) ) {		//if not even
				half2 = half.add( BigInteger.ONE );
			} else {
				half2 = new BigInteger( half.toString() );
			}
			if( more.remainder( TWO ).equals( BigInteger.ONE ) ) {			//if not even
				more = more.subtract( BigInteger.ONE );
			}
			//System.out.println( "more: " + more );
			more = more.divide( TWO );
			//System.out.println( "half2: " + half2 );
			half2 = half2.add( more );
			//System.out.println( "half2: " + half2 );
			half = half.subtract( more );
			System.out.println( half2 );
			System.out.println( half ); 
		}
	}
}
