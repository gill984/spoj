#include<iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
using namespace std;

int main() {
	stringstream ss;
	while( true ) {
		string s;
		cin >> s;
		//check for zero
		if( s == "0" ) {
			break;
		}
		//algorithm starts here
		long long * dp = new long long[ s.length() + 1 ];
		dp[0] = 1;
		dp[1] = 1;
		for( int i = 2; i < s.length() + 1; i++ ) {
			long long sum = 0;
			int a = s[i-1] - 48;
			int b = s[i-2] - 48;
			if( b * 10 + a <= 26 && b != 0 ) {		//double working
				sum += dp[i-2];
			}
			if ( a != 0 ) {	//case of no doubling
				sum += dp[i-1];
			}
			dp[i] = sum;
		}
		ss << dp[ s.length() ] << "\n";
		
	}
	cout << ss.str();
	return 0;
}