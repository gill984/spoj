import java.util.*;

class addRev {
	public static void main ( String[] args ) {
		Scanner scan = new Scanner( System.in );
		int T = scan.nextInt();
		for( int i = 0; i < T; i++ ) {
			int a = scan.nextInt();
			int b = scan.nextInt();
			String sA = reverseIt( Integer.toString( a ) );
			String sB = reverseIt( Integer.toString( b ) );
			int rA = Integer.valueOf( sA );
			int rB = Integer.valueOf( sB );
			//System.out.println( rA + "\n" + rB );
			int c = rA + rB;
			System.out.println( Integer.valueOf( reverseIt( String.valueOf( c ) ) ) );
		}
	}
	
	public static String reverseIt(String a) {
	
		int length = a.length();
		StringBuilder reverse = new StringBuilder();
		for(int i = length; i > 0; --i) {
			char result = a.charAt(i-1);
			reverse.append(result);
		}
		return reverse.toString();
	}
}
