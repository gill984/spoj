import java.util.*;

class adjRec {
	public static void main ( String[] args ) {
		System.out.println( f(3,0) );
		System.out.println( f(10,0) );
	}
	
	public static int f( int i, int j ) {
		int n = i;
		int k = j;
		if( n == 0 && k == 0 ) {
			return 1;
		} else if ( k >= n ) {
			return 0;
		} else if( k < 0 ) {
			return 0;
		}
		else if ( n == 0 && k == 1 ) {
			return 0;
		} else if ( n == 1 && k == 0 ) {
			return 2;
		} else if ( n == 1 && k == 1 ) {
			return 0;
		}
		else {
			return f( n-1,k ) + f( n-1,k-1 ) + f( n-2,k ) - f( n-2,k-1 );
		}
	}	
}
