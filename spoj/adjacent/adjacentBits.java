import java.util.*;
import java.io.*;

class adjacentBits {
	public static void main( String[] args ) throws Exception {
		//precomputation time
		int numRows = 101;
		int numCols = 101;
		long [][] dp = new long [ numRows ] [ numCols ];
		
		//populate column 0 with fibonacci numbers, we will overflow but it won't matter
		dp[0][0] = 1;
		dp[1][0] = 2;
		for( int i = 2; i < numRows; i++ ) {
			dp[i][0] = dp[i-1][0] + dp[i-2][0];
		}
		
		//now populate the rest of the array with the recurrence relation
		// dp[n][k] = dp[n-1][k] + dp[n-1][k-1] + dp[n-2][k] - dp[n-2][k-1]
		for( int j = 1; j < numCols; j++ ) {				//iterate through rows first, then columns
			for( int i = j+1; i < numRows; i++ ) {
				dp[i][j] = dp[i-1][j] + dp[i-1][j-1] + dp[i-2][j] - dp[i-2][j-1];
			}
		}
		//array populated
		
		//check the results seems to be working correctly
		/*
		for( int i = 0; i < 10; ++i ) {
			for( int j = 0; j < 10; ++j ) {
				System.out.print( dp[i][j] + "\t" );
			}
			System.out.print("\n");
		}
		*/
		//done with precomputation
		Parser scan = new Parser( System.in );		//start read in input
		int cases = scan.nextInt();
		StringBuilder output = new StringBuilder();
		while( cases-- > 0 ) {
			int line = scan.nextInt();
			int length = scan.nextInt();
			int adj = scan.nextInt();
			output.append( line + " " + dp[length][adj] + "\n" );
		}
		System.out.print( output );
	}
}



class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}


