import java.util.*;
import java.io.*;

class ae2a {
	public static void main( String[] args ) throws Exception {
		final int sides = 6;
		StringBuilder out = new StringBuilder();
		Parser scan = new Parser( System.in );
		int cases = scan.nextInt();
		int lastRow = 546;
		double[][] dp = new double[lastRow+1][(lastRow+1)*6+1];
		for( int i = 1; i <= sides; i++ ) {
			dp[1][i] = (float) (1.0/sides);
		}
		for( int i = 2; i < dp.length; i++ ) {
			for( int k = i; k < dp[0].length; k++ ) {
				dp[i][k] = dp[i][k-1] + dp[i-1][k-1]/sides;
				if( k-(sides+1) >= 0 ) {
					//remove the chance from 
					dp[i][k] -= dp[i-1][k-(sides+1)] / sides;
				}
			}
		}
		
		//for( int i = 0; i < dp.length; i++ ) {
		//	for( int j = i; j < i*6; j++ ) {
		//		System.out.print( ((int)(dp[i][j] * 100)) + " ");
		//	}
		//	System.out.println();
		//}
		
		while( cases-- > 0 ) {
			int rolls = scan.nextInt();
			int pips = scan.nextInt();
			
			//System.out.println( cases );
			//System.out.println( Arrays.deepToString(dp) );
			if( rolls > pips || rolls*sides < pips || rolls >= dp.length ) {
				out.append( 0 );
			}
			else if ( rolls == 0 && pips == 0 ) {
				out.append( 100 );
			}
			else {
				out.append( (int) Math.floor(dp[rolls][pips] * 100.0) );
			}
			out.append( "\n" );
		}
		System.out.print( out );
	}
}	
	

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}

