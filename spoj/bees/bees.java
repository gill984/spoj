import java.util.*;

class bee {
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		int num = 0;
		HashSet< Integer > set = new HashSet< Integer >();
		for( int i = 0; num < 1000000000 ; i++ ) {
			num = 3*i*(i+1)+1;
			set.add( num );
		}
		StringBuilder str = new StringBuilder();
		while( true ) {
			int check = scan.nextInt();
			if( check == -1 ) break;
			
			
			if( set.contains( check ) ) {
				str.append( "Y\n" );
			} else {
				str.append( "N\n" );
			}
		}
		System.out.print( str );
	}
}
