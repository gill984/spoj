import java.util.*;
import java.math.BigInteger;

class bishops {
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		while ( scan.hasNext() ) {
			BigInteger n = new BigInteger( scan.next() );
			if( n.equals( BigInteger.ONE ) ) {
				System.out.println( "1" );
			} else if ( n.equals( BigInteger.ZERO ) ) {
				System.out.println( "0" );
			} else {
				n = n.subtract( BigInteger.ONE );
				n = n.multiply( new BigInteger( "2" ) );
				System.out.println( n.toString() );
			}
		}
	}
}
