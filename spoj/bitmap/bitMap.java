import java.util.*;
import java.io.*;

class bitMap {
	public static Node[][] graph;
	
	public static void main ( String [] args ) throws Exception{
		Parser scan = new Parser( System.in );
		int cases = scan.nextInt();
		int rows = 0;
		int cols = 0;
		StringBuilder str = new StringBuilder();
		while( cases-- > 0 ) {
			
			rows = scan.nextInt();
			cols = scan.nextInt();
			String [] grid = new String [ rows ];
			for( int i = 0; i < rows; i++ ) {
				grid[i] = scan.next();		
			}								//grid is populated
			if( cases != 0 ) {
				//scan.next();				//take care of the extra line after the test case
			}
		
			//grid is now populated as an array of strings
			graph = new Node [ rows ][ cols ];
			for( int i = 0; i < rows; i++ ) {
				for( int j = 0; j < cols; j++ ) {
					char color;
					if( grid[i].charAt(j) == '0' ) {		//pixel is black
						color = 'b';
					} else {							//pixel is white
						color = 'w';
					}
					graph[i][j] = new Node( color, i, j );		//put a new node into our array of nodes
					//System.out.println( graph[i][j] );
					//prints nodes correctly
				}
			}
			//System.out.println( Arrays.deepToString( graph ) );
			//ArrayList of nodes now populated, now add in the neighbors
			for( int i = 0; i < rows; i++ ) {
				for( int j = 0; j < cols; j++ ) {
					//check bounds and add to our arrayList of nodes
					if( i > 0 ) {				//there is a pixel above us
						graph[i][j].addNeighbor( graph[i-1][j] );
					}
					if( i < rows - 1 ) {		//there is a pixel below
						//System.out.println( i + ", " + j );
						graph[i][j].addNeighbor( graph[i+1][j] );
					}
					if( j > 0 ) {				//there is a pixel left
						graph[i][j].addNeighbor( graph[i][j-1] );
					}
					if( j < cols - 1 ) {		//there is a pixel right
						graph[i][j].addNeighbor( graph[i][j+1] );
					}
				}
			}
		
		
		
			//finally finished creating graph, now do a BFS
			//int[][] answer = new int[ rows ][ cols ];		//array to store our answer in
			//str.append( "\n" );
			for( int i = 0; i < rows; i++ ) {
				for( int j = 0; j < cols; j++ ) {
					if( graph[i][j].color == 'b' ) {		//do a bfs if we are at black
						str.append( bfs( graph[i][j],rows,cols ) );
						//System.out.println( "We just looked at " + i + " " + j + " and got: " + answer[i][j] )
					} else {
						str.append(0);
					}
					if( j != cols-1 ) {
						str.append( " " );
					}
				}
				str.append( "\n" );
			}
			
		}
		System.out.print( str );		
	}
	
	public static int bfs( Node m, int rows, int cols ) {
		boolean[][] visited = new boolean[rows][cols];
		ArrayDeque<Node> queue = new ArrayDeque<Node>();
		queue.offer( m );
		visited[m.row][m.col] = true;
		Node n;
	
		while ( !queue.isEmpty() ) {							//while the queue is not empty
			n = queue.poll();									//poll the element off of the queue					
			for( Node visit : n.neighbors ) {					//add all of n's neighbors to the queue
				if( visited[visit.row][visit.col] == false ) {
					//check here for if we reached a white pixel
					if( visit.color == 'w' ) {
						return ( Math.abs( m.row - visit.row ) + Math.abs( m.col - visit.col ) );
					}
					visited[visit.row][visit.col] = true;
					queue.offer( visit );
				}
			}
		}
		return 0;
	}
	
}

class Node {
	public ArrayList< Node > neighbors;
	public char color;
	public int row;
	public int col;
	
	public Node ( char color, int row, int col ) {
		this.color = color;
		this.row = row;
		this.col = col;
		this.neighbors = new ArrayList< Node >();
	}
	
	public void addNeighbor ( Node n ) {
		neighbors.add( n );
	}
	
	public void addNeighbors ( ArrayList< Node > nodes ) {
		neighbors.addAll( nodes );
	}
	
	public String toString ( ) {
		return (color + "\trow: " + row + "\tcol: " + col + "\n");
	}
}



class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
