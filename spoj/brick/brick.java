import java.util.*;
import java.io.*;

class brick {
	public static void main( String [] args ) throws Exception {
		Parser scan  = new Parser( System.in );
		StringBuilder out = new StringBuilder();
		while( true ) {
			
			int lines = scan.nextInt();
			if( lines == 0 ) {
				break;
			}
			int[][] dim = new int [lines][3];
			int minDim = 0;
			//1 array for each respective dimension: width, depth, height
			for( int i = 0; i < lines; i++ ) {
				for( int j = 0; j < 3; j++ ) {
					dim[i][j] = scan.nextInt();
				}
			}
			int[] GCD = new int[3];
			for( int j = 0; j < 3; j++ ) {
				for( int i = 0; i < dim.length; i++ ) {
					if( i == 0 ) {
						GCD[j] = GCD( dim[i][j],dim[i+1][j] );
						//System.out.println( "The GCD of " + dim[i][j] + " and " + dim[i+1][j] + " equals " + GCD[j] );
						i++;
					} else {
						//System.out.print( "The GCD of " + GCD[i] + " and " + dim[i][j] + " equals ");
						GCD[j] = GCD( GCD[j], dim[i][j] );
						//System.out.println( GCD[j] );
					}
				}
			}
			//System.out.println( Arrays.toString( GCD ) );
			int min = GCD[0];
			for( int i = 0; i < GCD.length; i++) {
				if( min > GCD[i] ) {
					min = GCD[i];
				}
			}
			min = min * min * min;			//min = min ^ 3
			long blocks = 0;
			//now calculate the volume
			for( int i = 0; i < dim.length; i++ ) {
				long volume = 1;
				for( int j = 0; j < dim[0].length; j++ ) {
					volume *= dim[i][j];
				}
				blocks += volume/min;
			}
			out.append( blocks + "\n" );
			//volume of 4*5*6 = 120
			//found minDim, now binary search for the correct result
		}
		System.out.print( out );
	}
	
	public static int GCD( int a, int b ) {
		int t = b;
		if( b > a ) {
			b = a;
			a = t;
		}
		while( b != 0 ) {
			t = b;
			b = a%b;
			a = t;
		}
		return a;
	}
}



class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
