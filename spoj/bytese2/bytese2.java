import java.util.*;

class bytese2 {
	
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		int cases = scan.nextInt();
		StringBuilder out = new StringBuilder();
		while( cases-- > 0 ) {
			int entriesAndExits = scan.nextInt();
			HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
			int min = Integer.MAX_VALUE;
			int max = 0;
			
			for( int i = 0; i < entriesAndExits; i++ ) {
				int entry = scan.nextInt();
				int exit = scan.nextInt();
				if( entry < min ) {
					min = entry;
				}
				if ( entry > max ) {
					max = entry;
				}
				if( exit > max ) {
					max = exit;
				}
				if( exit < min ) {
					min = exit;
				}
				
				if( !map.containsKey(entry) ) {
					map.put(entry,0);
				}
				if( !map.containsKey(exit) ) {
					map.put(exit,0);
				}
				map.put( entry, map.get( entry ) + 1 );
				map.put( exit, map.get( exit ) - 1 );
			}
			//System.out.println( map );
			int[] people = new int[ (max - min) + 1 ];
			people[0] = map.get(min);
			int ret = 0;
			for( int i = 1; i < people.length; i++ ) {
				if( map.containsKey( min+i ) ) {
					people[i] = people[i-1] + map.get(min+i);
					if( people[i] > ret ) {
						ret = people[i];
					}
				} else {
					people[i] = people[i-1];
				}
			}
			out.append( ret + "\n" );
		}
		System.out.print( out );
	}	
	
}
