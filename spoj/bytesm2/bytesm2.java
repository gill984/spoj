import java.util.*;
import java.io.*;

class bytesm2
{
    public static void main(String [] args) throws Exception
    {
	Parser in = new Parser(System.in);
	StringBuilder out = new StringBuilder();

	int T = in.nextInt();

	while(T-- > 0)
	{
	    int rows = in.nextInt();
	    int cols = in.nextInt();

	    int [][] grid = new int[rows][cols];

	    for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
		    grid[i][j] = in.nextInt();

	    out.append(findMax(grid) + "\n");
	}

	System.out.print(out.toString());
    }

    public static int findMax(int[][] dp)
    {
	for(int i = 1; i < dp.length; i++)
	{
	    for(int j = 0; j < dp[0].length; j++)
	    {
		int max = dp[i-1][j];
		if(j > 0)
		    max = Math.max(max, dp[i-1][j-1]);
		if(j < dp[0].length - 1)
		    max = Math.max(max, dp[i-1][j+1]);

		dp[i][j] += max;
	    }
	}

	int max = Integer.MIN_VALUE;
	for(int i = dp.length - 1, j = 0; j < dp[i].length; j++)
	{
	    max = Math.max(max, dp[i][j]);
	}

	return max;
    }
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
