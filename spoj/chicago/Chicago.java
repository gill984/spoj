import java.util.*;

class Chicago {
	public static void main( String[] args ) {
		StringBuilder ret = new StringBuilder();
		Scanner scan = new Scanner( System.in );
		while( true ) {
			int n = scan.nextInt();
			if( n == 0 ) {
				break;
			}
			int m = scan.nextInt();
			node[] nodes = new node[n+1];
			System.out.println( n );
			for(int i = 0; i < nodes.length; i++) {
				nodes[i] = new node( i,n+1 );
			}
			//System.out.println( nodes[4].num );
			for( int i = 0; i < m; i++ ) {
				int node1 = scan.nextInt();
				int node2 = scan.nextInt();
				double cost = scan.nextDouble()/100;
				//System.out.println( "Adding " + node1 + " " + node2);
				nodes[ node1 ].addAdj( node2,cost );
				nodes[ node2 ].addAdj( node1,cost );
				//added costs for all nodes there and back
			}
			for( node n1 : nodes ) {
				System.out.println( n1.num + " " + Arrays.toString(n1.adj) );
			}
			HashSet<Integer> visited = new HashSet<Integer>();
			double[] maxChance = new double[ n+1 ];
			Arrays.fill( maxChance,0 );	//infinity
			maxChance[1] = 1;
			visited.add( 1 );
			boolean done = false;
			//done initializing for dijkstra's
			int count = 0;
			while( true ) {
				//relax the bounds
				int maxNode = 1;
				double maxCost = 0;
				boolean nextFound = false;
				for( int v : visited ) {
					for( int k = 1; k<n; k++ ) {
						if( maxCost < maxChance[v] * nodes[v].adj[k] && !visited.contains( k )  ) {		//finding the max cost
							maxCost = maxChance[v] * nodes[v].adj[k];
							maxNode = k;
							nextFound = true;
						}
					}
				}
				//we found the choice we want to make
				//now add to visited
				visited.add( maxNode );
				maxChance[maxNode] = maxCost;
				System.out.println( "Step " + ++count );
				System.out.println( "pick up node: " + maxNode );
				System.out.println( "cost to this node: " + maxCost);
				if( count == 10 ) break;
				if( visited.contains( n ) ) {
					System.out.println( maxChance[n] );
					break;
				}
			}
		}
		
		
		
		
				
	}
}

class node {
	public int num;
	public double[] adj;

	public node ( int num,int size ) {
		this.num = num;
		this.adj = new double[ size ];
	}
	
	public void addAdj( int n, double val ) {
		adj[n] = val;		//index is the number of the node, val is the weight
	}	
}
