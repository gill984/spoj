import java.util.Scanner;

class cmpls
{
    public static void main(String[] args) throws Exception
    {
        // Read Input
        Scanner in = new Scanner(System.in);
        StringBuilder out = new StringBuilder();
        int tc = in.nextInt();
        while (tc-- > 0)
        {
            int n = in.nextInt();
            int u = in.nextInt();
            int[][] d = new int[n][n];

            for (int i = 0; i < n; i++)
            {
                d[0][i] = in.nextInt();
            }

            // Compute Difference Table
            for (int i = 1; i < d.length; i++)
            {
                for (int j = 0; j < n - i; j++)
                {
                    d[i][j] = d[i - 1][j + 1] - d[i - 1][j];
                }
            }

            for (int t = 1; t <= u; t++)
            {
                // Value for x. The next term in the list.
                int x = t + n;

                int sum = 0;
                for (int i = 0; i < d.length; i++)
                {
                    sum += computeTerm(d[i][0], i, x);
                }

                out.append(sum + " ");
            }

            out.append("\n");
        }
        System.out.print(out);
    }

    public static int computeTerm(int delta, int i, int x)
    {
        if(delta == 0)
        {
            return 0;
        }

        int numerator = 1;
        int denom = 2;
        
        for (int j = i; j > 0; j--)
        {
            numerator *= (x - j);
            while(numerator % denom == 0 && denom <= i)
            {
                numerator /= denom;
                denom++;
            }
        }

        return (delta * numerator);
    }
}
