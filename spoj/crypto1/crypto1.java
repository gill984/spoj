import java.io.DataInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.util.TimeZone;

class StringTest
{
   public static void main(String[] args) throws Exception
   {
      // Read Input
      Scanner in = new Scanner(System.in);
      long a = in.nextLong();

      long p = 4000000007L;
      long k = (((p - 1) / 2 - 1) / 2);

      // long x = modpow(a, k + 1, p);

      BigInteger A = new BigInteger("" + a);
      BigInteger K = new BigInteger("" + (k + 1));
      BigInteger P = new BigInteger("" + p);

      BigInteger X = A.modPow(K, P);

      // if (x > p / 2)
      // {
      // x = p - x;
      // }

      long x = Long.valueOf(X.toString());

      if (x > p / 2)
      {
         x = p - x;
      }

      Date d = new Date(x * 1000);

      SimpleDateFormat formatter;
      formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy", Locale.ROOT);
      formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
      System.out.println(formatter.format(d));

   }

   public static long modpow(long a, long b, long p)
   {
      long result = 1;

      long multiplier = a;
      long powerOf = 1;
      while (powerOf <= b)
      {
         if (0 < (powerOf & b))
         {
            result = result * multiplier;
            result = result % p;
         }
         multiplier = (multiplier * multiplier) % p;
         powerOf = powerOf << 1;
      }

      return result;
   }
}

class Parser
{
   final private int BUFFER_SIZE = 1 << 16;

   private final DataInputStream din;
   private final byte[] buffer;
   private int bufferPointer, bytesRead;

   public Parser(InputStream in)
   {
      din = new DataInputStream(in);
      buffer = new byte[BUFFER_SIZE];
      bufferPointer = bytesRead = 0;
   }

   public int nextInt() throws Exception
   {
      int ret = 0;
      byte c = read();
      while (c <= ' ')
         c = read();
      boolean neg = c == '-';
      if (neg)
         c = read();
      do
      {
         ret = ret * 10 + c - '0';
         c = read();
      }
      while (c > ' ');
      if (neg)
         return -ret;
      return ret;
   }

   public double nextDouble() throws Exception
   {
      double toRet = 0.0;
      int ret = 0;
      byte c = read();
      while (c <= ' ')
         c = read();
      do
      {
         ret = ret * 10 + c - '0';
         c = read();
      }
      while (c > ' ' && c != '.');
      int ret2 = 0;
      double mult = 1.0;
      if (c == '.')
      {
         c = read();
         do
         {
            ret2 = ret2 * 10 + c - '0';
            mult *= .1;
            c = read();
         }
         while (c > ' ');
         toRet += ret2 * mult;
      }
      return toRet + ret;
   }

   public String nextString(int length) throws Exception
   {
      StringBuilder br = new StringBuilder();
      byte c = read();
      while (c <= ' ')
         c = read();
      for (int i = 0; i < length; ++i)
      {
         br.append((char) c);
         c = read();
      }
      return br.toString();
   }

   public String next() throws Exception
   {
      StringBuilder br = new StringBuilder();
      byte c = read();
      while (c <= ' ')
         c = read();
      while (c > ' ')
      {
         br.append((char) c);
         c = read();
      }
      return br.toString();
   }

   private void fillBuffer() throws Exception
   {
      bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
      if (bytesRead == -1)
         buffer[0] = -1;
   }

   private byte read() throws Exception
   {
      if (bufferPointer == bytesRead)
         fillBuffer();
      return buffer[bufferPointer++];
   }
}