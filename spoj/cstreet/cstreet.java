import java.util.*;
import java.io.*;

class cstreet {
	public static void main( String[] args ) throws Exception {
		Parser in = new Parser(System.in);
		StringBuilder out = new StringBuilder();
		int cases = in.nextInt();
		while( cases-- > 0 ) {
			int price = in.nextInt();
			int numNodes = in.nextInt();
			int numEdges = in.nextInt();
			//create the graph
			ArrayList< ArrayList<Edge> > graph = new ArrayList< ArrayList<Edge> >();
			for( int i = 0; i < numNodes; i++ ) {
				graph.add( new ArrayList<Edge>() );
			}
			for( int i = 0; i < numEdges; i++ ) {
				int node1 = in.nextInt()-1;
				int node2 = in.nextInt()-1;
				int cost = in.nextInt();
				graph.get(node1).add( new Edge(node1,node2,cost) );
				graph.get(node2).add( new Edge(node2,node1,cost) );
			}
			//graph populated
			//ArrayList<Edge> mst = new ArrayList<Edge>();
			int count = 0;
			boolean[] visited = new boolean[numNodes];
			PriorityQueue<Edge> queue = new PriorityQueue<Edge>();
			visited[0] = true;
			for( Edge e : graph.get(0) ) {
				queue.add(e);
			}
			while( !queue.isEmpty() ) {
				Edge e = queue.poll();
				if( visited[e.to] ) {
					continue;
				} else {
					visited[e.to] = true;
					//mst.add(e);
					count += e.weight;
					for( Edge adj : graph.get(e.to) ) {
						queue.add(adj);
					}
				}
			}
			count *= price;
			out.append( count + "\n" );			
		}
		System.out.print( out );
	}
}

class Edge implements Comparable<Object>{
	public int from, to, weight;
	
	public Edge( int from, int to, int weight ) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}
	
	public int compareTo( Object o ) {
		Edge e = (Edge)o;
		return this.weight - e.weight;
	}
	
	public String toString() {
		return ( "From: " + from + ", to: " + to + ", weight: " + weight );
	}	
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
