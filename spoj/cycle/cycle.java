import java.util.*;
import java.io.*;

class cycle {
	public static void main( String[] args ) {
		int size = 21;
		long[][] dp = new long[21][21];
		dp[1][1] = 1;
		dp[2][1] = 1;
		
		for( int i = 2; i < size; i++ ) {
			dp[i][1] = (i-1) * dp[i-1][1];
			dp[i][2] = dp[i-1][1];
			//System.out.println( dp[i][1] );
		}
		
		for( int i = 4; i < size; i++ ) {
			for( int j = 3; j <= i; j++ ) {
				dp[i][j] = dp[i][2] * (j-1);
				if( j == i ) {
					dp[i][j] = (fact(i)/2);
				}
			}
		}
		
		
		
		for( int i = 0; i < size; i++ ) {
			for( int j = 0; j <= i; j++ ) {
				System.out.print( dp[i][j] + " " );
			}
			System.out.println();
		}
		
		
	}
	
	public static int fact( int n ) {
		for( int i = n; i > 0; i-- ) {
			n = n * i;
		}
		return n;
	}
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
