import java.util.*;
import java.io.*;

class database {
	public static void main( String [] args ) throws Exception{
		Parser scan = new Parser( System.in );
		int cases = scan.nextInt();
		int count = 1;
		while( cases-- > 0 ) {
			int numStudents = scan.nextInt();
			int numLines = scan.nextInt();
			HashSet< Integer > classes = new HashSet< Integer >();
			//set of all classes which have been added previously
			HashMap< Integer,HashSet< Integer > > sets = new HashMap< Integer, HashSet<Integer> > ();
			//hashmap from class numbers to sets containing the ids of who is in that class
			boolean good = true;
			for( int i = 0; i < numLines; i++ ) {
				int student = scan.nextInt();
				int classNum = scan.nextInt();
				if( classes.add( classNum ) ) {
					HashSet< Integer > set = new HashSet<Integer>();
					set.add( student );
					sets.put( classNum, set );
				} else if( !sets.get( classNum ).add( student ) ) {		//this hashset has previously been created
					good = false;
					break;
				}
			}
			if( good ) {
				System.out.println( "Scenario #" + count + ": possible" );
			} else {
				System.out.println( "Scenario #" + count + ": impossible" );
			}
			count++;
			
		}	
	}
}



class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
