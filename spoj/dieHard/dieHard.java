import java.util.*;
import java.io.*;

class dieHard {
	
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		int cases = scan.nextInt();
		while( cases-- > 0 ) {
			int startHealth = scan.nextInt();
			int startArmor = scan.nextInt();
			int max = 0;
			if( startHealth >= startArmor ) {
				max = startHealth;
			} else {
				max = startArmor;
			}
			pair[][] states = new pair[max][max];
			states[0][0] = new pair( startHealth, startArmor );
			for( int i = 1; i < states.length; i++ ) {
				boolean allDead = true;
				states[i][0] = states[i-1][0].fire();				//step into the fire to move directly down a column
				states[i][0].dead();								//set this cell as dead if we are dead here
				if( !states[i][0].dead ) {						//are we dead?
					allDead = false;								//if I'm not dead, then we aren't all dead
				}
				//System.out.print( states[i][0] + "\t" );
				for( int j = 1; j <= i; j++ ) {						//try stepping into water from each previous position
					states[i][j] = states[i-1][j-1].water();
					//System.out.print( states[i][j] + "\t" );
					states[i][j].dead();								//set this cell as dead if we are dead here
					if( !states[i][j].dead ) {						//are we dead?
						allDead = false;								//if I'm not dead, then we aren't all dead
					}
				}
				//System.out.println();
				if( allDead ) {
					System.out.println( (i*2)-1 );
					break;
				}
				
			}
		}
	}
}

class pair {
	static final int fHealthDrop = -17;
	static final int fArmorDrop = 7;
	static final int wHealthDrop = -2;
	static final int wArmorDrop = -8;

	public int health;
	public int armor;
	public boolean dead;
	public pair( int health, int armor ) {
		this.health = health;
		this.armor = armor;
		this.dead = false;
	}
	
	public pair fire() {
		return new pair( this.health+fHealthDrop, this.armor+fArmorDrop );
	}
	
	public pair water() {
		return new pair( this.health+wHealthDrop, this.armor+wArmorDrop );
	}
	
	public boolean better( pair p ) {
		if( this.armor > p.armor && this.health > p.health ) {
			return true;
		} else {
			return false;
		}
	}
	
	public void dead() {
		if( this.health <= 0 || this.armor <= 0 ) {
			this.dead = true;
		} else {
			return;
		}
	}
	
	public String toString() {
		return ( "Health: " + health + ", " + "Armor: " + armor );
	}
}	
