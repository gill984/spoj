import java.util.*;

class diagonal {

	public static long MOD = 24 * 1000000007L;			//1 billion 7

	public static void main ( String [] args ) {
		Scanner scan = new Scanner( System.in );
		int cases = scan.nextInt();
		while( cases-- > 0 ) {
			long n = scan.nextLong();
			long perm = n;
			for( int i = 1; i <= 3; i++ ) {
				n = modMult( n, perm - i );
			}
			System.out.println( n/24 );
		}
	}
	
	public static long modMult ( long a, long val ) {			//pass multiplications to this function
		val = val * a;
		if( val >= MOD ) {
			val %= MOD;
		}
		//System.out.println( "Value: " + val );
		return val;
	}
}
