import java.io.DataInputStream;
import java.io.InputStream;

class dquery
{
    public static void main(String[] args) throws Exception {
        Parser in = new Parser(System.in);
        StringBuilder out = new StringBuilder();

        int n = in.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++)
            arr[i] = in.nextInt();

        segmentTree st = new segmentTree(arr);

        int m = in.nextInt();

        for (int i = 0; i < m; i++) {
            out.append(st.maxSubSum(in.nextInt(), in.nextInt()) + "\n");
        }
        System.out.print(out);
    }
}

class segmentTree {
    public Node[] tree;
    public int[] arr;

    public segmentTree(int[] arr) {
        this.arr = arr;
        int ceil_log_n = (int) Math.ceil((Math.log(arr.length) / Math.log(2)));
        tree = new Node[(1 << (ceil_log_n + 1)) - 1];
        build(0, 1, arr.length);
    }

    private void build(int index, int b, int e) {
        if (b > e) {
            return;
        } else if (b == e) {
            int a = arr[b - 1];
            tree[index] = new Node(a, a, a, a);
            return;
        }

        build(leftChild(index), b, (b + e) / 2);
        build(rightChild(index), (b + e) / 2 + 1, e);

        tree[index] = Node.merge(tree[leftChild(index)],
                tree[rightChild(index)]);

    }

    public int maxSubSum(int l, int r) {
        Node interval = getInterval(l, r, 0, 1, arr.length);
        return interval.max;
    }

    private Node getInterval(int l, int r, int index, int nl, int nr) {
        int mid = (nl + nr) / 2;

        if (nl >= l && nr <= r) {
            return tree[index];
        } else if (l > mid) {
            return getInterval(l, r, rightChild(index), mid + 1, nr);
        } else if (r <= mid) {
            return getInterval(l, r, leftChild(index), nl, mid);
        } else {
            return Node.merge(getInterval(l, r, leftChild(index), nl, mid),
                    getInterval(l, r, rightChild(index), mid + 1, nr));
        }
    }

    private int leftChild(int parentIndex) {
        return parentIndex * 2 + 1;
    }

    private int rightChild(int parentIndex) {
        return parentIndex * 2 + 2;
    }
}

class Node 
{
    HashSet<Integer> set;

    public Node(int n)
    {
        set = new HashSet<Integer>();
	set.add(n);
    }

    public Node(HashSet<Integer> set)
    {
	this.set = set;
    }

    public static Node merge(Node lc, Node rc) 
    {
	Node merged;
	if(lc.size() < rc.size())
	{
	    merged = new Node(rc);
	    for(Integer i : lc)
		merged.set.add(i);
	}
	else
	{
	    merged = new Node(lc);
	    for(Integer i : rc)
		merged.set.add(i);
	}

	return merged;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();

	for(Integer i : set)
	    out.append(i + " ");

	out.append("\n");

	return out.toString();
    }
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private final DataInputStream din;
    private final byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in) {
        din = new DataInputStream(in);
        buffer = new byte[BUFFER_SIZE];
        bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception {
        int ret = 0;
        byte c = read();
        while (c <= ' ')
            c = read();
        boolean neg = c == '-';
        if (neg)
            c = read();
        do {
            ret = ret * 10 + c - '0';
            c = read();
        } while (c > ' ');
        if (neg)
            return -ret;
        return ret;
    }

    public double nextDouble() throws Exception {
        double toRet = 0.0;
        int ret = 0;
        byte c = read();
        while (c <= ' ')
            c = read();
        do {
            ret = ret * 10 + c - '0';
            c = read();
        } while (c > ' ' && c != '.');
        int ret2 = 0;
        double mult = 1.0;
        if (c == '.') {
            c = read();
            do {
                ret2 = ret2 * 10 + c - '0';
                mult *= .1;
                c = read();
            } while (c > ' ');
            toRet += ret2 * mult;
        }
        return toRet + ret;
    }

    public String nextString(int length) throws Exception {
        StringBuilder br = new StringBuilder();
        byte c = read();
        while (c <= ' ')
            c = read();
        for (int i = 0; i < length; ++i) {
            br.append((char) c);
            c = read();
        }
        return br.toString();
    }

    public String next() throws Exception {
        StringBuilder br = new StringBuilder();
        byte c = read();
        while (c <= ' ')
            c = read();
        while (c > ' ') {
            br.append((char) c);
            c = read();
        }
        return br.toString();
    }

    private void fillBuffer() throws Exception {
        bytesRead = din.read(buffer, buferPointer = 0, BUFFER_SIZE);
        if (bytesRead == -1)
            buffer[0] = -1;
    }

    private byte read() throws Exception {
        if (bufferPointer == bytesRead)
            fillBuffer();
        return buffer[bufferPointer++;
    }
}
