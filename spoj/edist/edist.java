import java.util.Scanner;
import java.util.Arrays;
import java.io.*;

class edist
{
    public static void main(String [] args) throws Exception
    {
	Parser in = new Parser(System.in);
	StringBuilder out = new StringBuilder();

	int T = in.nextInt();

	for(int i = 0; i < T; i++)
	{
	    String s1 = in.next();
	    String s2 = in.next();
	    int answer = editDistance(s1, s2);
	    out.append(answer + "\n");
	}

	System.out.print(out.toString());
    }

    public static int editDistance(String string1, String string2)
    {
	char [] s1 = string1.toCharArray();
	char [] s2 = string2.toCharArray();

	int [][] dp = new int [s1.length + 1][s2.length + 1];
	for(int i = 0; i < dp.length; i++)
	    dp[i][0] = i;

	for(int j = 0; j < dp[0].length; j++)
	{
	    dp[0][j] = j;
	}

	for(int i = 1; i < dp.length; i++)
	{
	    for(int j = 1; j < dp[0].length; j++)
	    {
		if(s1[i-1] == s2[j-1])
		{
		    dp[i][j] = dp[i-1][j-1];
		}
		else
		{
		    dp[i][j] = Math.min(Math.min(dp[i-1][j-1], dp[i-1][j]), dp[i][j-1]) + 1;
		}
	    }
	}

	// for(int i = 0; i < dp.length; i++)
	// {
	//     System.out.println(Arrays.toString(dp[i]));
	// }

	return dp[s1.length][s2.length];
    }
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}


