import java.util.*;

class evenOdd {
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		int cases = scan.nextInt();
		int count = 1;
		while( cases-- > 0 ) {
			System.out.println( "Case #" + count++ + ":" );
			int n = scan.nextInt();
			scan.nextInt();
			if( n % 2 == 0 ) {
				System.out.println( "even even" );
			} else {
				System.out.println( "even odd" );
			}
		}
	}
}
