import java.util.*;
import java.io.*;

class factorial{

	public static void main(String[] args) {
	
		Scanner s = new Scanner(System.in);
		int x = s.nextInt();
		
		for(int i =0; i<x; i++) {
			
			long num = s.nextLong();
			long temp = num;
			long extra = 0;
			long divisor = 5;
			
			while(temp > 0) {
				temp = num/divisor;
				extra += (temp);
				divisor *= 5;
				
			}
			
		System.out.println(extra);
		}
	
	}
}