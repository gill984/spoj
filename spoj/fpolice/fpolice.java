import java.util.*;
import java.io.*;

class fpolice {
	public static void main( String[] args ) throws Exception {
		Parser in = new Parser( System.in );
		StringBuilder out = new StringBuilder();
		int cases = in.nextInt();
		while( cases-- > 0 ) {
			int numNodes = in.nextInt();
			int time = in.nextInt();
			int[][] times = new int[numNodes][numNodes];
			int[][] risks = new int[numNodes][numNodes];
			
			for( int k = 0; k < 2; k++ ) {
				for( int i = 0; i < numNodes; i++ ) {
					for( int j = 0; j < numNodes; j++ ) {
						if( k == 0 ) {
							times[i][j] = in.nextInt();
						} else {
							risks[i][j] = 	in.nextInt();
						}
					}
				}
			}
			//adjacency matrices populated!
			PriorityQueue<Path> paths = new PriorityQueue<Path>();
			paths.offer( new Path(0,0,0) );
			boolean found = false;
			//set up, time to dijkstra
			
			while( !paths.isEmpty() ) {
				Path p = paths.poll();
				if( p.current == numNodes-1 ) {
					out.append( p.risk + " " + p.time + "\n" );
					found = true;
					break;
				}
				for( int j = 0; j < numNodes; j++ ) {
					if( j == p.current ) {
						continue;	//don't add the same path to the queue
					}
					if( p.time + times[p.current][j] <= time ) {
						paths.offer( new Path( j, p.time + times[p.current][j], p.risk + risks[p.current][j] ) );
					}
				}
			}
			
			if( !found ) {
				out.append( "-1\n" );
			}				
		}
		System.out.print( out );
	}
}

class Path implements Comparable<Object> {
	public int current;
	public int time;
	public int risk;
	
	public Path( int c, int t, int r ) {
		this.current = c;
		this.time = t;
		this.risk = r;
	}
	
	public int compareTo( Object o ) {
		return ( this.risk - ((Path)o).risk );
	}
	
	public String toString() {
		return ( "Current node: " + current + ", time: " + time + ", risk: " + risk );
	}
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
