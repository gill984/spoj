import java.util.*;
import java.io.*;

class guessThe {
	public static void main( String [] args ) throws Exception {
		
		StringBuilder str = new StringBuilder();
		
		Parser scan = new Parser( System.in );
		while( true ) {
			String line = scan.next();
			char[] chars = line.toCharArray();
		
			if( chars[0] == '*' ) {
				break;
			}
		
			long lcm = 1;
			ArrayList< Integer > nos = new ArrayList< Integer >();
		
			for( int i = 0; i < chars.length; i++ ) {
				if( chars[i] == 'Y' ) {
					lcm = LCM( lcm, i+1 );
				} else {
					nos.add( i + 1 );
				}
			}
		
			
		
			boolean exists = true;
			for( long q : nos ) {
				if( lcm % q == 0 ) {
					exists = false;
				}
			}
		
			if( exists ) {
				str.append( lcm + "\n" );
			} else {
				str.append( "-1\n" );
			}
		}
		System.out.print( str );	
	}
	
	public static long LCM( long a, long b ) {
		long oldA = a;
		long oldB = b;
		while( b != 0 ) {
			long t = b;
			b = a % b;
			a = t;
		}
		return ((oldA*oldB) / a);
	}
	
	


}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
