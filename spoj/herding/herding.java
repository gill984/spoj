import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.ArrayDeque;

class herding
{
    public static void main(String [] args) throws Exception
    {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        String [] rowsCols = in.readLine().split(" ");

        int numRows = Integer.parseInt(rowsCols[0]);
        int numCols = Integer.parseInt(rowsCols[1]);

        char [][] city = new char[numRows][numCols];

        for(int i = 0; i < city.length; i++)
        {
            city[i] = in.readLine().toCharArray();
        }

        int numTraps = minNumTraps(city);

        System.out.println(numTraps);
    }

    public static int minNumTraps(char[][] city)
    {
        int numTraps = 0;

        int [][] visited = new int[city.length][city[0].length];

        for(int i = 0; i < city.length; i++)
        {
            for(int j = 0; j < city[0].length; j++)
            {
                if(visited[i][j] == 2) continue;

                ArrayDeque<Pair> q = new ArrayDeque<Pair>();
                q.add(new Pair(i, j));

                while(!q.isEmpty())
                {
                    Pair p = q.peekLast();

                    // 0 means this block is unvisited in the city
                    if(visited[p.row][p.col] == 0)
                    {
                        visited[p.row][p.col] = 1;

                        // Based on direction, add the next block to the queue
                        char direction = city[p.row][p.col];

                        if(direction == 'N')
                        {
                            q.add(new Pair(p.row - 1, p.col));
                        }
                        else if(direction == 'S')
                        {
                            q.add(new Pair(p.row + 1, p.col));
                        }
                        else if(direction == 'W')
                        {
                            q.add(new Pair(p.row, p.col - 1));
                        }
                        else if(direction == 'E')
                        {
                            q.add(new Pair(p.row, p.col + 1));
                        }
                    }
                    else if(visited[p.row][p.col] == 1)
                    {
                        // This is a new path, need a new trap, and break
                        numTraps += 1;
                        break;
                    }
                    else if(visited[p.row][p.col] == 2)
                    {
                        // This is not a new path, no new trap, just break
                        break;
                    }
                }

                // At this point we either reached a 1 or 2
                // Clean up the entire path by popping the whole queue and
                // marking everything as a 2.
                while(!q.isEmpty())
                {
                    Pair p = q.pop();
                    visited[p.row][p.col] = 2;
                }
            }
        }

        return numTraps;
    }

}

class Pair
{
    public int row;
    public int col;

    public Pair(int row, int col)
    {
        this.row = row;
        this.col = col;
    }
}
