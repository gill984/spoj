import java.util.Arrays;
import java.util.*;
import java.io.*;

class horrible
{

    public static SegmentTree st;

    public static void main(String [] args) throws Exception
    {
        Parser in = new Parser(System.in);
        StringBuilder out = new StringBuilder();

        int tc = in.nextInt();
        while(tc-- > 0)
        {
            int n = in.nextInt();
            int c = in.nextInt();

            int [] arr = new int[n];
            int [][] queries = new int [c][4];

            for(int i = 0; i < c; i++)
            {
                queries[i][0] = in.nextInt();
                for(int j = 1; j < 4 - queries[i][0]; j++)
                {
                    queries[i][j] = in.nextInt();
                }
            }

            //            for(int i = 0; i < queries.length; i++)
            //                System.out.println(Arrays.toString(queries[i]));

            st = new SegmentTree(arr);

            for(int i = 0; i < queries.length; i++)
            {
                int p = queries[i][1];
                int q = queries[i][2];
                if(queries[i][0] == 0)
                {
                    // Note the index passed in is 1 greater than the
                    // corresponding index in the input array.
                    st.lazyUpdate(p, q, 1, arr.length, 0, queries[i][3]);
                }
                else
                {
                    out.append(st.getSum(p, q) + "\n");
                }
                // System.out.println(st.toString());
            }

            //            System.out.println(st);
        }
        System.out.print(out.toString());
    }
}

class SegmentTree {
    public long[] tree;
    public long[] lazy;
    public int[] arr;
    public int start;

    public SegmentTree(int[] arr)
    {
        this.arr = arr;
        int ceil_log_n = (int) Math.ceil((Math.log(arr.length) / Math.log(2)));
        tree = new long[(1 << (ceil_log_n + 1)) - 1];
        lazy = new long[tree.length];
        start = (1 << ceil_log_n) - 1;
    }

    // Updates an index in the segment tree
    public void lazyUpdate(int l, int r, int nL, int nR, int index, int v)
    {
        lazyShift(index, nL, nR);

        int mid = (nL + nR) / 2;
        if(nL > r || nR < l ||  nL > nR)
        {
            return;
        }
        else if (nL >= l && nR <= r)
        {
            tree[index] += v * (nR - nL + 1);
            if(nL != nR)
            {
                lazy[rightChild(index)] += v;
                lazy[leftChild(index)] += v;
            }
            return;
        }
        else
        {
            lazyUpdate(l, r, nL, mid, leftChild(index), v);
            lazyUpdate(l, r, mid + 1, nR, rightChild(index), v);
        }

        tree[index] = tree[leftChild(index)] + tree[rightChild(index)];
        return;

    }

    public long getSum(int l, int r)
    {
        long interval = getInterval(l, r, 0, 1, arr.length);
        return interval;
    }

    private void lazyShift(int index, int nl, int nr)
    {
        if(lazy[index] > 0)
        {

            // System.out.printf("Lazy[%d] = %d shifting\n", index, lazy[index]);
            tree[index] += lazy[index] * (nr - nl + 1);
            if(nr != nl)
            {
                lazy[rightChild(index)] += lazy[index];
                lazy[leftChild(index)] += lazy[index];
            }
            lazy[index] = 0;
        }
    }

    private long getInterval(int l, int r, int index, int nl, int nr)
    {
        lazyShift(index, nl, nr);

        int mid = (nl + nr) / 2;
        if(nl > r || nr < l ||  nl > nr)
        {
            return 0;
        }
        else if (nl >= l && nr <= r)
        {
            return tree[index];
        }
        else
        {
            return getInterval(l, r, leftChild(index), nl, mid) +
                getInterval(l, r, rightChild(index), mid + 1, nr);
        }
    }

    private boolean isLeaf(int tIndex)
    {
        return (tIndex >= start && tIndex < tree.length);
    }

    private int parent(int childIndex)
    {
        return (childIndex - 1) / 2;
    }

    private int leftChild(int parentIndex) {
        return parentIndex * 2 + 1;
    }

    private int rightChild(int parentIndex) {
        return parentIndex * 2 + 2;
    }

    public String toString()
    {
        StringBuilder out = new StringBuilder();

        out.append("Tree ");
        for(int i = 0; i < tree.length; i++)
        {
            out.append(String.format("%d: %d, ", i, tree[i]));
        }

        out.append("\n");

        out.append("Lazy ");

        for(int i = 0; i < tree.length; i++)
        {
            out.append(String.format("%d: %d, ", i, lazy[i]));
        }
        return out.toString();
    }
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private final DataInputStream din;
    private final byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
        din = new DataInputStream(in);
        buffer = new byte[BUFFER_SIZE];
        bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
        int ret = 0;
        byte c = read();
        while (c <= ' ') c = read();
        boolean neg = c == '-';
        if (neg) c = read();
        do
        {
            ret = ret * 10 + c - '0';
            c = read();
        } while (c > ' ');
        if (neg) return -ret;
        return ret;
    }

    public double nextDouble() throws Exception {
        double toRet = 0.0;
        int ret = 0;
        byte c = read();
        while (c <= ' ') c = read();
        do
        {
            ret = ret * 10 + c - '0';
            c = read();
        } while (c > ' ' && c != '.');
        int ret2 = 0;
        double mult = 1.0;
        if (c == '.') {
            c = read();
            do {
                ret2 = ret2 * 10 + c - '0';
                mult *= .1;
                c = read();
            } while ( c > ' ');
            toRet += ret2*mult;
        }
        return toRet + ret;
    }

    public String nextString(int length) throws Exception {
        StringBuilder br = new StringBuilder();
        byte c = read();
        while(c <= ' ') c = read();
        for(int i = 0; i < length; ++i) {
            br.append((char)c);
            c = read();
        }
        return br.toString();
    }

    public String next() throws Exception{
        StringBuilder br = new StringBuilder();
        byte c = read();
        while(c <= ' ') c = read();
        while(c > ' ') {
            br.append((char)c);
            c = read();
        }
        return br.toString();
    }

    private void fillBuffer() throws Exception
    {
        bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
        if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
        if (bufferPointer == bytesRead) fillBuffer();
        return buffer[bufferPointer++];
    }
}
