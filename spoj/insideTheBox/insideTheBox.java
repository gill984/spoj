import java.util.*;
import java.awt.Rectangle;

class insideTheBox {
	public static void main( String[] args ) {
		StringBuilder build = new StringBuilder();
		Scanner scan = new Scanner( System.in );
		int cases = scan.nextInt();
		while( cases-- > 0 ) {
			int numData = scan.nextInt();
			ArrayList< Pair > dataBoxes = new ArrayList< Pair >();
			ArrayList< Pair > queryBoxes = new ArrayList< Pair >();
			HashSet< String > set = new HashSet< String >();
			scan.nextLine();
			for( int i = 0; i< numData; i++ ) {
				String point = scan.nextLine();
				String point1 = point.replace( ",", " " );
				Scanner scanStr = new Scanner( point1 );

				int y1 = scanStr.nextInt();	//latitude comes first
				int x1 = scanStr.nextInt();	//then longitude
				int y2 = scanStr.nextInt();
				int x2 = scanStr.nextInt();
				if (x2 < x1) {
					dataBoxes.add(new Pair(x1, 180, y1, y2, point));
					dataBoxes.add(new Pair(-180, x2, y1, y2, point));
				}
				else
					dataBoxes.add(new Pair(x1, x2, y1, y2, point));
			}
				
			
			int numQuery = scan.nextInt();
			scan.nextLine();
			for( int i = 0; i< numQuery; i++ ) {
				String point = scan.nextLine();
				String point1 = point.replace( ",", " " );
				Scanner scanStr = new Scanner( point1 );

				int y1 = scanStr.nextInt();	//latitude comes first
				int x1 = scanStr.nextInt();	//then longitude
				int y2 = scanStr.nextInt();
				int x2 = scanStr.nextInt();
				if (x2 < x1) {
					queryBoxes.add(new Pair(x1, 180, y1, y2, point));
					queryBoxes.add(new Pair(-180, x2, y1, y2, point));
				}
				else
					queryBoxes.add(new Pair(x1, x2, y1, y2, point));
				
			}
			boolean happened = false;
			//done creating the arrayLists necessary
			for( Pair pD : dataBoxes ) {
				for( Pair pQ : queryBoxes ) {
				//System.out.println( "got here" );
					boolean workedX = false;
					boolean workedY = false;
					if (pD.xT > pQ.xT) {
						if (pD.xT <= pQ.xB)
							workedX = true;
					}
					else if (pD.xT < pQ.xT) {
						if (pD.xB >= pQ.xT)
							workedX = true;
					}
					else
						workedX = true;

					if (pD.yT > pQ.yT){
						if (pD.yB <= pQ.yT)
							workedY = true;
					}
					else if (pD.yT < pQ.yT){
						if (pD.yT >= pQ.yB)
							workedY = true;
					}
					else
						workedY = true;
						
					if (workedX && workedY){
						if (set.add(pD+"")){
							build.append(pD+"\n");
							happened = true;
						}
					}
				}
			}
			if (!happened)
				build.append("No data found.\n");
			if ( cases > 0 )
				build.append("\n");
			//System.out.println( queryBoxes.toString() );
			//System.out.println( dataBoxes.toString() );
		}
		
		
		System.out.print( build );
	}
	

	
}

class Pair {
	
	
	public int xT;
	public int xB;
	public int yT;
	public int yB;
	public String str;

	public Pair( int a, int b, int c, int d, String str ) {
		xT = a;
		xB = b;
		yT = c;
		yB = d;
		this.str = str;
	}
	
	
	public String toString() {
		return str;
	}
}
