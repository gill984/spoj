import java.util.Scanner;
import java.util.Arrays;

class invcnt
{
    static long INVCNT = 0;
    public static void main(String [] args)
    {
        Scanner in = new Scanner(System.in);

        int T = in.nextInt();

        while(T-- > 0)
        {
            int n = in.nextInt();
            int [] A = new int [n];

            for(int i = 0; i < n; i++)
            {
                A[i] = in.nextInt();
            }


            mergeSort(A);
            System.out.println(INVCNT);
            INVCNT = 0;
        }
    }

    public static void mergeSort(int [] arr)
    {
        if(arr.length > 1)
        {
            // Split into left and right subarrays
            int [] left = subArray(arr, 0, arr.length/2);
            int [] right = subArray(arr, arr.length/2, arr.length);

            // Recursively call sort on the 2 halves
            mergeSort(left);
            mergeSort(right);

            // Merge the 2 sorted arrays
            merge(arr, left, right);
        }
    }

    public static int[] subArray(int[] arr, int start, int end)
    {
        int [] ret = new int [end - start];
        for(int i = 0; i < ret.length; i++, start++)
        {
            ret[i] = arr[start];
        }

        return ret;
    }

    public static int[] merge(int [] z, int [] x, int [] y)
    {
        int xdx = 0;
        int ydx = 0;
        int zdx = 0;

        for(; xdx < x.length && ydx < y.length; zdx++)
        {
            int xVal = x[xdx];
            int yVal = y[ydx];

            // Check less equal here because equal is NOT an inversion
            if(xVal <= yVal)
            {
                // No inversion case
                z[zdx] = xVal;
                xdx += 1;
            }
            else if(xVal > yVal)
            {
                // Inversion case
                z[zdx] = yVal;
                ydx += 1;

                // Increment numinv by the number of vals left in x
                INVCNT += x.length - xdx;
            }
        }

        // One of these two arrays may have values left
        while(xdx < x.length)
        {
            z[zdx] = x[xdx];
            zdx += 1;
            xdx += 1;
        }

        while(ydx < y.length)
        {
            z[zdx] = y[ydx];
            zdx += 1;
            ydx += 1;
        }

        if(zdx != z.length)
        {
            System.out.println("Merge not completed successfully.");
        }
        return z;
    }
}
