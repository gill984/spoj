import java.util.*;

class javaVsC {
	public static void main( String[] args ) {
		Scanner scan  = new Scanner( System.in );
		while( scan.hasNextLine() ) {
			String str = scan.nextLine();
			//System.out.println( str );
			char[] name = str.toCharArray();
			//System.out.println( Arrays.toString( name ) );
			boolean changed = false;
			for( int i = 0; i<name.length; i++ ) {
				if( name[i] == '_' ) {				//this is a c++ name (underscore)
					if( name[0] == '_' ) {
						System.out.println( "Error!" );
						changed = true;
						break;
					}
					cToJava( name );
					changed = true;
					break;
					//System.out.println( "javaToC" );
				} else if( Character.isUpperCase( name[i] ) ) {	//this is a java name (uppercase)
					if( Character.isUpperCase( name[0] ) ) {
						System.out.println( "Error!" );
						changed = true;
						break;
					}
					javaToC( name );
					changed = true;
					break;
					//System.out.println( "cToJava" );
				} else if( !Character.isLowerCase( name[i] ) ) {
					System.out.println( "Error!" );
					changed = true;
					break;
				}
			}
			if( !changed ) {
				System.out.println( str );
				
			}
		}
		return;
	}
	
	public static void cToJava( char[] name ) {
		StringBuilder ret = new StringBuilder();
		for( int i = 0; i<name.length ; i++ ) {
			if( Character.isLowerCase( name[i] ) ) {		//usual output
				ret.append( name[i] );
			}
			else if(name[i] == '_') {					//make some changes here
				++i;
				if( i >=name.length ) {
					System.out.println( "Error!" );
					return;
				}
				else if( Character.isLowerCase(name[i]) ) {
					ret.append( Character.toUpperCase(name[i]) );
				}
				else {
					System.out.println("Error!");
					return;
				}
			}
			else if ( Character.isUpperCase( name[i] ) ) {
				System.out.println("Error!");
				return;
			}
		}
		System.out.println( ret );
	}
	
	public static void javaToC( char[] name ){
		StringBuilder ret = new StringBuilder();
		for( int i = 0; i<name.length ; i++ ) {
			if( Character.isLowerCase( name[i] ) ) {		//usual output
				ret.append( name[i] );
			}
			else if(name[i] == '_') {					//make some changes here
				System.out.println( "Error!" );
				return;
			}
			else if ( Character.isUpperCase( name[i] ) ) {
				ret.append( '_' );
				ret.append( Character.toLowerCase( name[i] ) );
			}
		}
		System.out.println( ret );
	}
}	
