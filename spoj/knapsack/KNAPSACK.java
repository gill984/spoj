import java.util.*;

class KNAPSACK {

	public static void main (String[] args) {
		Scanner s = new Scanner(System.in);
		int size = s.nextInt();
		//System.out.print(size);
		int items  = s.nextInt();
		//System.out.print(items);
		int[] weights = new int[items];
		int[] values = new int[items];
		int[][] sumVal = new int [items+1][size+1];

		for( int i = 0; i<items; i++ )
		{
			weights[i] = s.nextInt();
			values[i] = s.nextInt(); //loads both array correctly
			//System.out.println(weights[i] + " " + values[i]);
		}

		for(int i = 1; i <= items; i++ ) {
			for( int w = 0; w <= size; w++ ) {
				if(weights[i-1] <= w) { 
					sumVal[i][w] = bigger(sumVal[i-1][w],values[i-1]+sumVal[i-1][w-weights[i-1]]);
				}
				else {
					sumVal[i][w] = sumVal[i-1][w];
				}
			}
		}
		
		System.out.println ( sumVal[items][size] );
	}

	public static int bigger( int l, int h ) {
		if( l > h ) return l;
		else return h;
	}

}
			
