import java.util.*;
import java.io.*;

class Maya
{

    public static void main(String[] args) {
        char dash = '-';
		char period = '.';
		char shell = 'S';
		
        Scanner s = new Scanner(System.in);
        while(s.hasNext()) {
			int lines = s.nextInt();
			if(lines == 0) return;//check for the end condition
			
			s.nextLine();
			String[] rows = new String[lines];
			for(int i =0; i<lines;i++)
				rows[i] = s.nextLine();
			
			s.nextLine(); //hopefully this will fix the error
				
			int[] a = new int[lines];
			//System.out.println(Arrays.toString(rows));  Correctly printed the array
			
			for(int i=0; i< lines;i++) { //iterates over the array of lines called rows
				for( int j=0; j < (rows[i]).length();j++) { //iterates over each of these entries in rows
					//System.out.println(rows[i].length());
					if(rows[i].charAt(j) == '-')
						a[lines-(i+1)] += 5;
					else if(rows[i].charAt(j) == '.')
						a[lines-(i+1)] += 1;
				}
			}
			//System.out.println(Arrays.toString(a));
			calcNumber(a,lines);
		}
	}
	
	public static void calcNumber(int [] a, int slots) {
        Double mult = new Double(1);
		Double sum = new Double(0);
        
		for(int i=0; i<slots ;i++) { //loop going through each slot in the int array
			if(i==0) mult = 1.0;
			else if(i==1) mult = 20.0;
			else if (i>1) {
			mult = 360*Math.pow(20,(i-2));
			}
			sum += a[i] * mult;
		}
		int total = 0;
		total = sum.intValue();
		System.out.println(total);
	}
	
}