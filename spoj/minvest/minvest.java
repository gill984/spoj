import java.util.*;
import java.io.*;

class minvest {
	public static void main( String[] args ) throws Exception {
		Parser scan = new Parser( System.in );

		int T = scan.nextInt();
		StringBuilder str = new StringBuilder();

		for( int i = 0; i < T; i++ ) {
			int start = scan.nextInt();
			int smallStart = start / 1000;
			int years = scan.nextInt();
			int bonds = scan.nextInt();
			int bigSize = smallStart * smallStart * smallStart;

			int[] interests = new int[ bonds ];
			int[] costs = new int[ bonds ];

			for( int j = 0; j < bonds; j++ ) {
				costs[ j ] = scan.nextInt() / 1000;
				interests[ j ] = scan.nextInt();
			}

			//System.out.println( Arrays.toString( costs ) );
			//System.out.println( Arrays.toString( interests ) );
			//input taken correctly

			//year1 calculation
			int[] dp = new int[ bigSize ];

			for( int j = 0; j < bigSize; j++ ) {
				dp[ j ] = -1;
			}

			dp[ 0 ] = 0;
			//generate the dp[] to pull values from
			//finding the max over a range takes O(n) time
			//generate the dp array
			for( int j = 0; j < bonds; ++j ) {
				for( int k = 0; k < dp.length; ++k ) {
					int interest = interests[j];		//interest and cost into temp variables for readability
					int cost = costs[j];
					if( dp[ k ] >= 0 ) {
						if( k + cost < dp.length && dp[ k + cost ] < dp[k] + interest ) {
							dp[ k + cost ] = dp[k] + interest;		//dp[k+cost] gets this new value
						}
					}
				}
			}

			//System.out.println( Arrays.toString( dp ) );

			int capital = start;
			int smallCapital = smallStart;

			for( int j = 0; j <= years; j++ ) {
				if( j == 0 ) {		//this is the first year, initial capital = capital
					capital = start;
					smallCapital = smallStart;
				} else {			//we are already past the first year
					//find the max for the given capital
					int max = 0;
					for( int k = 0; k <= smallCapital; k++  ) {
						if( dp[k] > max ) {
							max = dp[k];
						}
					}
					capital += max;
					smallCapital = capital / 1000;
					//System.out.println( capital );
				}
			}

			str.append( capital + "\n" );

		}
		System.out.print( str );
	}
}




class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
