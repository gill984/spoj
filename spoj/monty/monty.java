/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class monty
{
    public static void main (String[] args) throws java.lang.Exception
    {
        Random gen = new Random();
        int iterations = 10000;
        int wins = 0;
        int losses = 0;

        while(iterations-- > 0)
        {
            // Note in java, boolean arrays are initialized to all false
            // by default.

            // True if contains car, false if goat.
            boolean [] doors = new boolean [3];

            // True if door is opened, false if closed.
            boolean [] opened = new boolean [3];

            // Randomly pick a door for the car to be behind.
            int carDoor = gen.nextInt(3);
            doors[carDoor] = true;

            int choice = gen.nextInt(3) + 1;

            // Case where car is selected correctly at first.
            if(doors[choice - 1])
            {
                // Randomly select a door to open
                int open = (gen.nextInt(2) + choice) % 3;
                opened[open] = true;
            }
            else
            {
                // Have to open the door with a goat behind it.
                for(int i = 0; i < opened.length; i++)
                {
                    if(i + 1 != choice && !doors[i])
                    {
                        opened[i] = true;
                    }
                }
            }

            for(int i = 0; i < opened.length; i++)
            {
                if(!opened[i] && i + 1 != choice)
                {
                    choice = i + 1;
                    break;
                }
            }

            if(doors[choice - 1])
            {
                wins++;
            }
            else
            {
                losses++;
            }
        }

        System.out.println("Wins: " + wins);
        System.out.println("Losses: " + losses);
    }
}
