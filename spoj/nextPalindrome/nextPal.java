import java.util.*;
import java.io.*;

class nextPal {
	public static ArrayList< Integer > number;

	public static void main( String[] args ) throws Exception{
		StringBuilder build = new StringBuilder();
		Parser scan = new Parser( System.in );
		int cases = scan.nextInt();
		while( cases-- > 0 ) {
			String str = scan.next();
			number = new ArrayList<Integer>( str.length() );
			for( int i = 0; i < str.length(); i++ ) {
				number.add( (int) (str.charAt(i) - 48) );
			}
			addOne();
			while( ! check() ) {
				//add 1 to the number
				addOne();
			}
			//System.out.println( number.toString() );
			for( int i = 0; i < number.size(); i++ ) {
				build.append( number.get(i) );
			}
			build.append( "\n" );
			//number.clear();
			
		}
		System.out.print( build );
	}
	
	public static boolean check() {
		int i = 0;
		int j = number.size()-1;
		while( i < j ) {
			if( number.get(i) == number.get(j) ) {
				++i;
				--j;
				continue;
			} else {
				return false;
			}			
		}
		return true;
	}
	
	public static void addOne() {
		boolean done = false;
		for( int i = number.size()-1; i >= 0 && !done; --i ) {
			number.set( i, (number.get(i) + 1) );
			if( number.get(i) == 10 ) {
				number.set(i,0);					
				if( i == 0 ) {						//we need a bigger array
					number.add( 0,1 );				//append a 1 to the front
				}
			} else {
				done = true;
			}
		}
	}
	
}



class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
