import java.util.*;
import java.lang.Math;

public class notSoFlat {
	public static void main ( String[] args ) {
		
		boolean [] siev = new boolean[ 1000001 ];
		int [] primes = new int[ 78498 ];
		siev[0] = true;
		siev[1] = true;
		
		int count = 0;
		for( int i = 2; i < siev.length; i++ ) {
			if( siev[i] == false ) {
				primes[count] = i;
				count++;
				for( int j = i+i; j < siev.length; j += i ) {
					siev[j] = true;
				}	
			}
		}	
		//System.out.println( Arrays.toString(primes) );
		//primes were filled correctly
		
		Scanner scan = new Scanner( System.in );
		int c = 1;
		while( true ) {
			int a = scan.nextInt();
			int b = scan.nextInt();
			if( a == 0 && b == 0 ) {
				break;
			}
			
			ArrayDeque< Pair > queueA = new ArrayDeque< Pair >();
			ArrayDeque< Pair > queueB = new ArrayDeque< Pair >();
			
			//search for the primes which divide A
			for( int i = 0; ; i++ ) {
				if( primes[i] > a ) {
					break;
				} else if ( a % primes[i] == 0 ){
					Pair p = new Pair( primes[i] );			//create new pair for that prime
					a /= primes[i];							//divide a by that prime
					while( a % primes[i] == 0 ) {
						a /= primes[i];
						p.inc();
					}
					queueA.push( p );
				}
			}
			//System.out.println( Arrays.deepToString( queueA.toArray() ) );
			for( int i = 0; ; i++ ) {
				if( primes[i] > b ) {
					break;
				} else if ( b % primes[i] == 0 ){
					Pair p = new Pair( primes[i] );			//create new pair for that prime
					b /= primes[i];							//divide a by that prime
					while( b % primes[i] == 0 ) {
						b /= primes[i];
						p.inc();
					}
					queueB.push( p );
				}
			}
			
			System.out.println( Arrays.toString( queueA.toArray() ) );
			System.out.println( Arrays.toString( queueB.toArray() ) );
			
			//queues are now populated with the correct values,now go down the line and compare them
			int dimension = 0;
			int distance = 0;
			while( !queueA.isEmpty() && !queueB.isEmpty() ) {
				
				int primeA = queueA.peek().getPrime();
				int primeB = queueB.peek().getPrime();
				int freqA = queueA.peek().getFreq();
				int freqB = queueB.peek().getFreq();
				
				if( primeA == primeB ) {				//the two are equal, length is the abs difference in powers
														//still need to add one to the dimension count
					dimension++;
					distance += Math.abs( freqA - freqB );
					queueA.poll();
					queueB.poll();
					continue;
				} else if ( primeA < primeB ) {
					dimension++;
					distance += freqA;
					queueA.poll();
					continue;
				} else if ( primeA > primeB ) {
					dimension++;
					distance += freqB;
					queueB.poll();
					continue;
				}
			}
			while( !queueA.isEmpty() && queueB.isEmpty() ) {			//while B emptied first, copypaste A
				int primeA = queueA.peek().getPrime();
				int freqA = queueA.peek().getFreq();
				dimension++;
				distance += freqA;
				queueA.poll();
			}
			while( !queueB.isEmpty() && queueA.isEmpty() ) {			//while A emptied first, copypaste B
				int primeB = queueB.peek().getPrime();
				int freqB = queueB.peek().getFreq();
				dimension++;
				distance += freqB;
				queueB.poll();
			}
			System.out.println( c + ". " + dimension + ":" + distance );
			c++;
		}
		
	}
	
	static class Pair {
		int prime;
		int frequency;
		
		public Pair( int prime ) {
			this.prime = prime;
			this.frequency = 1;
		}
		
		public void inc () {
			++this.frequency;
		}
		
		public int getFreq() {
			return frequency;
		}
		
		public int getPrime() {
			return prime;
		}
		
		public String toString() {
			return( prime + " " + frequency );
		}
		
	}
}
