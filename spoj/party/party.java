import java.util.*;

class party {
	public static void main ( String[] args ) {
		Scanner scan = new Scanner( System.in );
		while( scan.hasNext() ) {
			int money = scan.nextInt();
			int n = scan.nextInt();
			int[] profits = new int[n];
			int[] costs = new int[n];
			for( int i = 0; i<n; i++ ) {
				costs[i] = scan.nextInt();
				profits[i] = scan.nextInt();
			}
			//System.out.println( Arrays.toString( profits ) );
			//System.out.println( Arrays.toString( costs ) );
			//array correctly populated
			//now come up with the DP solution
			int [][] dp = new int [n+1][money+1];
			// each slot in the array is populated by the optimal solution at that point
			//shifting to the right allows an increase in money allowed
			//shifting down allows an increase in the amount of parties we can go to
			//the column index is the amount of money spent
			//the row index is the amount parties we went to
			for(int i = 0; i<n+1; i++) {
				dp[0][i] = 0;
				dp[i][0] = 0;
			}
			//if we have no parties or money, we get no fun automatically
			//time for the real dp start
			for( int i = 1; i <= n ; i++ ) {						//for i < number of parties
				for( int j = 1; j <= money; j++ ) {					//for j < amount of money we are currently spending
					if( costs[i-1] > j ) {		//if the cost of the current party we are looking at is > money we canspend
						dp[i][j] = dp[i][j-1];	//this solution is equal to the solution with 1 less piece of money
					} else if ( costs[i-1] <= j ) {		//if costs we are looking at is less than the price allowed
						dp[i][j] = bigger( dp[i-1][j] , profits[i-1]+dp[i-1][j-costs[i-1]] );
						//this optimal solution is the biggest between
						//the optimal solution between that with one less item allowed
						//and the solution with this profit, added to the solution with 1 less item and profit
					}
				}
			}
			//solution is in the bottom right
			int maxValue = 0;
			int minMoney = money;
			for( int i = 0; i < n+1; i++) {
				System.out.println();
				for( int j = 0; j <= money; j++) {
					System.out.print( dp[i][j] + " " );
					if( dp[i][j] <= 9 ) {
						System.out.print(" ");
					}
					if( dp[i][j] >= maxValue ) {
						if(dp[i][j] == maxValue && minMoney > j) {
							maxValue = dp[i][j];
							minMoney = j;
						} else if ( dp[i][j] > maxValue ) {
							maxValue = dp[i][j];
							minMoney = j;
						}
					}
				}
			}
			System.out.println( "\n" + minMoney + " " + maxValue );
			//System.out.println( dp[n][money] );
			
		}
		
	}
	
	public static int bigger( int l, int h ) {
		if( l > h ) return l;
		else return h;
	}
}

/*
make two arrays of size money one being whether or not we have a valid solution there yet
and one being the max value at that point
iterate n( number of parties time ) over the array of size money
the initial case is [0] money is valid for zero fun
iterate backwards from the end of the fun array to find the first valid solution we have
then add the amount of money to the index and check if we have a larger value there and mark it as valid
keep doing this
search the array for the largest fun, for the least amount of money
