/*
 * Create a 1d array.
 * Fill the array with 0s except for the first element arr[0] = 1
 * loop:
 *  Count from length backward through the array.
 *  If arr[i] > 0, then arr[i+currentCost] = max(arr[i+currentCost], arr[i] + currentFun)
 */
import java.util.Scanner;

class partyRedo
{
    public static void main(String [] args)
    {
        Scanner in = new Scanner(System.in);
        StringBuilder out = new StringBuilder();

        while(true)
        {
            int b = in.nextInt();
            int n = in.nextInt();

            if(b == 0 && n == 0)
            {
                break;
            }

            // Read in party data
            int [] fun = new int[n];
            int [] cost = new int[n];

            // Read in values
            for(int i = 0; i < n; i++)
            {
                cost[i] = in.nextInt();
                fun[i] = in.nextInt();
            }

            // dp[0] is best with no cost, dp[n] is best with n cost
            int [] dp = new int[b + 1];

            // Iterate backwards from length to 0
            for(int p = 0; p < n; p++)
            {
                for(int i = dp.length; i >= 0; i--)
                {
                    // If this is a valid party already and that adding current
                    // cost will keep us in range
                    if(cost[p] + i < dp.length && (dp[i] > 0 || i == 0))
                    {
                        dp[i + cost[p]] = Math.max(dp[i + cost[p]], dp[i] + fun[p]);
                    }
                }
            }

            int max = 0;
            for(int i = 0; i < dp.length; i++)
                max = Math.max(dp[i], max);

            out.append(max + "\n");

        }

        System.out.print(out.toString());
    }
}
