import java.util.*;
import java.io.*;

class pigbank {
	public static void main( String[] args ) throws Exception {
		Parser in = new Parser( System.in );
		StringBuilder out = new StringBuilder();
		int cases = in.nextInt();
		while( cases-- > 0 ) {
			int empty = in.nextInt();
			int full = in.nextInt();
			int netWeight = full - empty;
			int[] dp = new int[ netWeight+1 ];
			for( int i = 0; i < dp.length; i++ ) {
				dp[i] = -1;
			}
			dp[0] = 0;
			int numCoins = in.nextInt();
			for( int i = 0; i < numCoins; i++ ) {
				int coinVal = in.nextInt();
				int coinWeight = in.nextInt();
				for( int j = 0; j + coinWeight < dp.length; j++ ) {
					if( dp[j] != -1 ) {	//we actually made it to this spot
						if( dp[j+coinWeight] > dp[j] + coinVal || dp[j+coinWeight] == -1 ) {
							dp[j+coinWeight] = dp[j] + coinVal;
						}
					}
				}
			}
			if( dp[dp.length-1] == -1 ) {	//could not find
				out.append("This is impossible.\n");
			} else {
				out.append("The minimum amount of money in the piggy-bank is " + dp[dp.length-1] + ".\n" );
			}
		}
		System.out.print( out );
	}
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
