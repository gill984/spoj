import java.io.DataInputStream;
import java.io.InputStream;

class StringTest
{
   public static void main(String[] args) throws Exception
   {
      // Read Input
      Parser in = new Parser(System.in);

      int tc = in.nextInt();

      while (tc-- > 0)
      {
         // Read in each edge length
         double U = in.nextInt();
         double W = in.nextInt();
         double v = in.nextInt();
         double V = in.nextInt();
         double w = in.nextInt();
         double u = in.nextInt();

         // All we need is the square of each
         U *= U;
         V *= V;
         W *= W;
         u *= u;
         v *= v;
         w *= w;

         // Intermediate computation for my ease
         double u_prime = prime(U, v, w);
         double v_prime = prime(V, u, w);
         double w_prime = prime(W, u, v);

         // Magic formula from the internet
         double answer = Math.sqrt((4 * u * v * w)
                                   - (u * u_prime * u_prime)
                                   - (v * v_prime * v_prime)
                                   - (w * w_prime * w_prime)
                                   + (u_prime * v_prime * w_prime)) / 12.0;

         System.out.format("%.4f\n", answer);
      }

   }

   public static double prime(double a, double b, double c)
   {
      return ((b + c - a));
   }
}

class Parser
{
   final private int BUFFER_SIZE = 1 << 16;

   private final DataInputStream din;
   private final byte[] buffer;
   private int bufferPointer, bytesRead;

   public Parser(InputStream in)
   {
      din = new DataInputStream(in);
      buffer = new byte[BUFFER_SIZE];
      bufferPointer = bytesRead = 0;
   }

   public int nextInt() throws Exception
   {
      int ret = 0;
      byte c = read();
      while (c <= ' ')
         c = read();
      boolean neg = c == '-';
      if (neg)
         c = read();
      do
      {
         ret = ret * 10 + c - '0';
         c = read();
      }
      while (c > ' ');
      if (neg)
         return -ret;
      return ret;
   }

   public double nextDouble() throws Exception
   {
      double toRet = 0.0;
      int ret = 0;
      byte c = read();
      while (c <= ' ')
         c = read();
      do
      {
         ret = ret * 10 + c - '0';
         c = read();
      }
      while (c > ' ' && c != '.');
      int ret2 = 0;
      double mult = 1.0;
      if (c == '.')
      {
         c = read();
         do
         {
            ret2 = ret2 * 10 + c - '0';
            mult *= .1;
            c = read();
         }
         while (c > ' ');
         toRet += ret2 * mult;
      }
      return toRet + ret;
   }

   public String nextString(int length) throws Exception
   {
      StringBuilder br = new StringBuilder();
      byte c = read();
      while (c <= ' ')
         c = read();
      for (int i = 0; i < length; ++i)
      {
         br.append((char) c);
         c = read();
      }
      return br.toString();
   }

   public String next() throws Exception
   {
      StringBuilder br = new StringBuilder();
      byte c = read();
      while (c <= ' ')
         c = read();
      while (c > ' ')
      {
         br.append((char) c);
         c = read();
      }
      return br.toString();
   }

   private void fillBuffer() throws Exception
   {
      bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
      if (bytesRead == -1)
         buffer[0] = -1;
   }

   private byte read() throws Exception
   {
      if (bufferPointer == bytesRead)
         fillBuffer();
      return buffer[bufferPointer++];
   }
}