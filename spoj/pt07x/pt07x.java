import java.util.*;
import java.io.*;

class pt07x {
	public static void main( String[] args ) throws Exception {
		Parser in = new Parser( System.in );
		StringBuilder out = new StringBuilder();
		int numNodes = in.nextInt();
		ArrayList< ArrayList<Edge> > graph = new ArrayList< ArrayList<Edge> >();
		//int [] degree = new int[numNodes];
		for( int i = 0; i < numNodes; i++ ) {
			graph.add( new ArrayList<Edge>() );
		}
		for( int i = 0; i < numNodes-1; i++ ) {
			int node1 = in.nextInt() - 1;	//0 indexing these values
			int node2 = in.nextInt() - 1;
			Edge e = new Edge( node1, node2 );
			graph.get(node1).add(e);
			graph.get(node2).add(e);	//edge object is the same between them...
			//degree[node1]++;
			//degree[node2]++;
		}
		for( int i = 0; i < graph.size(); i++ ) {
			//System.out.print( "Node " + i + " " );
			for( Edge e : graph.get(i) ) {
				//System.out.print( e + " " );
			}
			//System.out.println();
		}
		//graph is now created...
		int answer = 0;
		ArrayDeque<Integer> queue = new ArrayDeque<Integer>();
		for( int i = 0; i < numNodes; i++ ) {
			if( graph.get(i).size() == 1 ) {
				//this node is a leaf node
				queue.offer( i );	//push the leaf nodes onto the queue
			}
		}
		//System.out.println( queue );
		//queue initially correct
		
		
		while( !queue.isEmpty() ) {
			int child = queue.poll();
			//System.out.println( "Child: " + child );
			if( graph.get(child).size() != 1 ) {
				//System.out.println( "Size = " + graph.get(child).size() );
				continue;
			} else {
				//this is still a leaf node, remove it's parent node!
				//System.out.println( "Size = " + graph.get(child).size() );
				answer++;
				Edge toParent = graph.get(child).get(0);
				graph.get(child).remove(toParent);
				//assigning parent
				int parent = 0;
				if( toParent.node1 != child ) {
					parent = toParent.node1;
				} else {
					parent = toParent.node2;
				}
				//remove every edge in parent, and also the edge connecting the child to the parent
				for( int i = 0; graph.get(parent).size() != 0; ) {
					Edge e = graph.get(parent).get(i);
					//System.out.println( e );
					int removeFrom = 0;
					if( e.node1 != parent ) {
						removeFrom = e.node1;
					} else {
						removeFrom = e.node2;
					}
					graph.get(parent).remove(e);
					//System.out.println( parent + " parent : " + graph.get(parent) );
					graph.get(removeFrom).remove(e);
					//System.out.println( removeFrom + " removeFrom: " + graph.get(removeFrom) );
					
					if( graph.get(removeFrom).size() == 1 ) {
						queue.add(removeFrom);
					}
				}
			}
		}
		System.out.println( answer );
	}
}

class Edge {
	public int node1;
	public int node2;
	
	public Edge( int node1, int node2 ) {
		this.node1 = node1;
		this.node2 = node2;
	}
	
	public String toString() {
		return( "Edge between " + node1 + " and " + node2 );
	}
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
