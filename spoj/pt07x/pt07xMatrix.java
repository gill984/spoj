import java.util.*;
import java.io.*;

class pt07xMatrix {
	public static void main( String[] args ) throws Exception {
		Parser in = new Parser( System.in );
		StringBuilder out = new StringBuilder();
		int numNodes = in.nextInt();
		boolean[][] graph = new boolean[numNodes][numNodes];
		int[] degree = new int[numNodes];
		for( int i = 0; i < numNodes-1; i++ ) {
			int node1 = in.nextInt() - 1;	//0 indexing these values
			int node2 = in.nextInt() - 1;
			graph[node1][node2] = true;
			graph[node2][node1] = true;	//edge object is the same between them...
			degree[node1]++;
			degree[node2]++;
		}
		//graph is now created...
		int answer = 0;
		ArrayDeque<Integer> queue = new ArrayDeque<Integer>();
		for( int i = 0; i < numNodes; i++ ) {
			if( degree[i] == 1 ) {
				//this node is a leaf node
				queue.offer( i );	//push the leaf nodes onto the queue
			}
		}
		//System.out.println( queue );
		//queue initially correct
		
		
		while( !queue.isEmpty() ) {
			int child = queue.poll();
			//System.out.println( "Child: " + child );
			if( degree[child] != 1 ) {
				//System.out.println( "Size = " + graph.get(child).size() );
				continue;
			} else {
				//this is still a leaf node, remove it's parent node!
				//System.out.println( "Size = " + graph.get(child).size() );
				answer++;
				int parent = graph.get(child).get(0);
				Integer pointerParent = graph.get(child).get(0);
				graph.get(child).remove(0);	//remove the only edge here
				//remove every edge in parent, and also the edge connecting the child to the parent
				for( int i = 0; graph.get(parent).size() != 0; ) {
					int oc = graph.get(parent).get(i);
					//System.out.println( e );
					graph.get(parent).remove(0);
					//System.out.println( parent + " parent : " + graph.get(parent) );
					graph.get(oc).remove(pointerParent);
					//System.out.println( removeFrom + " removeFrom: " + graph.get(removeFrom) );
					
					if( graph.get(oc).size() == 1 ) {
						queue.add(oc);
					}
				}
			}
		}
		System.out.println( answer );
	}
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
