import java.util.*;
import java.io.*;

class roads {
	public static void main( String[] args ) throws Exception {
		Parser scan = new Parser( System.in );
		int cases = scan.nextInt();
		StringBuilder out = new StringBuilder();
		while( cases-- > 0 ) {
			int coins = scan.nextInt();
			int cities = scan.nextInt();
			int roads = scan.nextInt();
			ArrayList< ArrayList<Tuple> > graph = new ArrayList< ArrayList<Tuple> >(cities+1);
			//System.out.println( graph.size() );
			for( int i = 0; i < cities + 1; i++ ) {
				graph.add( new ArrayList<Tuple>() );
			}
			
			for( int i = 0; i < roads; i++ ) {
				int source = scan.nextInt();
				int dest = scan.nextInt();
				int length = scan.nextInt();
				int toll = scan.nextInt();
				Tuple tuple = new Tuple( dest, length, toll );
				//create these two adjacency matrices
				
				graph.get(source).add(tuple);
			}
			//System.out.println( Arrays.deepToString( graphLength ) + "\n" );
			//System.out.println( Arrays.deepToString( graphCost ) + "\n" );
			//graphs now created, do dijkstra's but allow same node to be added to the queue
			//we are going from city 1 to city n
			int currentNode = 1;
			PriorityQueue<Path> paths = new PriorityQueue<Path>();
			paths.add( new Path( currentNode, 0, 0 ) );
			boolean found = false;
			while( !paths.isEmpty() ) {
				//System.out.println( paths );
				Path p = paths.poll();
				while( true ) {
					if( p.cost > coins ) {
						//not a viable option ever
						if( paths.isEmpty() ) {
							break;
						}
						p = paths.poll();
						continue;
					} else {
						//viable option, continue
						break;
					}
					
				}
				//System.out.println( "We Chose: " + p + "\n" );
				if( p.currentLocation == cities && coins >= p.cost ) {
					//we have found the goal city, do we have enough money?
					out.append( p.length + "\n" );
					found = true;
					break;
				} else {
					int source = p.currentLocation;
					for( Tuple t : graph.get(source) ) {
							//we have a new path, add it to the priorityqueue
							paths.add( new Path( t.to, t.length + p.length, t.cost + p.cost ) );
					}
				}
			}
			if( !found ) {
				out.append( "-1\n" );
			}
			
		}
		System.out.print( out );
	}
}

class Tuple {
	public int to;
	public int length;
	public int cost;
	
	public Tuple( int to, int length, int cost ) {
		this.to = to;
		this.cost = cost;
		this.length = length;
	}
}

class Path implements Comparable<Object> {
	public int currentLocation;
	public int length;
	public int cost;
	
	public Path ( int currentLocation, int length, int cost ) {
		this.currentLocation = currentLocation;
		this.length = length;
		this.cost = cost;
	}
	
	public int compareTo( Object o ) {
		Path p = (Path) o;
		return this.length-p.length;
	}
	
	public String toString() {
		return(  currentLocation + " Length: " + length + " Cost: " + cost );
	}
}



class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
