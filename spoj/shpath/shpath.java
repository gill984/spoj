import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.io.DataInputStream;
import java.io.InputStream;

class shpath
{
    public static void main(String [] args) throws Exception
    {
        Parser in = new Parser(System.in);
        int tc = in.nextInt();
        StringBuilder out = new StringBuilder();

        while(tc-- > 0)
        {
            int numCities = in.nextInt();

            ArrayList<City> cities = new ArrayList<City>();
            HashMap<String, Integer> nameToIndex = new HashMap<String, Integer>();

            for(int i = 0; i < numCities; i++)
            {
                String name = in.next();
                nameToIndex.put(name, i+1);
                int numNeighbors = in.nextInt();
                City c = new City(i + 1, name, numNeighbors);
                for(int j = 0; j < numNeighbors; j++)
                {
                    int dest = in.nextInt();
                    int cost = in.nextInt();
                    c.addPath(new Path(i + 1, dest, cost));
                }
                cities.add(c);
            }

            int r = in.nextInt();
            
            for(int i = 0; i < r; i++)
            {
                int sourceIdx = nameToIndex.get(in.next());
                int destIdx = nameToIndex.get(in.next());
                int totalCost = dijkstra(sourceIdx, destIdx, cities);
                out.append(totalCost + "\n");
            }
        }

        System.out.print(out.toString());
    }

    public static int dijkstra(int sourceIdx, int destIdx, ArrayList<City> cities)
    { 
        PriorityQueue<Path> pq = new PriorityQueue<Path>();
        boolean [] visited = new boolean[cities.size() + 1];

        // Base case: a 0 cost path to the source is added to the pq
        pq.add(new Path(0, sourceIdx, 0));

        while(pq.size() > 0)
        {
            // Pick up the next shortest path
            Path p = pq.poll();

            // Check to see if this city has already been visited
            if(visited[p.getDest()])
            {
                continue;
            }

            if(p.getDest() == destIdx)
            {
                return p.getCost();
            }

            // This is a path to an unvisited node!
            visited[p.getDest()] = true;

            // Add all paths to unvisited nodes to the pq
            for(Path nextPath : cities.get(p.getDest() - 1).getPaths())
            {
                if(visited[nextPath.getDest()])
                {
                    continue;
                }
                else
                {
                    pq.add(new Path(nextPath.getSource(), nextPath.getDest(), p.getCost() +
                    nextPath.getCost()));
                }
            }
        }

        System.out.println("Error, path not possible");
        return -1;
    }
}

    

class City
{
    private final int index;
    private final String name;
    private ArrayList<Path> paths;

    public City(int i, String n, int numNeighbors)
    {
        this.index = i;
        this.name = n;
        paths = new ArrayList<Path>(numNeighbors);
    }

    public String getName()
    {
        return name;
    }

    public void addPath(Path p)
    {
        paths.add(p);
    }

    public ArrayList<Path> getPaths()
    {
        return this.paths;
    }

    public String toString()
    {
        StringBuilder out = new StringBuilder();
        out.append("Name: " + name + ", Index: " + index + "\n");
        for(Path p : paths)
        {
            out.append(p.toString());
            out.append("\n");
        }

        return out.toString();
    }
}

class Path implements Comparable
{
    private final int source;
    private final int dest;
    private final int cost;

    public Path(int s, int d, int c)
    {
        this.source = s;
        this.dest = d;
        this.cost = c;
    }

    public int getDest()
    {
        return this.dest;
    }

    public int getCost()
    {
        return this.cost;
    }

    public int getSource()
    {
        return this.source;
    }

    public int compareTo(Object o)
    {
        Path p = (Path) o;
        return this.cost - p.cost;
    }

    public String toString()
    {
        return ("source: " + source + ", dest: " + dest + ", cost: " + cost);
    }
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
