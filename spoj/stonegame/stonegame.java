import java.util.*;

class stones {

	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		int x = scan.nextInt();	
		
		
		for(int i = 0; i < x; i++)
		{
			int sum = 0;
			int b = scan.nextInt();
			int[] a = new int[b];
			for(int j =0; j<b;j++) {
				a[j] = (scan.nextInt())/(j+1); //piles are now populated with the amount of pulls allowed
			}
			for(int k=0;k<b;k++) {
				sum += a[k];
			}
			boolean odd = true;
			if( (sum%2) == 0 ) odd = false;
			if(odd) System.out.println("ALICE");
			else System.out.println("BOB");
		}

	}

}
			

