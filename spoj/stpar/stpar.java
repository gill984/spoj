import java.util.*;
import java.io.*;

class stpar
{
    public static void main(String [] args)
    {
	Scanner in = new Scanner(System.in);
	StringBuilder out = new StringBuilder();

	while(true)
	{
	    int n = in.nextInt();
	    if(n == 0) break;

	    ArrayDeque<Integer> street = new ArrayDeque<Integer>();
	    ArrayDeque<Integer> side = new ArrayDeque<Integer>();

	    for(int i = 0; i < n; i++)
		street.offer(in.nextInt());

	    boolean works = true;

	    // Start logic
	    int currentTarget = 1;
	    while((!street.isEmpty() || !side.isEmpty()) && currentTarget <= n)
	    {
		if(!side.isEmpty())
		{
		    int c = side.pop();
		    if(c == currentTarget)
		    {
			currentTarget++;
			continue;
		    }
		    else
		    {
			side.push(c);
			if(side.size() == n - currentTarget + 1)
			{
			    works = false;
			    break;
			}
		    }
		}

		if(!street.isEmpty())
		{
		    int c = street.poll();
		    if(c == currentTarget)
		    {
			currentTarget++;
			continue;
		    }
		    else
		    {
			side.push(c);
		    }
		}
	    }

	    if(works)
	    {
		out.append("yes\n");
	    }
	    else
	    {
		out.append("no\n");
	    }
	}

	System.out.print(out.toString());
    }
}
