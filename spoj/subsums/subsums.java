import java.util.Scanner;
import java.util.Arrays;
import java.io.DataInputStream;
import java.io.InputStream;

class subsums
{
    public static void main (String [] args) throws Exception
    {
        Parser scan = new Parser( System.in );
        int num = scan.nextInt();
        int min = scan.nextInt();
        int max = scan.nextInt();

        // Meet in the middle algorithm.
        // Find all the subsums of first then of second
        // Then sort the arrays
        // For each subsum in first's subsums, binary search the min and max values
        // Within second's subsums which satisfies the bounds given. Sum up all the
        // Ranges to get the final answer.
        int [] first = new int [num / 2];
        int [] second = new int [num / 2 + num % 2];

        // Fill the first and second arrays
        for( int i = 0; i < first.length; i++ )
        {
            first[i] = scan.nextInt();
        }

        for(int i = 0; i < second.length; i++)
        {
            second[i] = scan.nextInt();
        }

        int [] sums1 = findSums(first);
        int [] sums2 = findSums(second);

        Arrays.sort(sums1);
        Arrays.sort(sums2);

        long total = 0;
        for(int i = 0; i < sums1.length; i++)
        {
            int maxIdx = binarySearch(sums1[i], sums2, max, true);

            int minIdx = binarySearch(sums1[i], sums2, min, false);

            if(minIdx != -1 && maxIdx != -1)
            {
                total += (maxIdx - minIdx) + 1;
            }
        }

        System.out.print(total);
    }

    public static int[] findSums(int [] nums)
    {
        int [] sums = new int [1 << nums.length];

        // Fill each value in sums with the correct subsum
        // The outer loop goes through all subsums 1 by 1
        // The inner loop calculates each subsum. It loops from 0 to the length
        // of nums. It checks if the bit is a 1 and if so, it adds the number.
        for(int i = 0; i < sums.length; i++)
        {
            for(int bits = 0; bits < nums.length; bits++)
            {
                if((i & (1 << bits)) > 0)
                {
                    sums[i] += nums[bits];
                }
            }
        }

        return sums;
    }

    // The the argument maxSearch is passed in true if we are looking for a maxBound, and
    // false if we are looking for a minBound.
    public static int binarySearch(int s, int [] sums, int bound, boolean maxSearch)
    {

        // Find the maximum value of idx s.t.: min <= s + sums[idx] <= max
        int min = 0;
        int max = sums.length - 1;
        int mid = 0;
        int tempSum = 0;

        // This variable holds the current best index which has been found.
        int bestIdx = -1;

        while(min <= max)
        {
            mid = min + (max - min) / 2;
            tempSum = sums[mid] + s;

            if(bound > tempSum)
            {
                // If we are doing a max search, this is in bounds. Update
                // bestIdx if this is a new max.
                if(maxSearch && bestIdx < mid)
                {
                    bestIdx = mid;
                }

                // Shift up
                min = mid + 1;
            }
            else if(bound < tempSum )
            {
                // If we are doing a min search (!maxSearch), this is in bounds.
                // Update bestIdx if this is a new min, or it is the initial
                // value of -1.
                if(!maxSearch && (bestIdx > mid || bestIdx == -1))
                {
                    bestIdx = mid;
                }

                // Shift down
                max = mid - 1;
            }
            else
            {
                // Exact match found, but need to check for duplicates.
                // If this is a max search, check higher indexes, else check
                // lower indexes.
                if(maxSearch)
                {
                    for(; mid + 1 < sums.length; mid++)
                    {
                        if(sums[mid + 1] + s == bound)
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    for(; mid - 1 > 0; mid--)
                    {
                        if(sums[mid - 1] + s == bound)
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                return mid;
            }
        }

        // Remember this value is initialized to -1, so we still return -1 if
        // Nothing fits in our bounds. Note that if no exact match is found,
        // bestIdx will always be the index we want.
        return bestIdx;
    }
}

class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}



















