import java.util.*;

class tGrid {
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		int T = scan.nextInt();
		StringBuilder str = new StringBuilder();
		for(int i = 0; i<T; i++) {
			int rows = Integer.parseInt(scan.next());
			int cols = Integer.parseInt(scan.next());
			boolean square = false;
			boolean wider = false;
			boolean taller = true;
			if( rows == cols ) {
				square = true;				//if we have a square
				if( rows % 2 == 0) {
					str.append("L\n");
				} else {
					str.append("R\n");
				}
			} else if ( rows > cols ) {
				if( cols % 2 == 0) {		//even difference
					str.append("U\n");
				} else {
					str.append("D\n");
				}
			} else {			
				if( rows % 2 == 0 ) {
					str.append("L\n");
				} else {
					str.append("R\n");
				}
			}	
		}
		System.out.println(str);
	}
}
