import java.util.*;

class toAndFro {
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		StringBuilder str = new StringBuilder();
		while( true ) {
			int cols = Integer.parseInt( scan.next() );
			if( cols == 0 ) {
				break;
			} 
			String msg = scan.next();
			
			if ( cols == 1 ) {
				str.append( msg + "\n" );
				continue;								//trivial case, msg is the thing you want to add
			}
			
			int rows = msg.length() / cols;
			int sum = cols*2;
			int a = sum-1;
			int b = 1;
			for( int i = 0; ; i++) {
				str.append( msg.charAt(i) );			//append first character
				int index = i;
				while( index < msg.length() ) {
					index += a;
					if( msg.length() > index && index >= 0) {
						str.append( msg.charAt(index) );
					}
					index += b;
					if( msg.length() > index && index >= 0 ) {
						str.append( msg.charAt(index) );
					}
				}
				a -= 2;
				b += 2;
				if( a < 0 ) {
					//System.out.println("got here");
					break;
				}
			}											//done with that message
			str.append( "\n" );			
		}
		System.out.print( str );
	}
}
