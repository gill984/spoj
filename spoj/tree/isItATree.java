import java.util.*;
import java.io.*;

class isItATree {
	public static void main( String[] args ) throws Exception {
		Parser scan = new Parser( System.in );
		int nodes = scan.nextInt();
		int edges = scan.nextInt();
		boolean check = true;
		boolean [] visited = new boolean [ nodes + 1 ];		//0 means unseen so far, 1 means seen but not a back edge, 2 means it's a back edge
		StringBuilder str = new StringBuilder();
		if( nodes - edges != 1 ) {
			System.out.println( "NO" );
			check = false;
		}
		if( check ) {
			Node [] graph = new Node [ nodes + 1 ];
		
			for( int i = 0; i < graph.length; i++ ) {
				graph[i] = new Node( i );
			}
		
			for( int i = 0; i < edges; i++ ) {
				int from = scan.nextInt();
				int to = scan.nextInt();
				graph[ from ].addConnection( graph[ to ] );			//add a connection from the node to the other node
				graph[ to ].addConnection( graph[ from ] );			//add a connection in the reverse direction as well
			}
			/*
			for( Node n : graph ) {			//check to see if the graph is generated correctly
				System.out.println( n.num + ":" );
				for( int i = 0; i < n.connections.size(); i++ ) {
					System.out.println( n.connections.get(i).num);
				}
			}
			*/
			//seems to be correct from what I can see
		
			//do a bfs on this to see if we actually visit every node
			ArrayDeque<Node> queue = new ArrayDeque<Node>();
			Node n = graph[ 1 ];
			queue.offer( n );
			visited[ n.num ] = true;
		
			while ( !queue.isEmpty() ) {							//while the queue is not empty
				n = queue.poll();									//poll the element off of the queue					
				for( Node visit : n.connections ) {
					if( visited[visit.num] == false ) {
						visited[ visit.num ] = true;
						queue.offer( visit );
					}
				}
			}
		
			//System.out.println( Arrays.toString( visited ) );		//check whether this actually worked
			boolean good = true;
			for( int i = 1; i < visited.length; i++ ) {
				if( visited[i] == false ) {
					System.out.println( "NO" );
					good = false;
					break;
				} else {
					continue;
				}
			}
		
			if( good ) {
				System.out.println( "YES" );
			}
		
		}
	}		//end for the regular check
}

class Node {
	public int num;			//identifies this node
	public ArrayList< Node > connections = new ArrayList< Node >();		//an arraylist of all the nodes this node is connected to

	public Node ( int num ) {		//creating a new node from its identifier
		this.num = num;
	}
	
	public void addConnection( Node n ) {		//adding a connection by appending to the Connection arrayList
		connections.add( n );
	}
	
	public String toString() {
		return "" + num;
	}
}



class Parser {
    final private int BUFFER_SIZE = 1 << 16;

    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Parser(InputStream in)
    {
   	 din = new DataInputStream(in);
   	 buffer = new byte[BUFFER_SIZE];
   	 bufferPointer = bytesRead = 0;
    }

    public int nextInt() throws Exception
    {
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 boolean neg = c == '-';
   	 if (neg) c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ');
   	 if (neg) return -ret;
   	 return ret;
    }

    public double nextDouble() throws Exception {
   	 double toRet = 0.0;
   	 int ret = 0;
   	 byte c = read();
   	 while (c <= ' ') c = read();
   	 do
   	 {
   		 ret = ret * 10 + c - '0';
   		 c = read();
   	 } while (c > ' ' && c != '.');
   	 int ret2 = 0;
   	 double mult = 1.0;
   	 if (c == '.') {
   		 c = read();
   		 do {
   			 ret2 = ret2 * 10 + c - '0';
   			 mult *= .1;
   			 c = read();
   		 } while ( c > ' ');
   		 toRet += ret2*mult;
   	 }
   	 return toRet + ret;
    }

    public String nextString(int length) throws Exception {
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 for(int i = 0; i < length; ++i) {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    public String next() throws Exception{
   	 StringBuilder br = new StringBuilder();
   	 byte c = read();
   	 while(c <= ' ') c = read();
   	 while(c > ' ') {
   		 br.append((char)c);
   		 c = read();
   	 }
   	 return br.toString();
    }

    private void fillBuffer() throws Exception
    {
   	 bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
   	 if (bytesRead == -1) buffer[0] = -1;
    }

    private byte read() throws Exception
    {
   	 if (bufferPointer == bytesRead) fillBuffer();
   	 return buffer[bufferPointer++];
    }
}
