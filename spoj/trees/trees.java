import java.util.*;

class trees {
	public static void main( String[] args ) {
		Scanner scan = new Scanner( System.in );
		while( true ) {
			int nodes = scan.nextInt();
			int edges = scan.nextInt();
			if( nodes == 0 && edges == 0 ) {
				break;
			}
			node[] graph = new node[ nodes + 1 ];
			for( int i = 0; i <= nodes; i++ ) {
				graph[i] = new node( i );				
			}
			
			for( int i = 0; i < edges; i++ ) {
				int parent = scan.nextInt();
				int child = scan.nextInt();
				graph[ parent ].addChild( graph[child] );
			}
			System.out.println( Arrays.toString( graph ) );
		}
	}
}

class node {
		public ArrayList<node> children;
		public int number;
		
		public node( int number ) {
			this.number = number;
			children = new ArrayList<node>();
		}
		
		public void addChild( node n ) {
			this.children.add( n );
			System.out.println( this.number + " " + n.number );
		}
		
		public String toString() {
			return "hello";
		}
	}
