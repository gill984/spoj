import java.util.*;
import java.math.BigInteger;

class uncleJohn {
	public static void main ( String[] args ) {
		Scanner scan = new Scanner ( System.in );
		while( scan.hasNext() ) {
			BigInteger n = new BigInteger ( scan.nextInt() );
			BigInteger d = new BigInteger ( scan.nextInt() );
			if( n == 0 && d == 0 ) {
				break;
			}
			long n0 = n;
			for(int i = 0; i<d-1; i++) {
				n = n0*n;
				System.out.println( n );
				if( n == 0 ) {
					break;
				}
			}
			
			if ( d == 0 ) {
				n = 1;
			}
			System.out.println( n );
			
		}
	}
}
